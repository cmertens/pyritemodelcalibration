% Here we test if the assumption that monod kinetics of H2S in pyrite
% formation makes sense
% all concentrations in millimolar

H2S=8;%initial H2S concentration
FeOOH=4;%initial reactive iron concentration
zetamesh=1:1:200;

%% Reactive Iron
kFGoe=2.4*10^-3;
k=kFGoe;
syms F(t)
ode = diff(F,t) == k*F;
cond = F(0) == FeOOH;
ySol(t) = dsolve(ode,cond);
F=ySol(zetamesh);

%% H2S
odefun = @(zeta,H2S) odefuncH2S(zeta,H2S,F,zetamesh,kft);%equations
[zH,H] = ode45(odefun, zetamesh, [H2S]);
figure
 plot(zH,H) 
 odefun = @(zeta,P) odefuncP(zeta,P,F,H,zetamesh,kft);%equations
%Solve ODE
[zP,P] = ode45(odefun, zetamesh,[0]);
hold on
 plot(zP,P)
 plot(zetamesh,F)
legend('H2S','FeS2','FeOOH')


%% Data from Wang&Morse 1996
H2S=[133 30 5 2];
Py=[28 23 19 14]*100000;
figure
subplot(1,2,1)
p=plot(H2S,Py,'o','MarkerFaceColor','k');
xlabel('H_2S [mM]')
ylabel('Greigite pyritisation [% y^-1]')
subplot(1,2,2)
p=plot(1./H2S,1./Py,'o','MarkerFaceColor','k');
[b,bi]=gmregress(1./H2S,1./Py)
hold on
plot([0 max(1./H2S)],[b(1) max((1./H2S)*b(2)+b(1))])
xlabel('1/H_2S')
ylabel('1/Rate')
Rm=1/b(1)
Kh=b(2)*Rm
ErrRm=1/bi(2)-Rm;
KhErr=bi(4)*ErrRm
%% functions
function dHdz=odefuncH2S(z,H2S,F,zetamesh,k)
k=-k;
Fz=interp1(zetamesh,F,z);
dHdz=k*H2S/(1.62+H2S)*Fz;
end

function dPdz=odefuncP(z,P,F,H,zetamesh,k)
Hz=interp1(zetamesh,H,z);
Fz=interp1(zetamesh,F,z);
dPdz=k.*Hz./(1.62+Hz).*Fz;
end

