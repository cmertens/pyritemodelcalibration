syms Da S(z) H(z) z a KappaS Gamma0 Delta chi F b Psi0
%% more real

Sol=(0.1*sin(10*z)+2).*exp(-z);
DS = diff(Sol)
Ds2=diff(DS)
Source=Ds2*Delta-DS/Da+Gamma0*(a/(a+Da*z))^a*a/(a+Da*z)*S/(KappaS+S)-H*Psi0*(b./(b+(Da*chi*z))).^b*chi*b/(b/chi+Da*z);
simplify(Source)
