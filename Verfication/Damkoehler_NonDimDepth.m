% This is a small test showing if similar Da produces similar curves for
% dimensionless Sulfate and initial OC conditions no matter of individual
%kg0, w and t for
% 1) the definition of Da=sqrt(Ds*k)/w and zeta=z*sqrt(k/Ds)
% 2) the Definition of Da= Ds*k/w^2 and zeta=z*w/Ds

%% 1
figure
CoreNr=1
Data=ImportData(CoreNr);
Parameters=ImportParameters(CoreNr);
Boundaries=ImportBoundaries(Data,CoreNr);
%% --------------Non-dimensionalize Parameters----------------------------
NonDimParameters=makeNonDimParameters(Parameters,Boundaries);
kg0=[1e-4 2e-4 3e-4 4e-4 5e-4];
for i=1:5
Parameters.kg0=kg0(i)
Parameters.w=sqrt(Parameters.DS*Parameters.kg0)/NonDimParameters.Da;
[NonDimData]=makeNonDimData(Data,Parameters,Boundaries);
[NonDimBoundaries]=makeNonDimBoundaries(Parameters,Boundaries);
% calculate profiles with optimized paramters
maxDepth=2;
ModelSolution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,maxDepth);
% Plotting
plot(ModelSolution(2).Sigma,-ModelSolution(1).Sigma)
hold on
end