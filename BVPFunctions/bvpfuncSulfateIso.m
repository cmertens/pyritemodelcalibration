function dSdz=bvpfuncSulfateIso(z,S,Parameters,Boundaries)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
dSdz=[ S(2)   %d34S
    S(2)/(Parameters.Da)+Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(S(1)+S(3))/(Parameters.KappaS+(S(1)+S(3)))*(Parameters.alpha*S(1)/max([eps (S(3)+Parameters.alpha*S(1))]))
    S(4)  %d32S
    S(4)/Parameters.Da+Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(S(1)+S(3))/(Parameters.KappaS+(S(1)+S(3)))*(S(3)/(max([eps Parameters.alpha*S(1)+S(3)])))];
end