function NonDimParameters=makeNonDimParameters(Parameters,Boundaries)

field1='Da';value1=sqrt(Parameters.kg0*Parameters.DS)/Parameters.w;
field2='a';value2=Parameters.a;
field3='b';value3=Parameters.b;
field4='KappaS';value4=Parameters.Ks/Boundaries.S0;
field5='Delta';value5=Parameters.DH/Parameters.DS;
field6='chi';value6=1;%ratio of reactivity between OC and Iron
field8='KappaE';value8=Parameters.Ks/(Boundaries.S0); %Monod coefficient for H2S

%calculate temperature-dependent epsilon according to eldridge & farquhar 2016
%SO4
T=Parameters.T+273.15;
A_SO4=590.371*10^5;
B_SO4=-126.799*10^4;
C_SO4=109.869*10^2;
D_SO4=-137.516*10^-2;
E_SO4=1.000347;
%HS-
A_HS=213.257*10^5;
B_HS=-22.4384*10^4;
C_HS=12.2701*10^2;
D_HS=42.8805*10^-2;
E_HS=0.999864;
%H2S
A_H2S=462.536*10^5;
B_H2S=-48.3382*10^4;
C_H2S=20.1638*10^2;
D_H2S=77.2695*10^-2;
E_H2S=0.999749;

epsilon_HS=-1000*log((A_SO4/T^4+B_SO4/T^3+C_SO4/T^2+D_SO4/T+E_SO4)/(A_HS/T^4+B_HS/T^3+C_HS/T^2+D_HS/T+E_HS));
epsilon_H2S=-1000*log((A_SO4/T^4+B_SO4/T^3+C_SO4/T^2+D_SO4/T+E_SO4)/(A_H2S/T^4+B_H2S/T^3+C_H2S/T^2+D_H2S/T+E_H2S));
epsilon=mean([epsilon_H2S epsilon_HS]);

field9='alpha';value9=epsilon/1000+1;
NonDimParameters=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4,field5,value5,field6,value6,field8,value8,field9,value9);

end

