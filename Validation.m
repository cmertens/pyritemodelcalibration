%% Validating the non-dimensional equations conceptually and comparing to literature
% Please note that you need to have downloaded the validation dataset from 
% https://zenodo.org/uploads/13890801 to run this script. Put the Data fol-
% der into a overall project folder so that it contains a folder "Model"
% and a folder "Data".
% If you want to display and save the generated profiles, uncomment
% plotNonDimProfiles in line 67. Please make sure you have no other figure window open. 
%% Parallelise
%parpool('local')
%% Add folders to path
addpath(genpath('..\Data'));
addpath('ImportProfileDataFunctions');
addpath('NonDimensionalizeFunctions');
addpath('ODE');
addpath('Plotting_Functions');
addpath('SolverFunctions');
addpath('Data_Analysis_Functions');
addpath('Data_Processing_Functions');
addpath('Data_Extraction_Functions');
addpath('BVPFunctions');
addpath('CalculatingConstants');

%% Set directory and retrieve files
directoryPath = '../Data/Validation/Digitized_Profiles';
files = dir(fullfile(directoryPath, '*.xlsx')); % Specify the file extension
N = length(files); % Number of files in the directory
%% Preallocate
 allCost=struct;
 allResiduals=struct;
%% Loop
%looping through all profiles. If you wnat to run in parallel, use parfor
for i = 1:216
        %% File processing and parameters import
        fprintf('Processing CoreNr: %d, File: %s\n', i, files(i).name);
        
        % Import relevant data, parameters, and boundaries for each core
        Data = ImportData(i); % Import core data
        Parameters = ImportParameters(i); % Import model parameters
        Boundaries = ImportBoundaries(Data, i); % Set boundaries at sediment-water interface
        
        %% Oxygen Penetration Depth Calculation (z0)
        z0 = calcz0(Parameters.w); % Calculate z0 in cm
        
        % Modify boundary conditions at z0
        Boundaries.G0 = Boundaries.G0 * (Parameters.a * Parameters.w / (Parameters.a * Parameters.w + Parameters.kgSW * z0))^Parameters.a;
        Parameters.kg0 = Parameters.a / (Parameters.a / Parameters.kgSW + z0 / Parameters.w);
        
        %% Non-dimensionalize parameters and data
        NonDimParameters = makeNonDimParameters(Parameters, Boundaries);
        NonDimData = makeNonDimData(Data, Parameters, Boundaries);
        NonDimBoundaries = makeNonDimBoundaries(Parameters, Boundaries);
        
        %% Set Depth and Solver Grid
        maxDepth = min([2 * findMaxDepth(NonDimData), 10]); % Max depth in non-dimensional space
        zetamesh = linspace(0, maxDepth, 300); % Grid for the solver
        
        %% Solve Equations
        ModelSolution = ModelSolverNonDim(NonDimBoundaries, NonDimParameters, zetamesh);
        
        %% Calculate Cost and Residuals for different species
        species = {'G', 'S', 'F', 'P', 'H', 'd34S', 'd34H', 'd34P'};
        for sp = species
            [allCost(i).(sp{1}), allResiduals(i).(sp{1})] = calccost(ModelSolution, Data, Parameters, sp{1}, Boundaries.S0);
        end
        
        %% Plot Non-dimensional Profiles
        %plotNonDimProfiles(NonDimData, ModelSolution, i);
       
end

%% Plot Validation Results
% Plot figures showing RMSE for all modeled species
if length(allCost)==N
plot_Validation(allCost, allResiduals);
end

%% Save Results
%save('../Figures/ValidationResults.mat', 'allCost', 'allResiduals');
