function Dist2Coast= Interpolate_Dist2Coast(lat, long)

% Read Data
load("Data\Processed_Data\Global_Data\Dist2Coast.mat","raw_Dist")
raw_Dist.Dist(raw_Dist.Dist<0)=NaN;
Dist2Coast= interp2(raw_Dist.lat, raw_Dist.long, raw_Dist.Dist, lat, long);

end
