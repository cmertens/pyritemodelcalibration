function TOU= Interpolate_TOU(lat, long)
% fit found in OPD_Predictors.m
load("Data\Processed_Data\Global_Data\TOU.mat","raw_TOU")
F=scatteredInterpolant(raw_TOU.lat,raw_TOU.long,raw_TOU.TOU);
TOU=F(lat,long);


end
