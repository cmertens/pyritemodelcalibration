function interpolated_SedSize=Interpolate_SedSize(lat,long)

load('Data\Processed_Data\Global_Data\SedSize.mat','raw_SedSize');

F=scatteredInterpolant(deg2rad(raw_SedSize.lat),deg2rad(raw_SedSize.long),raw_SedSize.SedSize,'natural');
interpolated_SedSize=F(deg2rad(lat),deg2rad(long));

end
