function SedRate = Interpolate_SedRate(lat, long,Ref)
% extract_OC: Extracts total organic carbon (OC) data based on latitude and longitude values

%% Data from Bowles et al 2021
if strcmp(Ref,'Bowles')

    load("Data\Processed_Data\Global_Data\SedRate_Bowles.mat","raw_SedRate_Bowles")
    SedRate=interp2(raw_SedRate_Bowles.lat,raw_SedRate_Bowles.long,raw_SedRate_Bowles.SedRate,lat,long);

    %% Data from Egger 2019
elseif strcmp(Ref,'Egger')
   
    load("Data\Processed_Data\Global_Data\SedRate_Egger.mat","raw_SedRate_Egger")
    SedRate=interp2(raw_SedRate_Egger.lat,raw_SedRate_Egger.long,raw_SedRate_Egger.SedRate,lat,long);

        %% Data from Restreppo 2021
    elseif strcmp(Ref,'Restreppo')
     
    load("Data\Processed_Data\Global_Data\SedRate_Restreppo.mat","raw_SedRate_Restreppo")
    SedRate=interp2(raw_SedRate_Restreppo.lat,raw_SedRate_Restreppo.long,raw_SedRate_Restreppo.SedRate,lat,long);
end
