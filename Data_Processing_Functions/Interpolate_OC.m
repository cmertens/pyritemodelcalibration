function OC = Interpolate_OC(lat, long,Ref)
% extract_OC: Extracts total organic carbon (OC) data based on latitude and longitude values
warning off
%% Data from Paradis et al 2023
if strcmp(Ref,'Paradis')
    % Read Paradis et al. 2023 data
    load("Data\Processed_Data\Global_Data\OC_Paradis.mat","Raw_OC_Paradis")
    F = scatteredInterpolant(deg2rad(Raw_OC_Paradis.lat),deg2rad(Raw_OC_Paradis.long), Raw_OC_Paradis.OC, 'natural');
    
    OC = F(deg2rad(lat),deg2rad(long));
    OC(OC<0)=NaN;

    %% Data from Hayes et al 2021
elseif strcmp(Ref,'Hayes')
   
     load("Data\Processed_Data\Global_Data\OC_Hayes.mat","Raw_OC_Hayes")
     %convert to radians for interpolation
     x= deg2rad(Raw_OC_Hayes.lat);
    y= deg2rad(Raw_OC_Hayes.long);

    F = scatteredInterpolant(x,y,Raw_OC_Hayes.OC, 'natural');
    OC = F(deg2rad(lat), deg2rad(long));
    OC(OC<0)=NaN;

    elseif strcmp(Ref,'HayesandParadis')
    % Read Hayes et al. (2021) data
    load("Data\Processed_Data\Global_Data\OC_Hayes.mat","Raw_OC_Hayes")
    load("Data\Processed_Data\Global_Data\OC_Paradis.mat","Raw_OC_Paradis")

     %convert to radians for interpolation
    x= [deg2rad(Raw_OC_Hayes.lat);deg2rad(Raw_OC_Paradis.lat)];
    y= [deg2rad(Raw_OC_Hayes.long);deg2rad(Raw_OC_Paradis.long)];

    F = scatteredInterpolant(x, y, [Raw_OC_Hayes.OC;Raw_OC_Paradis.OC], 'natural');
    OC = F(deg2rad(lat),deg2rad(long));
    OC(OC<0)=NaN;

     
    %% Data from Seiter et al 2005
elseif strcmp(Ref,'Seiter')
    
    load("Data\Processed_Data\Global_Data\OC_Seiter.mat","Raw_OC_Seiter")

    % Interpolate data
    OC = interp2(Raw_OC_Seiter.long, Raw_OC_Seiter.lat, Raw_OC_Seiter.OC, long, lat);
    OC(OC<0)=NaN;
    %% Data from Atwood et al 2020
elseif strcmp(Ref,'Atwood')

   load("Data\Processed_Data\Global_Data\OC_Atwood.mat","Raw_OC_Atwood")

    % Interpolate OC data
    %Add values from 80 to 90 degree latitude
    Raw_OC_Atwood.lat=[repmat(90,8065,1) Raw_OC_Atwood.lat repmat(-90,8065,1)];
    Raw_OC_Atwood.long=[Raw_OC_Atwood.long(:,1) Raw_OC_Atwood.long Raw_OC_Atwood.long(:,end)];
    Raw_OC_Atwood.OC=[Raw_OC_Atwood.OC(:,1) Raw_OC_Atwood.OC Raw_OC_Atwood.OC(:,end)];
    OC= interp2(Raw_OC_Atwood.lat, Raw_OC_Atwood.long, Raw_OC_Atwood.OC, lat, long);
    OC(OC<0)=NaN;
    %% Data froom Lee et al 2019
elseif strcmp(Ref,'Lee')
 
    load("Data\Processed_Data\Global_Data\OC_Lee.mat","Raw_OC_Lee")

    F = scatteredInterpolant(deg2rad(Raw_OC_Lee.lat),deg2rad(Raw_OC_Lee.long),Raw_OC_Lee.OC');
    OC= F(deg2rad(long),deg2rad(lat));
    OC(OC<0)=NaN;
end



end
