function WaterDepth = Interpolate_WaterDepth(lat, long)

% Read Data
load("Data\Processed_Data\Global_Data\waterDepth.mat","raw_WaterDepth")
WaterDepth= interp2(raw_WaterDepth.lat, raw_WaterDepth.long, raw_WaterDepth.waterDepth, lat, long);

end
