function DOU = Interpolate_DOU(lat, long)

%based on Jorgensen 2022
%DOU
file_path = 'Data/Raw_Data/ChlorophyllA/SEASTAR_SEAWIFS_GAC.19970904_20101130.L3m.CU.CHL.chlor_a.9km.nc';
sed_ncid = netcdf.open(file_path);
Chlorq= ncread(file_path,'chlor_a');
datalong=ncread(file_path,'lon');
datalat=ncread(file_path,'lat');


[LAT,LONG]=meshgrid(datalat,datalong);

%import waterDepth
WaterDepth=Interpolate_WaterDepth(LAT, LONG);
%constants
A_DOU=1.728;
B_DOU=-0.631;
C_DOU=0.138;
DOU=A_DOU+B_DOU.*log10(WaterDepth)+C_DOU*log10(Chlorq);
DOU=10.^DOU;
DOU=interp2(LAT,LONG,DOU,lat,long);
end