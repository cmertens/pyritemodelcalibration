function interpolated_FeHR=Interpolate_FeHR(lat,long)
load('Data\Processed_Data\Global_Data\FeHR.mat');
load('Data\Processed_Data\Global_Data\FeTot.mat');

%convert Global FeTot to FeHR
fitresult = Total_Fe2FeHR(0);
FeHR_from_Total=raw_FeTot.FeTot.*fitresult(2);

%Add FeHR measured values
FeHRlat=[raw_FeTot.lat;raw_FeHR.lat];
FeHRlong=[raw_FeTot.long;raw_FeHR.long];
FeHR=[FeHR_from_Total;raw_FeHR.FeHR];
%repeat for consistent interpolation
 FeHR = [FeHR;FeHR;FeHR];
 FeHRlat =[FeHRlat;FeHRlat;FeHRlat];
 FeHRlong=[FeHRlong-360;FeHRlong;FeHRlong+360];

F=scatteredInterpolant((FeHRlat),(FeHRlong),FeHR,'natural');
interpolated_FeHR=F((lat),(long));

end
