function interpolated_FeTot=Interpolate_FeTot(lat,long)

load('Data\Processed_Data\Global_Data\FeTot.mat','raw_FeTot');

F=scatteredInterpolant(deg2rad(raw_FeTot.lat),deg2rad(raw_FeTot.long),raw_FeTot.FeTot,'natural');
interpolated_FeTot=F(deg2rad(lat),deg2rad(long));

end
