function Density= Interpolate_Density(lat, long)
load("Data\Processed_Data\Global_Data\Density.mat","raw_Density");
F=scatteredInterpolant(raw_Density.lat,raw_Density.long,raw_Density.Density');
Density=F(lat,long);
Density(Density>2.71)=2.71;
Density(Density<2.6)=2.6;
end
