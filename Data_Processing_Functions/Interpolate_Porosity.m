function Phi = Interpolate_Porosity(lat, long,Ref)
% extract_OC: Extracts total organic carbon (OC) data based on latitude and longitude values

%% Data from Martin et a 2015
if strcmp(Ref,'Martin')
    % Read Paradis et al. 2023 data
    load("Data\Processed_Data\Global_Data\Porosity_Martin.mat","raw_Porosity_Martin")
     Phi = interp2(raw_Porosity_Martin.lat, raw_Porosity_Martin.long, raw_Porosity_Martin.Porosity, lat, long);
    
    
    %% Data froom Lee et al 2019
elseif strcmp(Ref,'Lee')
 
    load("Data\Processed_Data\Global_Data\Porosity_Lee.mat","raw_Porosity_Lee")
    F = scatteredInterpolant(deg2rad(raw_Porosity_Lee.lat),deg2rad(raw_Porosity_Lee.long),raw_Porosity_Lee.Porosity);
    Phi= F(deg2rad(long),deg2rad(lat));
    Phi(Phi<0)=NaN;
    Phi(Phi>1)=NaN;
end



end
