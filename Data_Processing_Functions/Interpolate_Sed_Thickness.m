function interpolated_SedThickness=Interpolate_Sed_Thickness(lat,long)

load('Data\Processed_Data\Global_Data\Sediment_Thickness.mat','raw_Thickness');

interpolated_SedThickness=interp2(raw_Thickness.lat,raw_Thickness.long,raw_Thickness.Thickness,lat,long); %in m
end
