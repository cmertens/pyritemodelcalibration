function T= Interpolate_Seafloor_T(lat, long)

% Read Data
load("Data\Processed_Data\Global_Data\Seafloor_Temperature.mat","Raw_T")
Raw_T.T(Raw_T.T<0)=0.1;
T=interp2(Raw_T.lat,Raw_T.long,Raw_T.T,lat,long);
%fill in NaNs
Raw_T.lat(isnan(Raw_T.T))=[];
Raw_T.long(isnan(Raw_T.T))=[];
Raw_T.T(isnan(Raw_T.T))=[];
idx=isnan(T);
F=scatteredInterpolant(Raw_T.lat',Raw_T.long',Raw_T.T');
T(idx)=F(lat(idx),long(idx));


end
