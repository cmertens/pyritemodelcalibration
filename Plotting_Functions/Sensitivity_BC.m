f=figure;
Grid=zeros(864,432);
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\acmSmallGrid.mat')
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler57\Results.mat')
s1=subplot(2,1,1);
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(1,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
yline(s1,sum(totalPyBurial(:),'omitnan'),'Color','k','LineWidth',1,'LineStyle','--');
hold on
ylabel('pyrite burial mol S y^{-1}')
s2=subplot(2,1,2);
hold on
ylabel('\delta^{34}S_{pyrite}')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(1,avergaeDelta,'kd','MarkerFaceColor','k')
yline(s2,avergaeDelta,'Color','k','LineWidth',1,'LineStyle','--');

%% Reduced Porosity
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler58\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,2,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,2,avergaeDelta,'kd','MarkerFaceColor','k')


%% Double Psi
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler60\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,3,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,3,avergaeDelta,'kd','MarkerFaceColor','k')

%% Half Psi
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler59\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,4,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,4,avergaeDelta,'kd','MarkerFaceColor','k')

%% z0 =OPD
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler38\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,5,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,5,avergaeDelta,'kd','MarkerFaceColor','k')
%% 1.3 Gamma
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler48\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,6,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,6,avergaeDelta,'kd','MarkerFaceColor','k')
%% 0.7 Gamma

load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler47\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,7,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,7,avergaeDelta,'kd','MarkerFaceColor','k')

%% triple w
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler46\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,8,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,8,avergaeDelta,'kd','MarkerFaceColor','k')

%% 1/3 w
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler45\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,9,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,9,avergaeDelta,'kd','MarkerFaceColor','k')
%% high W*
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler51\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,10,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,10,avergaeDelta,'kd','MarkerFaceColor','k')
%% low w*
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler52\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,11,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,11,avergaeDelta,'kd','MarkerFaceColor','k')

%% +0.15% D_H D_S
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler49\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,12,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,12,avergaeDelta,'kd','MarkerFaceColor','k')
%% -0.15% D_H D_S
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler50\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,13,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,13,avergaeDelta,'kd','MarkerFaceColor','k')
%% 10*chi without dependencies
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler54\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,14,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,14,avergaeDelta,'kd','MarkerFaceColor','k')
%% 0.1*chi
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler53\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,15,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,15,avergaeDelta,'kd','MarkerFaceColor','k')
%% low kgsw
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler55\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,16,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,16,avergaeDelta,'kd','MarkerFaceColor','k')
%% high kgsw
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler56\Results.mat')
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=2*totalPyBurial/119.98; %in mols S y-1
plot(s1,17,sum(totalPyBurial(:),'omitnan'),'kd','MarkerFaceColor','k')
result=sum(totalPyBurial(:).*Pyd(:),'omitnan')./sum(totalPyBurial(:),'omitnan');           
avergaeDelta=mean(result,'omitnan')
plot(s2,17,avergaeDelta,'kd','MarkerFaceColor','k')

%% Ad lines for riverine input
y1=yline(s1,2.7*10^12,'Color',[0.2039    0.4275    0.5804],'LineWidth',2);
patch(s1,[0 18 18 0],[2.3*10^12 2.3*10^12 3.1*10^12 3.1*10^12], [0.2039    0.4275    0.5804],'FaceAlpha',0.5,'EdgeColor','none');

y2=yline(s1,1.3*10^12,'Color',[0.7098    0.4392    0.2549],'LineWidth',2);
patch(s1,[0 18 18 0],[1.1*10^12 1.1*10^12 1.5*10^12 1.5*10^12], [0.7098    0.4392    0.2549],'FaceAlpha',0.5,'EdgeColor','none');

%Delta Values
y3=yline(s2,4.8,'Color',[0.2039    0.4275    0.5804],'LineWidth',2);
patch(s2,[0 18 18 0],[-0.1 -0.1 9.7 9.7], [0.2039    0.4275    0.5804],'FaceAlpha',0.5,'EdgeColor','none');



xticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
xticklabels({'standard','reduced \phi','double \Psi','half \Psi','z_0=OPD','1.3 \Gamma','0.7 \Gamma',...
    'triple w','1/3 w','triple w*','1/3w*','+15%D_H & D_S','-15%D_H & D_S','10\chi','0.1\chi','low k_{G_{sw}}','high k_{G_{sw}}'})
xtickangle(45);
legend([y1,y2],'riverine influx','pyrite weathering','Location','northwest')

