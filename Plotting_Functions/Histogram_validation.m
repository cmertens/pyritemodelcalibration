function Histogram_validation(Res,ResWD,ResWDRBS,ylim1,xlim1,numbin1,numbinBS,Label)
h1=histogram(Res(ResWD),numbin1,'EdgeColor','none');
h1.BinLimits=xlim1;
h1.NumBins=numbin1;
hold on
h2=histogram(Res(ResWDRBS),numbinBS,'FaceColor','k','EdgeColor','none');
h2.BinLimits=xlim1;
h2.NumBins=numbinBS;
ylabel('Frequency')
set(gca,'FontSize',5)
plot([0 0],[0 ylim1-ylim1/7],'r:','LineWidth',1.2)
xlim(xlim1)
if any(ylim1)
ylim([0 ylim1])
end
text(xlim1(1)-xlim1(1)/10,ylim1-ylim1/10,Label,'FontSize',6,'FontName','arial')
