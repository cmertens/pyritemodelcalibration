function plot_Validation_func(allCost,allResiduals,whichprofile1,label1,whichprofile2,label2)
%% Load Data
load('Data\Processed_Data\Validation_Data.mat','lat','long')
load("Data\Processed_Data\Validation_Data_Combined.mat","Black_Sea_idx")
load("Data\Processed_Data\Validation_Data_Combined_withGlobal.mat",'WaterDepth_Combined_withGlobal','SMT_Combined_withGlobal')
% Interpolate Additional Variables
SMTq=SMT_Combined_withGlobal;
WaterDepth=WaterDepth_Combined_withGlobal;
BS=Black_Sea_idx';

%% Plotting options
Edgealpha=0;
Facealpha=1;
Size=20;

% individual settings
if contains(label1,'OC')
xlim1=[-3 3];
xlim2=[-30 40];
numbin1=8;
numbin2=8;
numbin3=8;
numbinBS=10; 
numbin11=8;
numbin22=8;
numbin33=8;
elseif contains(label1,'Fe')
xlim1=[-1 1];
xlim2=[-10 10];
numbin1=8;
numbin2=8;
numbin3=8;
numbin11=8;
numbin22=8;
numbin33=8;
numbinBS=8;
elseif contains(label1,'delta')
xlim1=[-50 50];
xlim2=[-50 50];
numbin1=6;
numbin2=8;
numbin3=8;
numbin11=5;
numbin22=5;
numbin33=5;
numbinBS=5;
elseif contains(label1,'Pyrite')
xlim1=[-1.5 1.5];
xlim2=[-50 50];
numbin1=8;
numbin2=5;
numbin3=6;
numbin11=8;
numbin22=5;
numbin33=5;
numbinBS=5;
end

%% Extract RMSE 1
allValuesCell = {allCost.(whichprofile1)};
% Convert empty cells to NaN values
allValuesCell(cellfun('isempty', allValuesCell)) = {NaN};
% Convert the cell array to a numeric array
RMSE = cell2mat(allValuesCell);
RMSE(RMSE==0)=NaN;

%% Extract Residuals 1
allValuesCell = {allResiduals.(whichprofile1)};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;

%Divide based on Water Depth
multipliedArraysWD = cellfun(@(x,r) x*0+r, allValuesCell,num2cell(WaterDepth),'UniformOutput', false);
ResidualBS=cellfun(@(x,r) x*0+r, allValuesCell,num2cell(BS),'UniformOutput', false);
ResWD= cat(2, multipliedArraysWD{:});  % Concatenate the arrays
RBS= cat(2, ResidualBS{:});  % Concatenate the arrays
ResWD(ResWD==0)=NaN;
RBS(isnan(RBS))=0;

%Divide based on MTZ
multipliedArraysSMT = cellfun(@(x,r) x*0+r, allValuesCell,num2cell(SMTq),'UniformOutput', false);
ResSMT= cat(2, multipliedArraysSMT{:});  % Concatenate the arrays
ResSMT(ResSMT==0)=NaN;
%Black Sea Idx
BSidx = cellfun(@(x,r) x*0+r, allValuesCell,num2cell(BS),'UniformOutput', false);
BSR= cat(2, BSidx{:});  % Concatenate the arrays
BSR(isnan(BSR))=0;

%% Identify limits for plotting
countsDepth=max(histcounts(Res(ResWD<200),numbin1,"BinLimits",xlim1));
countsSMT=max(histcounts(Res(ResSMT<1),numbin1,"BinLimits",xlim1));
ylim1=max([countsDepth countsSMT]);
ylim1=ceil(ylim1/10)*10;
ylim1=ylim1+ylim1/5;

countsDepth=max(histcounts(Res(ResWD>200&ResWD<2000),numbin2,"BinLimits",xlim1));
countsSMT=max(histcounts(Res(ResSMT>1&ResSMT<10),numbin2,"BinLimits",xlim1));
ylim2=max([countsDepth countsSMT]);
ylim2=ceil(ylim2/10)*10;
ylim2=ylim2+ylim2/5;

countsDepth=max(histcounts(Res(ResWD>2000),numbin3,"BinLimits",xlim1));
countsSMT=max(histcounts(Res(ResSMT>10),numbin3,"BinLimits",xlim1));
ylim3=max([countsDepth countsSMT]);
ylim3=ceil(ylim3/10)*10;
ylim3=ylim3+ylim3/5;


%% Plotting
%--------------------------------------------------------------------------
%Residuals
P=figure('Position', [10 10 510.236 200]);
n1=subplot(3,8,[1 2 9 10 17 18]);
n1.Position=[0.11    0.11    0.17    0.825];
map = [0.8588    0.7137    0.1216
    repmat([0.2235    0.5098    0.3961],9,1)
    repmat([0.4588    0.7098    0.8784],16,1)];
colormap(map)
x=1:numel(allCost);
scatterRMSE(x,RMSE,SMTq,WaterDepth,Size,BS,Facealpha,Edgealpha,label1)
t = n1.Title;
y=n1.YLim(2);
text(n1.Position(1),t.Position(2)+y/24,'a','FontName','Arial','FontSize',8,'FontWeight','bold')
%--------------------------------------------------------------------------
% Histograms
n3=subplot(3,8,3);
n3.Position=[ 0.330    0.6860    0.09   0.25];
Label='Shelf';
Histogram_validation(Res,ResWD<200,ResWD<200&RBS,ylim1,xlim1,numbin1,numbinBS,Label)
set(gca,'XTickLabel',[])
t = n3.Title;
text(xlim1(1),t.Position(2)+3*ylim1/24,'b','FontName','Arial','FontSize',8,'FontWeight','bold')
%%
n4=subplot(3,8,11);
n4.Position=[ 0.330    0.4064  0.09   0.25];
Label='Slope';
Histogram_validation(Res,ResWD>200&ResWD<2000,ResWD>200&ResWD<2000&RBS,ylim2,xlim1,numbin2,numbinBS,Label)
set(gca,'XTickLabel',[])
%%
n5=subplot(3,8,19);
n5.Position=[ 0.330  0.1100   0.09   0.25];
Label='Abyss';
Histogram_validation(Res,ResWD>2000,ResWD>2000&RBS,ylim3,xlim1,numbin3,numbinBS,Label)
xlabel('Residuals')

%----------------------------------------------------------------------------------------------------------
%% SMT
%------------------------------------------------------------------------------------------------------

n7=subplot(3,8,4);
n7.Position=[ 0.425    0.6860    0.09   0.25];
Label='SMT<1m';
Histogram_validation(Res,ResSMT<1,ResSMT<1&BSR,ylim1,xlim1,numbin2,numbinBS,Label)
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
t = n7.Title;
text(xlim1(1),t.Position(2)+3*ylim1/24,'c','FontName','Arial','FontSize',8,'FontWeight','bold')
%%
n8=subplot(3,8,12);
n8.Position=[ 0.425   0.4064  0.09   0.25];
Label='SMT 1-10m';
Histogram_validation(Res,ResSMT>1&ResSMT<10,ResSMT>1&ResSMT<10&BSR,ylim2,xlim1,numbin2,numbinBS,Label)
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
%%
n9=subplot(3,8,20);
n9.Position=[ 0.425  0.1100   0.09   0.25];
Label='SMT>10m';
Histogram_validation(Res,ResSMT>10,ResSMT>10&BSR,ylim3,xlim1,numbin2,numbinBS,Label)

set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
%% Extract RMSE 2
allValuesCell2 = {allCost.(whichprofile2)};
% Convert empty cells to NaN values
allValuesCell2(cellfun('isempty', allValuesCell2)) = {NaN};
% Convert the cell array to a numeric array
RMSEd = cell2mat(allValuesCell2);
RMSEd(RMSEd==0)=NaN;

%% Extract Residuals 2
allValuesCell2 = {allResiduals.(whichprofile2)};  % Extract the arrays within the field
multipliedArraysd = cellfun(@(x) x , allValuesCell2, 'UniformOutput', false);
Resd= cat(2, multipliedArraysd{:});  % Concatenate the arrays
Resd(Resd==0)=NaN;

%Divide based on SMT
multipliedArraysSMT = cellfun(@(x,r) x*0+r, allValuesCell2,num2cell(SMTq),'UniformOutput', false);
ResSMT= cat(2, multipliedArraysSMT{:});  % Concatenate the arrays
ResSMT(ResSMT==0)=NaN;

multipliedArraysBS = cellfun(@(x,r) x*0+r, allValuesCell2,num2cell(BS),'UniformOutput', false);
BSR= cat(2, multipliedArraysBS{:});  % Concatenate the arrays
BSR(isnan(BSR))=0;

% Divide based on Water Depth
multipliedArraysWD = cellfun(@(x,r) x.*0+r, allValuesCell2,num2cell(WaterDepth),'UniformOutput', false);
ResWDd= cat(2, multipliedArraysWD{:});  % Concatenate the arrays
ResWDd(ResWDd==0)=NaN;

multipliedArraysBS = cellfun(@(x,r) x*0+r, allValuesCell2,num2cell(BS),'UniformOutput', false);
RBS= cat(2, multipliedArraysBS{:});  % Concatenate the arrays
RBS(isnan(RBS))=0;

%% Identify limits for plotting
countsDepth=max(histcounts(Resd(ResWDd<200),numbin11,"BinLimits",xlim2));
countsSMT=max(histcounts(Resd(ResSMT<1),numbin11,"BinLimits",xlim2));

ylim1=max([countsDepth countsSMT]);
ylim1=ceil(ylim1/10)*10;
ylim1=ylim1+ylim1/5;

countsDepth=max(histcounts(Resd(ResWDd>200&ResWDd<2000),numbin22,"BinLimits",xlim2));
countsSMT=max(histcounts(Resd(ResSMT>1&ResSMT<10),numbin22,"BinLimits",xlim2));
ylim2=max([countsDepth countsSMT]);
ylim2=ceil(ylim2/10)*10;
ylim2=ylim2+ylim2/5;

countsDepth=max(histcounts(Resd(ResWDd>2000),numbin33));
countsSMT=max(histcounts(Resd(ResSMT>10),numbin33,"BinLimits",xlim2));
ylim3=max([countsDepth countsSMT]);
ylim3=ceil(ylim3/10)*10;
ylim3=ylim3+ylim3/5;

%% Plotting SMT
%plot histograms first beacuse of figure design.
n7=subplot(3,8,8);
n7.Position=[ 0.9   0.6860    0.09   0.25];
Label='SMT<1m';
Histogram_validation(Resd,ResSMT<1,ResSMT<1&BSR,ylim1,xlim2,numbin11,numbinBS,Label)
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
t = n7.Title;
text(xlim2(1),t.Position(2)+3*ylim1/24,'f','FontName','Arial','FontSize',8,'FontWeight','bold')
%%
n8=subplot(3,8,16);
n8.Position=[ 0.9   0.4064     0.09   0.25];
Label='SMT 1-10m';
Histogram_validation(Resd,ResSMT>1&ResSMT<10,ResSMT>1&ResSMT<10&BSR,ylim2,xlim2,numbin22,numbinBS,Label)
set(gca,'XTickLabel',[])
set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
%%
n9=subplot(3,8,24);
n9.Position=[ 0.9   0.11    0.09   0.25];
Label='SMT>10m';
Histogram_validation(Resd,ResSMT>10,ResSMT>10&BSR,ylim3,xlim2,numbin33,numbinBS,Label)
set(gca,'YTickLabel',[])
set(gca,'YLabel',[])
%% Water Depth
n3=subplot(3,8,7);
n3.Position=[ 0.805   0.6860    0.09   0.25];
Label='Shelf';
Histogram_validation(Resd,ResWDd<200,ResWDd<200&RBS,ylim1,xlim2,numbin11,numbinBS,Label)
set(gca,'XTickLabel',[])
t = n7.Title;
text(xlim2(1),t.Position(2)+3*ylim1/24,'e','FontName','Arial','FontSize',8,'FontWeight','bold')
%%
n4=subplot(3,8,15);
n4.Position=[ 0.805   0.4064    0.09   0.25];
Label='Slope';
Histogram_validation(Resd,ResWDd>200&ResWDd<2000,ResWDd>200&ResWDd<2000&RBS,ylim2,xlim2,numbin22,numbinBS,Label)
set(gca,'XTickLabel',[])
%%
n5=subplot(3,8,23);
n5.Position=[ 0.805   0.11    0.09   0.25];
Label='Abyss';
Histogram_validation(Resd,ResWDd>2000,ResWDd>2000&RBS,ylim3,xlim2,numbin33,numbinBS,Label)

%% Plot
n1=subplot(3,8,[5 6 13 14 21 22]);
n1.Position=[0.565    0.11    0.19    0.825];
x=1:numel(allCost);
scatterRMSE(x,RMSEd,SMTq,WaterDepth,Size,BS,Facealpha,Edgealpha,label2)
t = n1.Title;
y=n1.YLim(2);
text(n1.Position(1),t.Position(2)+y/24,'d','FontName','Arial','FontSize',8,'FontWeight','bold')
%% Save figures
% save_path=append('Figures\Calibrated_Profiles\');
% filename = sprintf('%s SMT', whichprofile1);
% full_path = fullfile(save_path, filename);
% saveas(P, full_path);
% set(P,'Units','Inches');
% pos = get(P,'Position');
% set(P,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
% print(P, append(full_path,'.pdf'), '-dpdf','-r0')
end