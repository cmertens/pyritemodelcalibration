function scatterRMSE(x,RMSE,SMTq,WaterDepth,Size,BS,Facealpha,Edgealpha,label1)
scatter(x(SMTq<1), RMSE(SMTq<1),Size,WaterDepth(SMTq<1),'o','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
hold on
scatter(x(SMTq<1&BS), RMSE(SMTq<1&BS),Size,'k','o','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
scatter(x(SMTq<10&SMTq>1), RMSE(SMTq<10&SMTq>1),Size,WaterDepth(SMTq<10&SMTq>1),'<','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
scatter(x(SMTq<10&SMTq>1&BS), RMSE(SMTq<10&SMTq>1&BS),Size,'k','<','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
scatter(x(SMTq>10), RMSE(SMTq>10),Size,WaterDepth(SMTq>10),'s','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
scatter(x(SMTq>10&BS), RMSE(SMTq>10&BS),Size,'k','s','filled','MarkerFaceAlpha',Facealpha,'MarkerEdgeColor','k','MarkerEdgeAlpha',Edgealpha);
%for legend
s4=plot(NaN,NaN,'ko','MarkerSize',4);
s5=plot(NaN,NaN,'k<','MarkerSize',4);
s6=plot(NaN,NaN,'ks','MarkerSize',4);
yline(median(RMSE,'omitnan'))
y=ylabel(label1);
if any(RMSE)
ylim([0 max(RMSE)+max(RMSE)/4])
end
%y.Position=[77.0900    1.500   -1.0000];%pyrite
%y.Position=[y.Position(1)-y.Position(1)   max(RMSE)/1.7  -1.0000];%OC
xlabel('Profile No.')
t=title('RMSE','FontSize',7);
set(gca,'FontSize',5)
set(gca,'FontName','arial')
x=get(gca,'Position');

c=colorbar;
title(c,'mbsl [m]');%meters below sea level
clim([0 max(WaterDepth)])
c.Position=[0.03936 0.0944 0.0114 0.8515];
set(c,'YLim',[0 5000]);
set(c,"Ticks" ,[200 2010 5000]);
set(c,'TickLabels',[100 2000 6000]);
%l.NumColumns = 2;

%x = [105,190, 190, 105, 105];%pyrite
xl=xlim;
yl=ylim;
x = [xl(1)+10,xl(2)-10, xl(2)-10 xl(1)+10, xl(1)+10];
y = [yl(2)-yl(2)/5.5, yl(2)-yl(2)/5.5, max(RMSE)+max(RMSE)/4.5, max(RMSE)+max(RMSE)/4.5, yl(2)-yl(2)/5.5];
plot(x, y, 'k-');
l =legend([s4 s5 s6],'SMT 0-1m','SMT 1-10m','SMT 10-1000m','Location','northwest');
l.Position=[l.Position(1)-0.0489 l.Position(2)+0.0136 l.Position(3)+0.0156 l.Position(4)+0.0200];

set(l,'Box','off')
set(gca,'Box','on')

end