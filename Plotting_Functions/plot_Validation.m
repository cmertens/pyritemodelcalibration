function plot_Validation(allCost,allResiduals)

%Organic Carbon and Sulfate
plot_Validation_func(allCost,allResiduals,'G','OC [wt%]','S','SO_4^{2-} [mM]');
%Reactive Iron and H2S
plot_Validation_func(allCost,allResiduals,'F','Fe_{HR} [wt%]','H','H_2S [mM]');
%Pyrite and Pyrite Isotopes
plot_Validation_func(allCost,allResiduals,'P','Pyrite (wt%)','d34P','\delta^{34}S_{Pyrite}');
%Sulfate Isotpoes and H2S Isotopes
plot_Validation_func(allCost,allResiduals,'d34S','\delta^{34}S_{SO_4}','d34H','\delta^{34}S_{H_2S}');

end