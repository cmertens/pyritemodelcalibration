function plotNonDimProfiles(NonDimData,ModelSolution,i)
maxDepth=findMaxDepth(NonDimData);
load('Data\Processed_Data\Validation_Data.mat','RefNames')
    f=figure('units','normalized','outerposition',[0 0 1 1],'Name',RefNames{i});
    %OC__________________________________________________________________________________
    subplot(1,8,1)
    plot(NonDimData(2).Gamma,-NonDimData(1).Gamma,'o')
    hold on
    plot(ModelSolution(2).Gamma,-ModelSolution(1).Gamma)
    ylabel('$ \zeta $','Interpreter','latex')
    xlabel('$ \Gamma $','Interpreter','latex')
    title('TOC')
    ylim([-maxDepth 0])

    % SO4--------------------------------------------------------------------------
    subplot(1,8,2)
    plot(NonDimData(2).Sigma,-NonDimData(1).Sigma,'o')
    hold on
    plot(ModelSolution(2).Sigma,-ModelSolution(1).Sigma)
ylabel('$ \zeta $','Interpreter','latex')
xlabel('$ \Sigma $','Interpreter','latex')
    title('SO4')
    ylim([-maxDepth 0])

%     % H2S---------------------------------------------------------------------------
    subplot(1,8,3)
    plot(NonDimData(2).Eta,-NonDimData(1).Eta,'o')
    hold on
    plot(ModelSolution(2).Eta,-ModelSolution(1).Eta)
    ylabel('$ \zeta $','Interpreter','latex')
    xlabel('$ \eta $','Interpreter','latex')
    title('H2S')
      ylim([-maxDepth 0])
   

    % Fe-------------------------------------------------------------------------
    subplot(1,8,4)
    plot(NonDimData(2).Psi,-NonDimData(1).Psi,'o')
    hold on
    plot(ModelSolution(2).Psi,-ModelSolution(1).Psi)
ylabel('$ \zeta $','Interpreter','latex')
xlabel('$ \Psi $','Interpreter','latex')
    title('FeHR')
        ylim([-maxDepth 0])

    % Pyrite -------------------------------------------------------------------------
    subplot(1,8,5)
    plot(NonDimData(2).Pi,-NonDimData(1).Pi,'o')
    hold on
    plot(ModelSolution(2).Pi,-ModelSolution(1).Psi)
    ylabel('$ \zeta $','Interpreter','latex')
    xlabel('$\ \Pi $','Interpreter','latex')
    title('Pyrite')
        ylim([-maxDepth 0])

% %     % Sulfate  isotopes----------------------------------------------
%     %d34S Iso
    subplot(1,8,6)
    plot(NonDimData(2).d34Sigma,-NonDimData(1).d34Sigma,'o')
    hold on
    plot(ModelSolution(2).d34Sigma,-ModelSolution(1).d34Sigma)
    ylabel('$\zeta $','Interpreter','latex')
   xlabel('$^{34} \delta \Sigma $','Interpreter','latex')
   title('SO4 Isotopes')
       ylim([-maxDepth 0])

%     %H2S Isotopes ---------------------------------------------------------
    subplot(1,8,7)
    plot(NonDimData(2).d34Eta,-NonDimData(1).d34Eta,'o')
    hold on
    plot(ModelSolution(2).d34Eta,-ModelSolution(1).d34Eta)
    ylabel('$\zeta $','Interpreter','latex')
    xlabel('$^{34} \delta \eta $','Interpreter','latex')
    title('H2S Isotopes')
   ylim([-maxDepth 0])

%     %Pyrite Isotopes ---------------------------------------------------------
    subplot(1,8,8)
    plot(NonDimData(2).d34Pi,-NonDimData(1).d34Pi,'o')
    hold on
    plot(ModelSolution(2).d34Pi,-ModelSolution(1).d34Pi)
    ylabel('$\zeta $','Interpreter','latex')
    xlabel('$^{34} \delta \Pi $','Interpreter','latex')
    title('Pyrite Isotopes')
        ylim([-maxDepth 0])

%% Save figure
    saveName=RefNames{i};
    savepath=append('C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Calibrated_Profiles\',saveName(1:end-4),'jpg');
   saveas(f,savepath);
end