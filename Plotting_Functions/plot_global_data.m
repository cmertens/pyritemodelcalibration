function plot_global_data(latitude, longitude, data, label, scale)
    % plot_global_extracted_data: Plots global extracted data on a map
    
    % Create a new figure
    %figure

    % Plot the data on a world map
    worldmap world
    geoshow(latitude, longitude, data, 'DisplayType', 'texturemap')
    geoshow('landareas.shp', 'FaceColor', 'white')
    colormap(flipud(slanCM('rain')))
    
    % Customize map appearance
    mlabel off; plabel off; gridm off
    set(gca, 'ColorScale', scale)
    
    % Add colorbar with label
    cb = colorbar;
    cb.Label.String = label;
  % clim([min(data(:)) max(data(:))])

end
