%% Read in Pyrite isotopic data
[t_Py,d34Py]=extract_delta34Py;
% Binned Data from Wu for comparison
TWu=readtable('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\d34Py\Wuetal2010.xlsx');
d34Py_Wu=str2double(TWu.Var4);
t_Wu=TWu.Var1;
%exclude NaNs
d34Py(isnan(t_Py))=[];
t_Py(isnan(t_Py))=[];
t_Py(isnan(d34Py))=[];
d34Py(isnan(d34Py))=[];
[t_Py,sortidx]=sort(t_Py);
d34Py=d34Py(sortidx);
%extract unique time values
 unique_t_Py=unique(t_Py);
 avg_Py= arrayfun(@(t) median(d34Py(t==t_Py),'omitnan'), unique_t_Py);

% smooth data
smoothed_Py=smoothdata(avg_Py,'loess','omitnan','SamplePoints', unique_t_Py,'SmoothingFactor',0.5);


%% Seawtaer sulfate concentration
DataSO4=readtable('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\d34SO4\grl61179-sup-0002-2020gl088766data_set_si-1.xlsx');
TSO4=DataSO4.Age_Myr_ICS2016_04Timescale_;
SO4=DataSO4.x_34SSO4___;
%exclude CAS derived isotopic composition
f=find(strcmp(DataSO4.Type,'Bulk CAS'));
TSO4(f)=[];
SO4(f)=[];
%exclude dta older than 600 Ma
SO4((TSO4)>600)=[];
TSO4((TSO4)>600)=[];
%extract unique time values
unique_t_SO4=unique(TSO4);
avg_SO4 = arrayfun(@(t) median(SO4(TSO4==t),'omitnan'), unique_t_SO4);
% smooth data
smoothed_SO4=smoothdata(avg_SO4,'loess','omitnan','SamplePoints',unique_t_SO4,'SmoothingFactor',0.2);

%% Delta SO4-PY
interp_smoothed_SO4=interp1(unique_t_SO4,smoothed_SO4,unique_t_Py);
smoothed_Delta_Py=interp_smoothed_SO4-smoothed_Py;
interp_SO4=interp1(unique_t_SO4,avg_SO4,t_Py);
%% Plots
%Isotopic composition SO4 and Pyrite
figure
%sulfate
s1=scatter(-TSO4,SO4,[],[0.2118, 0.3529, 0.4510],'filled','MarkerEdgeColor','none','MarkerFaceAlpha',0.3,'Marker','d');
hold on
p1=plot(-unique_t_SO4,smoothed_SO4,'Color',[0.1804, 0.6549, 0.9608],'LineWidth',2);
ylabel('\delta^{34}S')
xlabel('Ma')
%pyrite
s2=scatter(-t_Py,d34Py,[],[0.4509, 0.2980, 0.2980],'filled','MarkerEdgeColor','none','MarkerFaceAlpha',0.3,'Marker','d');
p2=plot(-unique_t_Py,smoothed_Py,'Color',[0.9490, 0.1579, 0.1569],'LineWidth',2);
legend([p1,p2],'\delta^{34}S_{SO4}','\delta^{34}S_{Py}')

%% Confidence Interval 
num_iterations=1000;
% Initialize array to store bootstrap differences
interpolated_bootstrap_difference = nan(length(unique_t_Py),num_iterations);

% Bootstrap Resampling Loop
for i = 1:num_iterations
    % Resample 
    resample_indices_Py = randsample(length(t_Py), length(t_Py), true);
    resampled_Py = d34Py(resample_indices_Py);
    resampled_t_Py=t_Py(resample_indices_Py);
    resample_indices_SO4 = randsample(length(SO4), length(SO4), true);
    resampled_SO4 = SO4(resample_indices_SO4); 
    resampled_t_SO4=TSO4(resample_indices_SO4);

    %unique data
    resampled_unique_t_SO4=unique(TSO4);
    resampled_unique_SO4 = arrayfun(@(t) median(resampled_SO4(resampled_t_SO4==t),'omitnan'), resampled_unique_t_SO4);
    resampled_unique_t_Py=unique(resampled_t_Py);
    resampled_unique_Py= arrayfun(@(t) median(resampled_Py (t==resampled_t_Py),'omitnan'), resampled_unique_t_Py);
   
    % smooth data
    smoothed_Py_bootstrap=smoothdata(resampled_unique_Py,'loess','omitnan','SamplePoints', resampled_unique_t_Py,'SmoothingFactor',0.5);
    smoothed_SO4_bootstrap=smoothdata(resampled_unique_SO4,'loess','omitnan','SamplePoints',resampled_unique_t_SO4,'SmoothingFactor',0.2);
   
    % Interpolate smoothed SO4 to Pyrite timestamps
    interp_smoothed_SO4_bootstrap = interp1(resampled_unique_t_SO4, smoothed_SO4_bootstrap, resampled_unique_t_Py);
    
    % Calculate difference for bootstrap sample
    bootstrap_difference = interp_smoothed_SO4_bootstrap - smoothed_Py_bootstrap;
    
    %interpolate to original t_py-values
    interpolated_bootstrap_difference(:,i) =interp1(resampled_unique_t_Py,bootstrap_difference,unique_t_Py);
    % Store differences

end


% Calculate the 5th and 95th percentiles for each common timestamp
lower_percentile = prctile(interpolated_bootstrap_difference, 5, 2);
upper_percentile = prctile(interpolated_bootstrap_difference, 95, 2);


%% Plot Confidence interval
figure
scatter(t_Py,interp_SO4-d34Py,[],[0.3804    0.4902    0.7882],'filled','MarkerEdgeColor','none','MarkerFaceAlpha',0.08,'Marker','d')
%delete NaNs
lower_percentile(isnan(upper_percentile))=[];
unique_t_Py(isnan(upper_percentile))=[];
smoothed_Delta_Py(isnan(upper_percentile))=[];
upper_percentile(isnan(upper_percentile))=[];
%smooth envelope

%color gradient
g=interp1(unique_t_Py,smoothed_Delta_Py,[unique_t_Py;flipud(unique_t_Py)]);
g(isnan(g))=5;
g=g/10;
% Patch for confidence intervals
patch([unique_t_Py; flipud(unique_t_Py)]', ...
      [lower_percentile', fliplr(upper_percentile')], ...
      g, 'FaceAlpha', 0.6, 'EdgeColor', 'none');
hold on;


% Adjust colormap
c=colormap(flipud(slanCM('deep')));
c=c(1:end-100,:);
colormap(c);
xlabel('Age (Ma)')
ylabel('\Delta_{Pyrite} (‰)')
ylim([-40 80])
xlim([0 550])
% Delta PYrite
plot(unique_t_Py,smoothed_Delta_Py,'Color',[0.3804    0.4902    0.7882 ],'LineWidth',2)
plot(unique_t_Py,upper_percentile,'Color',[0.3804    0.4902    0.7882 ],'LineWidth',0.5)
plot(unique_t_Py,lower_percentile,'Color',[0.3804    0.4902    0.7882 ],'LineWidth',0.5)
%ylim([10 60])

%% Pyrite concentration
Data=readtable('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\Pyrite_wt\Py_wt.csv');
Py_wt_t=Data.interpretedAge;
Py_wt=Data.S_py_wt__;
figure
plot(Py_wt_t,Py_wt,'o')
