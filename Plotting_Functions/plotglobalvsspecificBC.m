function plotglobalvsspecificBC(allCostGlobal,allCostLocal)
%Specific
%find out mean concentrations for relative RMSE
% for i=1:216
% Data=ImportData(i);
% OC(i)=median(Data(2).G,'omitnan');
% SO4(i)=median(Data(2).S,'omitnan');
% H2S(i)=median(Data(2).H,'omitnan');
% Fe(i)=median(Data(2).F,'omitnan');
% P(i)=median(Data(2).P,'omitnan');
% dSH(i)=median(abs(Data(2).d34H),'omitnan');
% dSS(i)=median(abs(Data(2).d34S),'omitnan');
% dSP(i)=median(abs(Data(2).d34P),'omitnan');
% end
Spec=struct2array(allCostLocal);
SpecR=reshape(Spec,8,216)';
N=ones(1,8);%sum(~isnan(SpecR));
S=median(SpecR,'omitnan');
% S(1)=S(1)/(N(1)*median(OC,'omitnan'));
% S(2)=S(2)/(N(2)*median(SO4,'omitnan'));
% S(3)=S(3)/(N(3)*median(Fe,'omitnan'));
% S(4)=S(4)/(N(4)*median(P,'omitnan'));
% S(5)=S(5)/(N(5)*median(H2S,'omitnan'));
% S(6)=S(6)/(N(6)*median(dSS,'omitnan'));
% S(7)=S(7)/(N(7)*median(dSH,'omitnan'));
% S(8)=S(8)/(N(8)*median(dSP,'omitnan'));

fields = fieldnames(allCostGlobal);
for i = 1:numel(fields)
    emptyIndices = cellfun('isempty', {allCostGlobal.(fields{i})});
     [allCostGlobal(emptyIndices).(fields{i})] = deal(NaN);
end
Glob=struct2array(allCostGlobal);
GlobR=reshape(Glob,8,216)';
G=median(GlobR,'omitnan');
G(1)=G(1)/S(1);
G(2)=G(2)/S(2);
G(3)=G(3)/S(3);
G(4)=G(4)/S(4);
G(5)=G(5)/S(5);
G(6)=G(6)/S(6);
G(7)=G(7)/S(7);
G(8)=G(8)/S(8);
figure
% p1=plot(1:8,S,'o','MarkerFaceColor',[0.3843, 0.6157, 0.6510]);
% hold on
p2=plot(1:8,G,'d','MarkerFaceColor',[0.9019, 0.5686, 0.3059]);
legend([p1 p2],'Local BC','Global BC','Location','northwest')
ylabel('Median Relative RMSE')
xticklabels({'OC','SO_4','Fe_{HR}','FeS_2','H_2S','\delta^{34}S_{SO_4}','\delta^{34}S_{H_2S}','\delta^{34}S_{FeS_2}'})
xtickangle(90);
end