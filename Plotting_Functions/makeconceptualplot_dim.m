function makeconceptualplot(ModelSolution,Parameters,Boundaries,color)
%dimensional depth
 zfactor=sqrt(Parameters.kg0/Parameters.DS);

figure
nexttile
 plot(ModelSolution(2).Gamma*Boundaries.S0/Parameters.fG,-ModelSolution(1).Gamma/zfactor,'LineWidth',1,'Color',color)
 xlabel('\Gamma')
 ylabel('z [cm]')
 title('OC [wt%]')
 nexttile
 plot(ModelSolution(2).Sigma*Boundaries.S0,-ModelSolution(1).Sigma/zfactor,'LineWidth',1,'Color',color)
 xlabel('\Sigma')
  ylabel('z [cm]')
  title('Sulphate [mM]')
 nexttile
 plot(ModelSolution(2).Eta*Boundaries.S0,-ModelSolution(1).Eta/zfactor,'LineWidth',1,'Color',color)
 xlabel('\eta')
 ylabel('\zeta')
  title('H_2S [mM]')
 nexttile
 plot(ModelSolution(2).Psi*Boundaries.S0/Parameters.fF,-ModelSolution(1).Psi/zfactor,'LineWidth',1,'Color',color)
 xlabel('\Psi')
 ylabel('z [cm]')
  title('Fe_{HR} [wt%]')
 nexttile
 plot(ModelSolution(2).Pi*Boundaries.S0/Parameters.fP,-ModelSolution(1).Pi/zfactor,'LineWidth',1,'Color',color)
 xlabel('\Pi')
 ylabel('z [cm]')
  title('Pyrite [wt%]')
 nexttile
 plot(ModelSolution(2).d34Sigma,-ModelSolution(1).d34Sigma/zfactor,'LineWidth',1,'Color',color)
 xlabel('\delta^{34}S_{SO_4^{2-}}')
 ylabel('z [cm]')
  title('Sulfate Isotopes')
 nexttile
  plot(ModelSolution(2).d34Eta,-ModelSolution(1).d34Eta/zfactor,'LineWidth',1,'Color',color)
  xlabel('\delta^{34}S_{H_2S}')
 ylabel('z [cm]')
  title('H_2S Isotopes')
 nexttile
  plot(ModelSolution(2).d34Pi,-ModelSolution(1).d34Pi/zfactor,'LineWidth',1,'Color',color)
  xlabel('\delta^{34}S_{FeS_2}')
   title('Pyrite Isotopes')
 ylabel('z [cm]')
end