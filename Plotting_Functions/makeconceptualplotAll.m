function makeconceptualplotAll(allSolutions,allSolutions2,idx1,idx2)
cm=[1.0000    1.0000    0.0667
   0.6941    0.9804    0.2627
   0.0588    1.0000    1.0000];
cm2=[0.6784    0.4588    0.1294
    0.1725    0.3686    0.2784
    0.0157    0.1725    0.2784];
%in the middle colors for lines
cmm=[0.9294    0.6941    0.1255
    0.2392    0.7020    0.5451
     0.1843    0.4471    0.5608];
c=zeros(300,3);
for i=1:length(idx1)
    c(i*100+1-100:i*100,:)=interp1([1;100],[cm(i,:);cm2(i,:)],1:100);
end
ModelSolution=allSolutions(idx1,idx2);
ModelSolution2=allSolutions2(idx1,idx2);
f=figure('Renderer', 'painters')
t=tiledlayout(1,8,'TileSpacing','tight','Padding','tight');
t.Title.FontSize=6;
t.XLabel.FontSize=6;
t.YLabel.FontSize=6;
%% GAMMA
nexttile
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,1,300) repmat((i-1)*100,1,300) ];
x=[log10((PlotSolution2(2).Gamma'));flipud(log10(PlotSolution(2).Gamma'))];
y=[-(PlotSolution2(1).Gamma'); -flipud(PlotSolution(1).Gamma')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
y(isinf(x))=[];
g(isinf(x))=[];
x(isinf(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(c)
hold on
plot(log10(PlotSolution2(2).Gamma),-PlotSolution(1).Gamma,':','LineWidth',1,'Color',cmm(i,:))
plot(log10(PlotSolution(2).Gamma),-PlotSolution(1).Gamma,'LineWidth',1,'Color',cmm(i,:))
end
 xlabel('\Gamma')
 ylabel('\zeta')
 title('Organic Carbon')
ylim([-1 0])

%% SIGMA
nexttile
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,1,300) repmat((i-1)*100,1,300) i];
x=[((PlotSolution2(2).Sigma));flipud((PlotSolution(2).Sigma));PlotSolution2(2).Sigma(1)];
y=[-(PlotSolution2(1).Sigma); -flipud(PlotSolution(1).Sigma);PlotSolution2(1).Sigma(1)];
% g(isnan(x))=[];
% y(isnan(x))=[];
% x(isnan(x))=[];
% y(isinf(x))=[];
% g(isinf(x))=[];
% x(isinf(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(c)
hold on
plot((PlotSolution2(2).Sigma),-PlotSolution(1).Sigma,'LineWidth',1,'Color',cmm(i,:))
plot((PlotSolution(2).Sigma),-PlotSolution(1).Sigma,':','LineWidth',1,'Color',cmm(i,:))
end
 xlabel('\Sigma')
 ylabel('\zeta')
  title('Sulfate')
ylim([-1 0])

%% ETA
nexttile
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,1,length(PlotSolution2(2).Eta)) repmat((i-1)*100,1,length(PlotSolution(2).Eta)) ];
x=[PlotSolution2(2).Eta';flipud(PlotSolution(2).Eta')];
y=[-(PlotSolution2(1).Eta)'; -flipud(PlotSolution(1).Eta')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
y(isinf(x))=[];
g(isinf(x))=[];
x(isinf(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(c)
hold on
plot((PlotSolution(2).Eta),-PlotSolution(1).Eta,':','LineWidth',1,'Color',cmm(i,:))
plot((PlotSolution2(2).Eta),-PlotSolution2(1).Eta,'LineWidth',1,'Color',cmm(i,:))
end
 xlabel('\eta')
 ylabel('\zeta')
  title('Sulfide')
ylim([-1 0])

%% PSI
nexttile
 for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,1,length(PlotSolution2(2).Psi)) repmat((i-1)*100,1,length(PlotSolution(2).Psi)) ];
x=[(log10(PlotSolution2(2).Psi'));flipud(log10(PlotSolution(2).Psi'))];
y=[-(PlotSolution2(1).Psi'); -flipud(PlotSolution(1).Psi')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
y(isinf(x))=[];
g(isinf(x))=[];
x(isinf(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(c)
hold on
plot(log10(PlotSolution(2).Psi),-PlotSolution(1).Psi,':','LineWidth',2,'Color',cmm(i,:))
plot(log10(PlotSolution2(2).Psi),-PlotSolution2(1).Psi,'LineWidth',2,'Color',cmm(i,:))
end
 xlabel('\Psi')
 ylabel('\zeta')
  title('Reactive Iron')
 ylim([-1 0])

 %% PYRITE
 cc=zeros(300,3);
for i=1:length(idx1)
    cc(i*100+1-100:i*100,:)=interp1([1;100],[cm2(i,:);cm(i,:)],1:100);
end
n1=nexttile;
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat((i)*100,1,length(PlotSolution2(2).Pi)) repmat((i-1)*100,1,length(PlotSolution(2).Pi)) ];
x=[(log10(PlotSolution(2).Pi));flipud(log10(PlotSolution2(2).Pi))];
y=[-(PlotSolution2(1).Pi); -flipud(PlotSolution(1).Pi)];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
y(isinf(x))=[];
g(isinf(x))=[];
x(isinf(x))=[];
%align patch with colormap
patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
hold on
plot(log10(PlotSolution(2).Pi),-PlotSolution(1).Pi,':','LineWidth',1,'Color',cmm(i,:))
plot(log10(PlotSolution2(2).Pi),-PlotSolution2(1).Pi,'LineWidth',1,'Color',cmm(i,:))
end
colormap(n1,cc)
 xlabel('\Pi')
 ylabel('\zeta')
  title('Pyrite')
ylim([-1 0])
%xlim([-6 2])
%% SULFATE ISO

n2=nexttile;
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,size(PlotSolution2(2).d34Sigma)) repmat((i-1)*100,size(PlotSolution(2).d34Sigma)) ];
x=[((PlotSolution2(2).d34Sigma'));flipud(PlotSolution(2).d34Sigma')];
y=[-(PlotSolution2(1).d34Sigma'); -flipud(PlotSolution(1).d34Sigma')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(n2,c)
hold on
plot((PlotSolution(2).d34Sigma),-PlotSolution(1).d34Sigma,':','LineWidth',1,'Color',cmm(i,:))
plot((PlotSolution2(2).d34Sigma),-PlotSolution2(1).d34Sigma,'LineWidth',1,'Color',cmm(i,:))
end
 xlabel('\delta^{34}S_{SO_4^{2-}}')
 ylabel('\zeta')
  title('Sulfate Isotopes')
set(gca, 'ColorOrder', cm)
ylim([-1 0])
xlim([0 500])
%% H2S ISOTOPES
n2=nexttile;
 for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat((i-1)*100,size(PlotSolution(2).d34Eta')); repmat((i)*100,size(PlotSolution2(2).d34Eta')) ];
x=[((PlotSolution2(2).d34Eta'));flipud(PlotSolution(2).d34Eta')];
y=[-(PlotSolution2(1).d34Eta'); -flipud(PlotSolution(1).d34Eta')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(n2,cc)
hold on
plot((PlotSolution(2).d34Eta'),-PlotSolution(1).d34Eta,':','LineWidth',1,'Color',cmm(i,:))
plot((PlotSolution2(2).d34Eta'),-PlotSolution2(1).d34Eta,'LineWidth',1,'Color',cmm(i,:))
end
  xlabel('\delta^{34}S_{H_2S}')
 ylabel('\zeta')
  title('Sulfide Isotopes')
 set(gca, 'ColorOrder', cm)
 ylim([-1 0])

 %% PYRITE ISO
n3=nexttile
for i=1:length(idx1)
PlotSolution=ModelSolution(i,i).SubStruct;
PlotSolution2=ModelSolution2(i,i).SubStruct;
g=[repmat(i*100,1,300) repmat((i-1)*100,1,300) ];
x=[((PlotSolution2(2).d34Pi));flipud(PlotSolution(2).d34Pi)];
y=[-(PlotSolution2(1).Gamma'); -flipud(PlotSolution(1).Gamma')];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
colormap(n3,c)
hold on
plot((PlotSolution(2).d34Pi),-PlotSolution(1).d34Pi,':','LineWidth',1,'Color',cmm(i,:))
plot((PlotSolution2(2).d34Pi),-PlotSolution(1).d34Pi,'LineWidth',1,'Color',cmm(i,:))
end
  xlabel('\delta^{34}S_{FeS_2}')
   title('Pyrite Isotopes')
 ylabel('\zeta')
 ylim([-1 0])
 
 %% SAVE
 %print(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\HeatMaps\DaG0\profiles','-dpng','-r1000')
end