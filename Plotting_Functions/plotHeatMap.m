function plotHeatMap(x,y,z)
set(groot, 'DefaultSurfaceEdgeColor', 'k', 'DefaultSurfaceEdgeAlpha', 0)
%figure('Renderer', 'Painters')
[X,Y]=meshgrid(log10(x),log10(y));
pcolor(X,Y,z)
xlabel('log_{10} \Gamma_0')
ylabel('log_{10} Da')  
%title(sprintf('\\chi=%1.0f, \\phi=%0.2f, \\Psi_0= %d',NonDimParameters.chi,Parameters.phi,NonDimBoundaries.Psi0))
% cmap=colormap(flipud(slanCM('blues')));
cmap=colormap(flipud(slanCM('deep')));
%cmap(100:900,:)=interp1(linspace(100,900,157),cmap(100:256,:),100:900);
%cmap(900:1900,:)=repmat(cmap(end,:),1001,1);
%cmap(700:1000,:)=interp1(linspace(700,1000,201),cmap(700:900,:),700:1000);
 set(gca,'layer','top')
  set(gca,'XMinorTick','off')
set(gca,'YMinorTick','off')
grid off
 colormap(cmap)
c=colorbar;
set(c, 'YDir', 'reverse' );
c.Label.String ='Pyrite formation depth \zeta_{Pfd}';%'\Delta_{Pyrite}';%'Integrated \Pi concentration';%
c.Label.FontSize = 12;
%clim([0 26]);
%delete(gcp)

end