%close all;clear variables;clc
% Read in measured Data
load("Data\Processed_Data\Validation_Data_combined.mat", 'FeTot_Combined','lat','long');

% Read in Fe estimates from Hayes 2021

interpolated_FeTot=Interpolate_FeTot(lat,long);

% Prepare data for comparison
[FeTot_measured, FeTot_estimate] = prepareCurveData(FeTot_Combined,interpolated_FeTot);


%weights= 1./abs(FeTot_measured-FeTot_estimate);
[~,gof] = fit(FeTot_measured,FeTot_estimate,'poly1');

% Set x-axis limit
maxX = max([FeTot_measured;FeTot_estimate]);
minX = min([FeTot_measured; FeTot_estimate]);

% Plotting
%f=figure
hold on
unity_line = plot([minX, maxX], [minX, maxX], ':', 'Color', [0.6784, 0.6784, 0.6784], 'LineWidth', 2); % Unity line
scatter(FeTot_measured, FeTot_estimate, [], [0.3490, 0.5647, 0.7098], 'filled', 'MarkerEdgeColor', [0.3490, 0.5647, 0.7098]); % Scatter plot
xlabel('Measured Fe [wt%]')
ylabel('Estimated Fe [wt %]')
xlim([minX, maxX])
ylim([minX, maxX])
legend(unity_line, sprintf('R^2=%0.2f',gof.adjrsquare), 'Location', 'northwest')
colormap(slanCM('Blues'))
set(f, 'Units', 'Inches');
pos = get(f, 'Position');
set(f, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)]);
print(f, 'Figures/Global_Data_Validation/FeTot.pdf', '-dpdf');
