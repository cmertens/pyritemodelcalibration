% Read in measured Data
close all;clear variables;clc
load("Data\Processed_Data\Validation_Data_combined.mat", 'FeHR_Combined', 'lat','long');

% Read in FeHR estimates from Hayes and other FeHR estimates
interpolated_FeHR=Interpolate_FeHR(lat,long);

% Prepare data for comparison
[FeHR_measured, FeHR_estimate] = prepareCurveData(FeHR_Combined,interpolated_FeHR);

%weights= 1./abs(FeHR_measured-FeHR_estimate);
[~,gof] = fit(FeHR_measured,FeHR_estimate,'poly1');

% Set x-axis limit
maxX = max([FeHR_measured;FeHR_estimate]);
minX = min([FeHR_measured; FeHR_estimate]);

% Plotting
f=figure;
hold on
unity_line = plot([minX, maxX], [minX, maxX], ':', 'Color', [0.6784, 0.6784, 0.6784], 'LineWidth', 2); % Unity line
scatter(FeHR_measured, FeHR_estimate, [], [0.3490, 0.5647, 0.7098], 'filled', 'MarkerEdgeColor', [0.3490, 0.5647, 0.7098]); % Scatter plot
xlabel('Measured FeHR [wt%]')
ylabel('Estimated FeHR [wt %]')
xlim([minX, maxX])
ylim([minX, maxX])
legend(unity_line, sprintf('R^2=%0.2f',gof.adjrsquare), 'Location', 'best')
colormap(slanCM('Blues'))
set(f, 'Units', 'Inches');
pos = get(f, 'Position');
set(f, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)]);
print(f, 'Figures/Global_Data_Validation/FeHR.pdf', '-dpdf');
