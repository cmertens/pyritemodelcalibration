load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Atwood et al 2020
OC_Atwood = Interpolate_OC(lat, long, 'Atwood');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_Atwood);

plot_OC_regression(OC_measured, OC_estimate);
title('Atwood et al. 2020')