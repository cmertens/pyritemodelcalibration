% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Paradis et al 2023
OC_Paradis = Interpolate_OC(lat, long, 'Paradis');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_Paradis);

plot_OC_regression(OC_measured, OC_estimate);
title('Paradis et al. 2023')
