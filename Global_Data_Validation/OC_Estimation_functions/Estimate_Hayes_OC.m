% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Hayes et al 2021
OC_Hayes = Interpolate_OC(lat, long, 'Hayes');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_Hayes);

plot_OC_regression(OC_measured, OC_estimate);
title('Hayes et al. 2021')