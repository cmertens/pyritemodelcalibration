close all; clear variables; clc
f=figure;
f.Units="centimeters";
f.Position(3:4) = [18 9];
t=tiledlayout(2,3,'TileSpacing','tight','Padding','tight');

% Atwood
ax=nexttile();
Estimate_Atwood_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

%Paradis 2023
ax=nexttile();
Estimate_Paradis_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

%Hayes 2021
ax=nexttile();
Estimate_Hayes_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

%Hayes 2021 and Paradis 2023
ax=nexttile();
Estimate_HayesandParadis_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

%Lee 2019
ax=nexttile();
Estimate_Lee_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

%Seiter 2004
ax=nexttile();
Estimate_Seiter_OC
set(gca,'Xscale','log')
set(gca,'Yscale','log')

c=colorbar;
c.Label.String='Weights';
c.Layout.Tile = 'east';

% Save Figure
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
 print(f,'Figures/Global_Data_Validation/OC_estimates.pdf','-dpdf')