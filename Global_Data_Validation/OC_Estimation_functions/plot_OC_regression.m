function plot_OC_regression(OC_measured, OC_estimate)

% Weight data according to distance to 1:1 line
weights= 1./abs(OC_measured-OC_estimate);

[~,gof] = fit(OC_measured,OC_estimate,'poly1','Weights',weights);

% Set x-axis limit
maxX = max([OC_measured; OC_estimate]);
minX = min([OC_measured; OC_estimate]);

% Plotting

hold on
unity_line = plot([minX, maxX], [minX, maxX], ':', 'Color', [0.6784, 0.6784, 0.6784], 'LineWidth', 2); % Unity line
scatter(OC_measured, OC_estimate, [], weights, 'filled', 'MarkerEdgeColor', [0.3490, 0.5647, 0.7098]); % Scatter plot
xlabel('Measured OC [wt%]')
ylabel('Estimated OC [wt %]')
xlim([minX, maxX])
ylim([minX, maxX])
legend(unity_line, sprintf('R^2=%0.2f',gof.adjrsquare), 'Location', 'best')
colormap(slanCM('Blues'))