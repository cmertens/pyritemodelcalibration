% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Seiter et al 2005
OC_Seiter = Interpolate_OC(lat, long, 'Seiter');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_Seiter);

plot_OC_regression(OC_measured, OC_estimate);
title('Seiter et al. 2005')
