% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Paradis et al 2023
OC_HayesandParadis = Interpolate_OC(lat, long, 'HayesandParadis');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_HayesandParadis);

plot_OC_regression(OC_measured, OC_estimate);
title('Hayes 2021 & Paradis 2023')