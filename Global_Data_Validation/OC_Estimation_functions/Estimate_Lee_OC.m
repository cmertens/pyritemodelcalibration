% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'OC_Combined', 'lat', 'long')

% Read in OC estimates from Paradis et al 2023
OC_Lee = Interpolate_OC(lat, long, 'Lee');

% Prepare data for comparison
[OC_measured, OC_estimate] = prepareCurveData(OC_Combined, OC_Lee);

plot_OC_regression(OC_measured, OC_estimate);
title('Lee et al. 2019')
