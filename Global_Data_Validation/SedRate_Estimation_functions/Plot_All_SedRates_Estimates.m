close all; clear variables; clc
f=figure;
f.Units="centimeters";
f.Position(3:4) = [18 9];
t=tiledlayout(1,3,'TileSpacing','tight','Padding','tight');

% Restreppo
ax=nexttile();
Estimate_Restreppo_SedRate
set(gca,'Xscale','log')
set(gca,'Yscale','log')

% Egger
ax=nexttile();
Estimate_Egger_SedRate
set(gca,'Xscale','log')
set(gca,'Yscale','log')

% Bowles
ax=nexttile();
Estimate_Bowles_SedRate
set(gca,'Xscale','log')
set(gca,'Yscale','log')


c=colorbar;
c.Label.String='Weights';
c.Layout.Tile = 'east';

% Save Figure
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
 print(f,'Figures/Global_Data_Validation/SedRates_estimates.pdf','-dpdf')