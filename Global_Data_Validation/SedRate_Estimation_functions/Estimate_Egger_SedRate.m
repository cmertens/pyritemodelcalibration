load('Data\Processed_Data\Validation_Data_Combined.mat', 'SedRate_Combined', 'lat', 'long')

% Read in OC estimates from Restreppo 2021
SedRate_Egger = Interpolate_SedRate(lat, long, 'Egger');

% Prepare data for comparison
[SedRate_measured, SedRate_estimate] = prepareCurveData(SedRate_Combined, SedRate_Egger);

plot_SedRate_regression(SedRate_measured, SedRate_estimate);
title('Egger et al. 2018')