load('Data\Processed_Data\Validation_Data_Combined.mat', 'SedRate_Combined', 'lat', 'long')

% Read in OC estimates from Bowles 2015
SedRate_Bowles = Interpolate_SedRate(lat, long, 'Bowles');

% Prepare data for comparison
[SedRate_measured, SedRate_estimate] = prepareCurveData(SedRate_Combined, SedRate_Bowles);

plot_SedRate_regression(SedRate_measured, SedRate_estimate);
title('Bowles et al 2015')