load('Data\Processed_Data\Validation_Data_Combined.mat', 'SedRate_Combined', 'lat', 'long')

% Read in OC estimates from Restreppo 2021
SedRate_Restreppo = Interpolate_SedRate(lat, long, 'Restreppo');

% Prepare data for comparison
[SedRate_measured, SedRate_estimate] = prepareCurveData(SedRate_Combined, SedRate_Restreppo);

plot_SedRate_regression(SedRate_measured, SedRate_estimate);
title('Restreppo et al. 2021')