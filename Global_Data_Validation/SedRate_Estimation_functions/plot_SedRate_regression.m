function plot_SedRate_regression(SedRate_measured, SedRate_estimate)

% Weight data according to distance to 1:1 line
weights=(1./abs(SedRate_measured-SedRate_estimate)).*SedRate_measured;

[~,gof] = fit(SedRate_measured,SedRate_estimate,'poly1','Weights',weights);

% Set x-axis limit
maxX = max([SedRate_measured; SedRate_estimate]);
minX = min([SedRate_measured; SedRate_estimate]);

% Plotting

hold on
unity_line = plot([minX, maxX], [minX, maxX], ':', 'Color', [0.6784, 0.6784, 0.6784], 'LineWidth', 2); % Unity line
scatter(SedRate_measured, SedRate_estimate, [], weights, 'filled', 'MarkerEdgeColor', [0.3490, 0.5647, 0.7098]); % Scatter plot
xlabel('measured w [cm y^-1]')
ylabel('estimated w [cm y^-1]')
xlim([minX, maxX])
ylim([minX, maxX])
legend(unity_line, sprintf('R^2=%0.2f',gof.adjrsquare), 'Location', 'northwest')
colormap(slanCM('Blues'))
set(gca,'ColorScale','log')