function plot_Phi_regression(Phi_measured, Phi_estimate)

% Weight data according to distance to 1:1 line
weights= Phi_measured./abs(Phi_measured-Phi_estimate);

[~,gof] = fit(Phi_measured,Phi_estimate,'poly1','Weights',(weights));

% Set x-axis limit
maxX = max([Phi_measured; Phi_estimate]);
minX = min([Phi_measured; Phi_estimate]);

% Plotting

hold on
unity_line = plot([minX, maxX], [minX, maxX], ':', 'Color', [0.6784, 0.6784, 0.6784], 'LineWidth', 2); % Unity line
scatter(Phi_measured, Phi_estimate, [], weights, 'filled', 'MarkerEdgeColor', [0.3490, 0.5647, 0.7098]); % Scatter plot
xlabel('Measured \phi')
ylabel('Estimated \phi')
xlim([minX, maxX])
ylim([minX, maxX])
legend(unity_line, sprintf('R^2=%0.2f',gof.adjrsquare), 'Location', 'northwest')
colormap(slanCM('Blues'))
set(gca,'ColorScale','log')