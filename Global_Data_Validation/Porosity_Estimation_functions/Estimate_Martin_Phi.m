% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'Porosity_Combined', 'lat', 'long')

% Read in OC estimates from Martin et al 2015
Phi_Martin = Interpolate_Porosity(lat, long, 'Martin');

% Prepare data for comparison
[Phi_measured, Phi_estimate] = prepareCurveData(Porosity_Combined, Phi_Martin);

plot_Phi_regression(Phi_measured, Phi_estimate);
title('Martin et al. 2015')