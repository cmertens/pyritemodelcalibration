close all; clear variables; clc
f=figure;
f.Units="centimeters";
f.Position(3:4) = [18 9];
t=tiledlayout(1,2,'TileSpacing','tight','Padding','tight');

%Martin 2105
ax=nexttile();
Estimate_Martin_Phi

%Lee 2019
ax=nexttile();
Estimate_Lee_Phi

c=colorbar;
c.Label.String='Weights';
c.Layout.Tile = 'east';

% Save Figure
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
 print(f,'Figures/Global_Data_Validation/Phi_estimates.pdf','-dpdf')