% Read in measured Data
% Read in measured Data
load('Data\Processed_Data\Validation_Data_Combined.mat', 'Porosity_Combined', 'lat', 'long')

% Read in OC estimates from Lee et al 2019
Phi_Lee= Interpolate_Porosity(lat, long, 'Lee');

% Prepare data for comparison
[Phi_measured, Phi_estimate] = prepareCurveData(Porosity_Combined, Phi_Lee);

plot_Phi_regression(Phi_measured, Phi_estimate);
title('Lee et al 2019')