%% Read in Data
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler57\Results.mat')
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler57\Variables.mat')
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\acmSmallGrid.mat')
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\latlongSmallGrid.mat')
Grid=zeros(864,432);
%Grid=zeros(2160,1080);
%% Pyrite Isotopic composition--------------------------------------------------------
PydR=reshape(Pyd,size(Grid)); % delta34S Pyrite
PydR=21-PydR;

f=figure;
worldmap world
g=geoshow(sedX,sedY,PydR,'DisplayType','Texturemap');
geoshow('landareas.shp','FaceColor','white')

cm=colormap(flipud(slanCM('curl')));
%cm=[repmat(cm(1,:),100,1);cm];
colormap(cm)
mlabel off; plabel off; gridm off
cb = colorbar('eastoutside');
cb.Label.String = '\Delta_{Pyrite}';

g.AlphaDataMapping = 'none'; % interpet alpha values as transparency values
g.FaceAlpha = 'texturemap'; % Indicate that the transparency can be different each pixel
alpha(g,double(~isnan(PydR)))
%exportgraphics(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Paper\GlobalPy.jpg','Resolution',1000)

% weighted average delta py global
%convert pyrite burial into weights
dPdt=reshape(dPydt,size(Grid));
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=(totalPyBurial(:));
result=sum(totalPyBurial.*PydR(:),'omitnan')./sum(totalPyBurial,'omitnan');           
avergaeDelta=mean(result,'omitnan')

%% Pyrite Formation Depth%
PydDepthR=PydDepthDim;%.*sqrt(DSEuler./kg0Euler);
PydDR=reshape(PydDepthR,size(Grid));
%PydDR((PydDR)<1)=NaN;
%limit to actual sediment thickness
% for i=1:size(PydDR,1)
%     for j=1:size(PydDR,2)
% PydDR(i,j)=min([PydDR(i,j) SedatPydepth(i,j)]);
%     end
% end
f=figure;
worldmap world
g=geoshow(sedX,sedY,PydDR,'DisplayType','Texturemap');
geoshow('landareas.shp','FaceColor','white')
g.AlphaDataMapping = 'none'; % interpet alpha values as transparency values
g.FaceAlpha = 'texturemap'; % Indicate that the transparency can be different each pixel
alpha(g,double(~isnan(PydDR)))
%cm=colormap(flipud(slanCM('rain')));
cm=colormap(flipud(slanCM('curl')));
% cm=[repmat(cm(1,:),200,1);cm];
% afm=colormap((slanCM('dense')));
% cm=[(afm(100:250,:));cm];
colormap(cm)
mlabel off; plabel off; gridm off
cb = colorbar('eastoutside');
cb.Label.String = 'Pyrite Formation depth (cm)';
 set(gca,'ColorScale','log')
%
%exportgraphics(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Paper\GlobalPydDepth.jpg','Resolution',1000)
%% Pyrite Burial
dPdt=reshape(dPydt,size(Grid));
%PydDR=inpaint_nans(PydDR,2);
%PydR(PydR==0)=max(PydR);
f=figure;
worldmap world
g=geoshow(sedX,sedY,dPdt,'DisplayType','Texturemap');
geoshow('landareas.shp','FaceColor','white')
g.AlphaDataMapping = 'none'; % interpet alpha values as transparency values
g.FaceAlpha = 'texturemap'; % Indicate that the transparency can be different each pixel
alpha(g,double(~isnan(dPdt)))
%cm=colormap(flipud(slanCM('rain')));
cm=colormap((slanCM('curl')));
% cm=[cm;repmat(cm(end,:),100,1)];
% afm=colormap((slanCM('dense')));
% cm=[(afm(100:250,:));cm];
colormap(cm)
mlabel off; plabel off; gridm off
cb = colorbar('eastoutside');
cb.Label.String = 'Pyrite burial rate [g/(y cm^2)]';
 set(gca,'ColorScale','log')
% total 
%calc lat and long values around each data point on the grid
% latX=sedX-(sedX(1,2)-sedX(1,1))/2;
% latX(:,size(latX,2)+1)=(latX(:,size(latX,2))+sedX(1,2)-sedX(1,1));
% longY=sedY-(sedY(1,2)-sedY(1,1))/2;
% longY(size(longY,1)+1,:)=(longY(size(longY,1),:)+sedY(2,1)-sedY(1,1));
% latX=deg2rad(latX(1,:));
% longY=deg2rad(longY(:,1));
%wgs84 = wgs84Ellipsoid("km");
% for i=1:length(latX)-1
%     i/length(latX)
% for j=1:length(longY)-1
% a(i,j) = areaquad(latX(i),longY(j),latX(i+1),longY(j+1),wgs84,"radians");
% end 
% end
%convert to square cm
% acm=a.*1e10;
%multiply each cell by its burial rate
totalPyBurial=(acm)'.*(dPdt);
totalPyBurial=(totalPyBurial(:));
totalPyBurial(isnan(totalPyBurial))=[];
totalPyBurial=sum(totalPyBurial)
%exportgraphics(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Paper\GlobalPyBurial.jpg','Resolution',1000)
%% Pyrite total mols 
gPy=reshape(Pyg,size(sedX));
molPy=gPy./119;
molPy(molPy==0)=NaN;
f=figure;
worldmap world
g=geoshow(sedX,sedY,molPy,'DisplayType','Texturemap');
geoshow('landareas.shp','FaceColor','white')
g.AlphaDataMapping = 'none'; % interpet alpha values as transparency values
g.FaceAlpha = 'texturemap'; % Indicate that the transparency can be different each pixel
alpha(g,double(~isnan(gPy)))
%cm=colormap(flipud(slanCM('rain')));
cm=colormap((slanCM('curl')));
% cm=[cm;repmat(cm(end,:),100,1)];
% afm=colormap((slanCM('dense')));
% cm=[(afm(100:250,:));cm];
colormap(cm)
mlabel off; plabel off; gridm off
cb = colorbar('eastoutside');
cb.Label.String = 'total pyrite [mol cm^{-2}]';
 set(gca,'ColorScale','log')
%exportgraphics(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Paper\GlobalPyMol.jpg','Resolution',1000)
molPy(isnan(molPy))=[];
sum(molPy)
%% Pyrite CDF plot
totalPyBurial=(dPdt);
totalPyBurial=(totalPyBurial(:));
[lat,long]=Define_Global_Grid(5);
wD=Interpolate_WaterDepth(lat,long);
Pm=totalPyBurial;%molPy(sortidx);
wD(isnan(Pm))= [];
Pm(isnan(Pm))= [];
Pm(isnan(wD))=[];
wD(isnan(wD))=[];
wD(wD==0)=0.00001;
% Pm(wD==0)=[];
% wD(wD==0)=[];
[wD,sortidx]=sort(wD);
Pm=Pm(sortidx);
Pmsum=cumsum(Pm);  
P=(Pmsum/max(Pmsum)*100);
f=figure('Renderer', 'painters')
semilogx(wD,P,'k','LineWidth',2)
xlim([1 max(wD)]);
shelfIdx=find(wD>200,1);
slopeIdx=find(wD>2000,1);
hold on
p1=patch([wD(1:shelfIdx)';fliplr(wD(1:shelfIdx))']',[P(1:shelfIdx); zeros(length(P(1:shelfIdx)),1)],[0.8902 0.6706 0.4431],'FaceAlpha',0.6);
p2=patch([wD(shelfIdx:slopeIdx)';fliplr(wD(shelfIdx:slopeIdx))'],[P(shelfIdx:slopeIdx); zeros(length(P(shelfIdx:slopeIdx)),1)],[0.4039 0.9020 0.8706],'FaceAlpha',0.6);
p3=patch([wD(slopeIdx:end)';fliplr(wD(slopeIdx:end))'],[P(slopeIdx:end); zeros(length(P(slopeIdx:end)),1)],[0.4431 0.6353 0.8902],'FaceAlpha',0.6);
line([wD(shelfIdx) wD(shelfIdx)],[0 P(shelfIdx)],'Color','k','LineStyle',':')
line([wD(slopeIdx) wD(slopeIdx)],[0 P(slopeIdx)],'Color','k','LineStyle',':')
xlabel('water depth [m]')
ylabel('% total pyrite burial')
legend([p1 p2 p3],sprintf('Shelf %2.0f %%',P(shelfIdx)),sprintf('Slope %2.0f %%',P(slopeIdx)-P(shelfIdx)),sprintf('Abyss %2.0f %%',P(end)-P(slopeIdx)),'Location','northwest')
