% This code calculates global grids for Pyrite burial rate,
% formation depth and isotopic composition
%% --------------------------------------------------------------------- %%
start_time = datetime() % Records the start time of code execution
load('Variables.mat'); % Loads necessary variables from Global Boundary Conditions function output

local_job = parcluster('local');
pool = parpool(local_job, 48); % Starts a parallel pool with 48 workers

%% Initialize arrays to store solutions and calculations

PydDepth =  nan(size(OCAll));
PydDepthDim = nan(size(OCAll));
Pyd =nan(size(OCAll));
PyWtPercent = nan(size(OCAll));
dPydt = nan(size(OCAll));
Pyg = nan(size(OCAll));

parfor i=1:length(OCAll)
    lastwarn('') % Clear last warning message
    try
        % set up parameters
        Parameters=struct();
        Parameters.rho=rhoEuler(i); %sediment density
        Parameters.phi=PqEuler(i);%Porosity
        Parameters.fG=calcfG(Parameters.phi,Parameters.rho);
        Parameters.fF=calcfF(Parameters.phi,Parameters.rho);
        Parameters.fP=calcfP(Parameters.phi,Parameters.rho);
        Parameters.w=WqEuler(i); %sedimentation rate
        Parameters.kg0=kg0Euler(i); %OC reactivity
        Parameters.DS=DSEuler(i); %Sulfate Diffusivity
        Parameters.DH=DHEuler(i); %H2S Diffusivity
        Parameters.a=0.23; %constant for OC decay
        Parameters.b=0.17; %constant for Fe decay
        Parameters.Ks=1.62; %Monod coefficient
        Parameters.T=TEuler(i); %Temperature

        %set up boundaries
        Boundaries=struct();
        Boundaries.G0=OCAll(i); %Organic carbon
        Boundaries.S0=SO4All(i); %Sulfate
        Boundaries.F0=FeHrAll(i); %Reactive Iron
        Boundaries.P0=0;         %Pyrite
        Boundaries.d34S0=21;     %delta34S sulfate

        %calculate non-dim Parameters
        NonDimParameters=makeNonDimParameters(Parameters,Boundaries);
        NonDimBoundaries=makeNonDimBoundaries(Parameters,Boundaries);
        NonDimBoundaries.Eta0=0;

        maxDepth=5; %non-dim depth until profiles are calculated
        zetamesh=linspace(0,maxDepth,300); %solver grid
        z0=OPDEuler(i);
        %non-dim depth index
        zeta0=sqrt(Parameters.kg0./DSEuler(i))*z0;
        %Initiating solver loop for grid points where boundary conditions
        %are given
        if ~isnan(WqEuler(i)) && ~isnan(DSEuler(i)) && ~isnan(OCAll(i)) && zeta0<100*sedEuler(i)

            %solve equations
            ModelSolution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,zetamesh);

            %Pyrite concentration
            Py=ModelSolution(2).Pi;
            Pyz=ModelSolution(1).Pi;

            %Pyrite isotopic composition
            Pyd34=ModelSolution(2).d34Pi;
            Pyd34z=ModelSolution(1).d34Pi;
            Pyd34=interp1(Pyd34z,Pyd34,Pyz); %use same grid

            Pyz(Py==0)=[];
            Pyd34(Py==0)=[];
            Py(Py==0)=[];
            Pyz(isnan(Py))=[];
            Pyd34(isnan(Py))=[];
            Py(isnan(Py))=[];
            FormationDepthThreshhold=0.0005*Py(end);


            idx=find(flipud(diff(Py))>FormationDepthThreshhold,1);% find first derivative in upside down array that is larger than threshhold
            if isempty(idx)
                PydDepth(i)=0;
            else
                PydDepth(i)=Pyz(numel(Py)-idx);
            end
            %limit to sediment thickness
            PydDepthDim(i)=sqrt(DSEuler(i)./ Parameters.kg0).*PydDepth(i);%convert to dimensional depth
            if PydDepthDim(i)>100*sedEuler(i) %in cm
                PydDepthDim(i)=100*sedEuler(i);
            end
            PydDepth(i)=PydDepthDim(i)/sqrt(DSEuler(i)./ Parameters.kg0); %convert back to non-dimensional depth
            fdIdx=find(Pyz>=PydDepth(i),1);
            %delete pyrite values for depth larger than pyrite formation
            %depth
            Py(fdIdx:end)=[];
            Pyd34(fdIdx:end)=[];
            Pyz(fdIdx:end)=[];

            %Pyrite formation rate
            if length(Py)>1
                %Pyrite concentration at Pyrite formation depth
                PyDim=Py*Boundaries.S0/Parameters.fP;
                PyatDepth=PyDim(end);
                dPydt(i)=PyatDepth/100*Parameters.w*Parameters.rho*(1-Parameters.phi);
                %Pyrite isotopic composition at pyrite formation depth
                Pyd(i)=Pyd34(end);
                %if pyrite isotopic composition is nan
                if isnan(Pyd34(end))
                    Pyd34(isnan(Pyd34))=[];
                    Pyd(i)=Pyd34(end);
                end
                %Total grams of pyrite/cm2
                PyWtPercentNonDim=trapz(Pyz,Py);%sum(Py)/numel(Pyz);%sum(Py);;
                %Dimnesional
                PyWtPercent(i)=PyWtPercentNonDim*Parameters.fP*Boundaries.S0/Parameters.fF;
                Pyg(i)=PyWtPercent(i)/100.*Parameters.rho.*(1-Parameters.phi);
            else
                PyWtPercent(i)=NaN;
                dPydt(i)=NaN;
                Pyg(i)=NaN;
                Pyd(i)=NaN;
            end
        end

    catch ME
        fprintf('Profile %1.0f failed',i)
    end

    [warnMsg, warnId] = lastwarn;
    if ~isempty(warnMsg)
        PyWtPercent(i)=NaN;
        dPydt(i)=NaN;
        Pyg(i)=NaN;
        Pyd(i)=NaN;
        fprintf('Profile %1.0f warning',i)
    end
end
stop_time = datetime() % Records the stop time of code execution
save('Results.mat',"PyWtPercent","Pyd","PydDepth","PydDepthDim","dPydt",'Pyg');