%IODP HOLE 1352
TOC=3;%wt%
SO4=28; %mM
w=0.02; %cm/y
DPyrite=3.1;
phi=0.5;
lat=-44.937400;
long=172.022692;

%calculate dependent BC
OPD=calcz0(w);
kgsw=calkSW(w);  
a=0.23;
TInt=6.5; %from interpolating global data
D0=7.12.*TInt+156.5;
DS=D0./(1-2.*log(phi));
fG=10^4.*0.5.*(1/12).*2.65.*((1-(phi))./(phi));
OC=TOC*(a*w./(a*w+kgsw.*OPD)).^a;  %0.26 is parameter a for OC decay  
Gamma=OC*fG./28;
kg0=a./(a./kgsw+OPD./w);  
Da=sqrt(kg0*DS)/w;

%% IODP HOle 1123
TOC=0.8;%wt%
SO4=28; %mM
w=0.004; %cm/y
DPyrite=74.9;
phi=0.75;
lat=-44.937400;
long=172.022692;

%calculate dependent BC
OPD=calcz0(w);
kgsw=calkSW(w);  
a=0.23;
TInt=1.1; %from interpolating global data
D0=7.12.*TInt+156.5;
DS=D0./(1-2.*log(phi));
rho=2.7;
fG=calcfG(phi,rho);
Gamma=OC*fG./28; 
Da=sqrt(kgsw*DS)/w;

%% Average all dataIODP 1353
Data=readtable("Data\Raw_Data\Pasquier_IODP_1352.txt","Delimiter",' ');
%Data(Data.x_34Spyr<0,:)=[];
Gamma=Data.TOC*fG./28;
w=gradient(Data.Depthinborehole,Data.Age)/10^4;
kgsw=calkSW(w);
Da=sqrt(84*kgsw)./w;

%% Average all dataIODP 1123
Data=readtable("Data\Raw_Data\Pasquier_IODP_1123.txt","Delimiter",' ');
%Data(Data.x_34Spyr<0,:)=[];
Gamma=Data.TOC*fG./28;
w=gradient(Data.Depthinborehole,Data.Age)/10^4;
kgsw=calkSW(w);
Da=sqrt(104.*kgsw)./w;
%% Histogram
Da=logspace(-0.7,3.3,40);
Gamma0=logspace(-0.73,2.7,40);
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\HeatMaps\DaG0GlobalPsi\DaG0_GlobalAvergaePhi.mat');
DaPas=sqrt(84*kgsw)./w;
PYd=interp2(Da,Gamma0,Pyd,DaPas,Gamma);
figure
histogram(PYd)
hold on
histogram(Data.x_34Spyr)
