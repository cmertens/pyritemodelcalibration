% This function is defining a global grid used for Global Boundary
% conditions
function [latitude_values,longitude_values]=Define_Global_Grid(Resolution)
% Because most variables are dependent on sedimentation rate, we use data
% from Restreppo et al. 2020 as the input grid.


% Path to the sedimentation rate data file
sed_rate_file_path = 'Data\Raw_Data\Sedimentation_Rates\GlobalSedRates.nc';

% Open the sedimentation rate data file
sed_ncid = netcdf.open(sed_rate_file_path);

% Get latitude and longitude data from the sedimentation rate file
sed_lat = netcdf.getVar(sed_ncid, 0);
sed_lon = netcdf.getVar(sed_ncid, 1);

% Downsample latitude and longitude data to reduce resolution
downsampled_lat = sed_lat(1:Resolution:end)';
downsampled_lon = sed_lon(1:Resolution:end)';

[latitude_values,longitude_values]= meshgrid(downsampled_lat, downsampled_lon);

% Close the sedimentation rate data file
netcdf.close(sed_ncid);
end





