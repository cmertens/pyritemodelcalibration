%Here we import Global Boundary Conditions that are then used to calculate Global rates 
%of Pyrite formation etc.
[lat,long]=Define_Global_Grid(5); %the number determines the coarseness of grid; the higher, the coarser
%if you change this value, you need to re-define the area of the black sea
%for lower sulfate concentrations in the boundary condition
%% Gloabl sedimentation rates
SedRate = Interpolate_SedRate(lat, long,'Restreppo');
WqEuler=SedRate(:);

%% Global Water Depths
waterDepth=Interpolate_WaterDepth(lat,long);

%% Mixed Layer Depth
OXX=calcz0(SedRate);
%%Black sea
%OXX(1240:1350,780:850)=0;
OXX(493:541,316:337)=0;
%OXX(2450:2700,1570:1680)=0;
OPDEuler=OXX(:);

%% Global OC reactivity
kgSW=calkSW(WqEuler); %at water-sediment interface
Parameters=ImportParameters(1);
a=Parameters.a;
kg0=a./(a./kgSW+OPDEuler./WqEuler);
kg0Euler=kg0(:);

%% Global Density
rho=repmat(2.65,size(lat));
rhoEuler=rho(:);
%% Global sediment thickness
Sedz=Interpolate_Sed_Thickness(lat,long);
sedEuler=Sedz(:);
%% Temperature
TInt=Interpolate_Seafloor_T(lat,long);
TEuler=TInt(:);

%% Porosity
Pq=Interpolate_Porosity(lat,long,'Martin');
PqEuler=Pq(:);


%% Diffusivity
D0global=7.12.*TInt+156.5;
DH0Global=600+12.1.*TInt;  %H2S Diffusivity
DSglobal=D0global./(1-2.*log(Pq));
DSEuler=DSglobal(:);

DHGlobal= DH0Global./(1-2.*log((Pq)));
DHEuler=DHGlobal(:);
%% Organic carbon
OCSW=Interpolate_OC(lat,long,'HayesandParadis');
OCSW=OCSW(:);
%new OC at z0(oxygen penetration depth)
OCAll=OCSW.*(a.*WqEuler./(a.*WqEuler+kgSW(:).*OPDEuler)).^a;  %0.26 is parameter a for OC decay  
OCAll(OCAll<0)=NaN;
OCallR=reshape(OCAll,size(DHGlobal));

%% Reactive Iron
FeHR=Interpolate_FeHR(lat,long);
FeHrAll=FeHR(:);

%% Make map of global sulfate concentrations
SO4AllR=repmat(28,size(lat));
%For fine grids
%SO4AllR(1240:1350,780:850)=16;
SO4AllR(493:541,316:337)=16;
%SO4AllR(2450:2700,1570:1680)=16;
SO4All=SO4AllR(:);

%% Gamma0
fG=10^4.*0.5.*(1/12).*rho.*((1-(Pq))./(Pq));
Gamma=OCallR.*fG./SO4AllR; %Non dimensional OC

%% Psi0
 fF=10^4.*2.*(1/55.845).*rho.*(1- (Pq))./(Pq);
Psi=FeHR.*fF./SO4AllR; %Non dimensional OC

%% For Euler
save('Variables.mat','PqEuler','sedEuler','WqEuler','kg0Euler','DHEuler','DSEuler','OCAll',"FeHrAll",'SO4All','rhoEuler','OPDEuler','TEuler');
