function fP=calcfP(phi,rho)
%calculate scaling term to convert from solid-phase wt% Pyrite in sediment
%to dissolved phase mM SO4 2-
%phi= porosity [fraction]
%rho= density [g cm-3]
%------------------------------------------------------------------------
%wt% FeS2 to mol conversion factor
L_P=1/(100*119.98);
%reaction stochiometry conversion factor
L_S=10^6*2;
%scaling term
fP=L_P*L_S*rho*(1-phi)/phi;
end