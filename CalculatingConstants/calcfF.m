function fF=calcfF(phi,rho)
%calculate scaling term to convert from solid-phase wt% FeHR in sediment
%to dissolved phase mM S-II
%phi= porosity [fraction]
%rho= density [g cm-3]
%------------------------------------------------------------------------
%wt% FeHR to mol conversion factor
L_I=1/(100*55.49);
%reaction stochiometry conversion factor
L_F=10^6*2;
%scaling term
fF=L_I*L_F*rho*((1- phi)/ phi);
end