%calculates carbon reactivity at the sediment water interface
function kSW=calkSW(w)
kSW=0.0490.*w.^1.5062;
end