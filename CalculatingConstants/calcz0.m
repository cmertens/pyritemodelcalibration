function z0=calcz0(w)
fitmdl=OPD_Predictors(0);
OPD=10.^(fitmdl(2)*log10(w)+fitmdl(1));% in cm; 
z0=10*OPD;
end