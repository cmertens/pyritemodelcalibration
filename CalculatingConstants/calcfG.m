function fG=calcfG(phi,rho)
%calculate scaling term to convert from solid-phase wt% OC in sediment
%dissolved phase mM SO4 2-
%phi= porosity [fraction]
%rho= density [g cm-3]
%------------------------------------------------------------------------
%wt% OC to mol conversion factor
L_C=1/(100*12);
%reaction stochiometry conversion factor
L_G=10^6/1.7;
%scaling term
fG=L_G*L_C*rho*(1-phi)/phi;
end