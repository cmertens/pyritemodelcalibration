%This routine allows you to model all relevant species for a given set of
%parameters and boundary conditions, which you can specify below.

%% add folders to path
addpath('BVPFunctions');
addpath('CalculatingConstants');
addpath('ODE')
addpath('SolverFunctions')
addpath('Plotting_functions')
addpath('NonDimensionalizeFunctions')

%% user inputs

% define parameters
rho = 2.7; %sediment solid phase density [g cm-3]
phi = 0.5; %Porosity [fraction]
T = 6.5; %Bottom water temperature [°C]
w = 0.02; %sedimentation rate [cm y-1]

% define boundary conditions
G0 = 10; % Organic carbon [wt%]
S0 = 28; % Sulfate [mM]
F0 = 2; % Reactive Iron [wt%]
P0 = 0; % Pyrite [wt%]
d34S0 = 21; % delta34S sulfate

dimDepth = 200; % define Depth in cm

%% initialize parameters and boundaries

Parameters = struct();
Parameters.rho = rho;
Parameters.phi = phi;
Parameters.T = T;
Parameters.w = w;

% constants
Parameters.a = 0.23; % constant for OC decay
Parameters.b = 0.17; % constant for Fe decay
Parameters.Ks = 1.62; % Monod coefficient for sulfate
Parameters.Kh = 1.62; % Monod coefficient for H2S

% calculate dependent parameters
Parameters.fG = calcfG(Parameters.phi, Parameters.rho); %to go from wt%OC to mM SO4
Parameters.fF = calcfF(Parameters.phi, Parameters.rho); %to go from wt%Fe to mM S-II
Parameters.fP = calcfP(Parameters.phi, Parameters.rho); %to go from wt%Pyrite to mM S-II
Parameters.kg0 = calkSW(Parameters.w); % time-dependent OC reactivity constant
D0 = 7.12 * Parameters.T + 156.5; % SO4 diffusivity
Parameters.DS = D0 / (1 - 2 * log(Parameters.phi));
DH0 = 600 + 12.1 * Parameters.T;  % S-II Diffusivity
Parameters.DH = DH0 / (1 - 2 * log(Parameters.phi));

Boundaries = struct();
Boundaries.G0 = G0;
Boundaries.S0 = S0;
Boundaries.F0 = F0;
Boundaries.P0 = P0;
Boundaries.d34S0 = d34S0;

%% Calculate Non-dimensional Parameters and Solver Grid

NonDimParameters = makeNonDimParameters(Parameters, Boundaries);
NonDimBoundaries = makeNonDimBoundaries(Parameters, Boundaries);
NonDimBoundaries.Eta0 = 0; %S-II concentration

maxDepth = dimDepth * sqrt(Parameters.kg0 / Parameters.DS); % convert to non-dimensional depth
zetamesh = linspace(0, maxDepth, 300); % solver grid

%% Calculate Solution

ModelSolution = ModelSolverNonDim(NonDimBoundaries, NonDimParameters, zetamesh);

%% Plot Solution

% Non-dimensional plot
makeconceptualplot(ModelSolution, 'k');

% Dimensional plot
makeconceptualplot_dim(ModelSolution, Parameters, Boundaries, 'k');
