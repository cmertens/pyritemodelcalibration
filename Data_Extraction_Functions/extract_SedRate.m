clear variables; close all;clc
%% Sed Rates from Restreppo et al 2020
sed_rate_file_path = 'Data\Raw_Data\Sedimentation_Rates\GlobalSedRates.nc';

% Open the sedimentation rate data file
sed_ncid = netcdf.open(sed_rate_file_path);

% Get latitude and longitude data from the sedimentation rate file
sed_lat = netcdf.getVar(sed_ncid, 0);
sed_lon = netcdf.getVar(sed_ncid, 1);

raw_SedRate_Restreppo.lat=sed_lat;
raw_SedRate_Restreppo.long=sed_lon;
raw_SedRate_Restreppo.SedRate=10.^double(netcdf.getVar(sed_ncid,2));

save("Data\Processed_Data\Global_Data\SedRate_Restreppo.mat","raw_SedRate_Restreppo")


% Close the sedimentation rate data file
netcdf.close(sed_ncid);

%% Sedimentation rate by Bowles et al 2014
%NPP
file_path = 'Data/Raw_Data/ChlorophyllA/SEASTAR_SEAWIFS_GAC.19970904_20101130.L3m.CU.CHL.chlor_a.9km.nc';
% Open the sedimentation rate data file
sed_ncid = netcdf.open(file_path);
Chlorq= ncread(file_path,'chlor_a');
datalong=ncread(file_path,'lon');
datalat=ncread(file_path,'lat');


[LAT,LONG]=meshgrid(datalat,datalong);

%import waterDepth
WaterDepth=Interpolate_WaterDepth(LAT, LONG);

wBowles=10.^(0.764 + 0.838*log10(Chlorq) + 0.090*log10(Chlorq).*log10(WaterDepth))/1000;

raw_SedRate_Bowles.lat=LAT;
raw_SedRate_Bowles.long=LONG;
raw_SedRate_Bowles.SedRate=wBowles;

save("C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Global_Data\SedRate_Bowles.mat","raw_SedRate_Bowles")

%%Verify their relationship with data from mueller and suess

 Data_MandSuess=tdfread('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\Sedimentation_Rates\Surface_data_sed_acc_rate_TOC.tab');
 PP=log10(Data_MandSuess.PP_C_area_0x5Bg0x2Fm0x2A0x2A20x2Fa0x5D);
 Depth=log10(Data_MandSuess.Depth_bot_0x5Bm0x5D);
 AccRate=log10(Data_MandSuess.SR_0x5Bcm0x2Fka0x5D);
 T=[PP PP.*Depth];


%% Sedimentation rates accroding to Egger 2018
%NPP
file_path = 'Data/Raw_Data/ChlorophyllA/SEASTAR_SEAWIFS_GAC.19970904_20101130.L3m.CU.CHL.chlor_a.9km.nc';
% Open the sedimentation rate data file
sed_ncid = netcdf.open(file_path);
Chlorq= ncread(file_path,'chlor_a');
datalong=ncread(file_path,'lon');
datalat=ncread(file_path,'lat');


[LAT,LONG]=meshgrid(datalat,datalong);
distances=Interpolate_Dist2Coast(LAT,LONG);

%import waterDepth
WaterDepth=Interpolate_WaterDepth(LAT, LONG);

%use formula from egger2018
wEgg=1.49.*10.^(-0.98193-0.27933*log10(WaterDepth)-0.60214*log10(distances)+0.61702*log10(Chlorq));


raw_SedRate_Egger.lat=LAT;
raw_SedRate_Egger.long=LONG;
raw_SedRate_Egger.SedRate=wEgg;

save("Data\Processed_Data\Global_Data\SedRate_Egger.mat","raw_SedRate_Egger")

%% Sed rates from Paradis 2023
DataSarah=readtable('Data\Raw_Data\TOC\mosaic_downcoretoc_sar_c14_conni_2023-02-02');

  coreId=unique(DataSarah.core_id);
for i=1:numel(coreId)
    loopId=coreId(i);
    Sed=DataSarah.SAR_cm_yr(DataSarah.core_id==loopId);
    SedRate(i)=mean(Sed,'omitnan');
end    

raw_SedRate_Paradis.lat=DataSarah.latitude;
raw_SedRate_Paradis.long=DataSarah.longitude;
raw_SedRate_Paradis.SedRate=SedRate;

save("Data\Processed_Data\Global_Data\SedRate_Paradis.mat","raw_SedRate_Paradis")



