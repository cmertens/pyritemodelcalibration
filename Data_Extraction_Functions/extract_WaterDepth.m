%% Waterdepth from Sattelite Data 

waterpath='Data\Raw_Data\WaterDepth\WaterDepths.grd';
waterncid=(netcdf.open(waterpath));
waterDepth=double(netcdf.getVar(waterncid,2));
waterDepth=waterDepth(1:2:end,1:2:end);
waterLat=netcdf.getVar(waterncid,1);
waterLat=waterLat(1:2:end);
waterLong=netcdf.getVar(waterncid,0);
waterLong=waterLong(1:2:end);


[Latwater,Longwater] = meshgrid(waterLat,waterLong);
waterDepth(waterDepth>0)=0;

raw_WaterDepth.lat=Latwater;
raw_WaterDepth.long=Longwater;
raw_WaterDepth.waterDepth=-waterDepth;

save("Data\Processed_Data\Global_Data\waterDepth.mat","raw_WaterDepth")


