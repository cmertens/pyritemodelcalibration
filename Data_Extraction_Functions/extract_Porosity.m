close all; clear variabls;clc

%% Porosity from Martin et el. 2015

poroscid=netcdf.open('Data\Raw_Data\Porosity\Porosity.grd');
% Get the number of variables in the NetCDF file

porosity=double(netcdf.getVar(poroscid,2));
porLong=netcdf.getVar(poroscid,0);
porLat=netcdf.getVar(poroscid,1);
[Xpor,Ypor] = meshgrid(porLat,porLong);

raw_Porosity_Martin.lat=Xpor;
raw_Porosity_Martin.long=Ypor;
raw_Porosity_Martin.Porosity=porosity/100;

save("Data\Processed_Data\Global_Data\Porosity_Martin.mat","raw_Porosity_Martin")


%% Porosity from Lee et al 2019
OCpath='Data\Raw_Data\Porosity\GlobalPhiLee2019.txt';
Data= readtable(OCpath);
LongLee=Data.Var1;
LatLee=Data.Var2;
PhiLee=Data.Var3;

raw_Porosity_Lee.lat=LatLee;
raw_Porosity_Lee.long=LongLee;
raw_Porosity_Lee.Porosity=PhiLee/100;

save("Data\Processed_Data\Global_Data\Porosity_Lee.mat","raw_Porosity_Lee")


