function [t,d34Py]=extract_delta34Py

%% Data from Canfield&Farquar 2009
%read in Data
Data=readtable('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\d34Py\sd1.xls');
%extract time
tidx1=contains(Data.Properties.VariableNames,'time');
tidx2=contains(Data.Properties.VariableNames,'Age');
tidx=logical(tidx1+tidx2);
timeall=Data(:,tidx);
Ttimeall=timeall(:,2:end);
Ttimeall(:,[78:81])=[];
T=table2array(Ttimeall);
T=T(:);
dPyidx=contains(Data.Properties.VariableNames,'d34');
dPyall=Data(:,dPyidx);
dPyall(:,71)=[];
dPyall(:,[77:81])=[];
dPy=table2array(dPyall);
dPy=dPy(:);
dPy(T>0.6)=[];
T(T>0.6)=[];
T=T*1000;
t=T;
d34Py=dPy;
% Data from Stanford Databas SGP (farell et al)
DataS=readtable('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\d34Py\SGP.csv');
t=[t; DataS.interpretedAge];
d34Py=[d34Py; DataS.Delta34S_py_permil_];

%% datapoints from Whittaker 1990
Age=[79 80 81 82 83 84 85 86]';
dPy=[5.8 -2.9 -3.5 4.7 9.0 -14.3 -8.4 -19.0]';
d34Py=[d34Py; dPy];
t=[t; Age];
%% Datapoints from Gautier 1985b
dPyG=[-29.49
 -24.40
-22.82
-27.25
-22.19
-27.11
-20.49
-19.35
-18.81
-8.08
-23.77
-22.63
-25.37
-25.85
-18.53
-23.03
-27.40
-26.54
-21.51
-28.60
-28.46
-25.24
-17.46
-10.60
-28.64
-14.19
-7.64
-18.24
16.75
-20.37
-19.76
10.49
-10.21
-10.76
-14.74
-10.83
-13.50
-34.58
13.97
-12.71
-16.28
-11.90
-9.52
-33.16
-34.42
-34.03
-34.68];
AgeG=[repmat(90,11,1);repmat(105,8,1); repmat(95,4,1);repmat(80,20,1);repmat(88,4,1)] ;
d34Py=[d34Py; dPyG];
t=[t; AgeG];
%% Dinur et al 1980
PyD=[-22.5
    -23.8
    -19.4
    6.1];
d34Py=[d34Py; PyD];
t=[t; repmat(68,4,1)];
%% Bottrell&Raiswell 1989
dPyB=[-41 -37 -37 -37 -37 -40 -37 -44 -42 -41 -37 -41]';
d34Py=[d34Py; dPyB];
t=[t; repmat(195,12,1)];

end