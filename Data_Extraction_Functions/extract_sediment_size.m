
    % Open the txt file containing sediemnt size
    file_path = 'Data\Raw_Data\Sediment_Size\g00127interval.txt';
    T=readtable(file_path);
    Tlatlong=readtable('Data\Raw_Data\Sediment_Size\g00127sample.txt');
    % Read latitude, longitude, and distance to coast data from the netCDF file
    sed_lat = Tlatlong.Var20;
    sed_lon = Tlatlong.Var21;
    sed_ID_latlong=Tlatlong.Var4;
    sed_size = T.Var36;
    sed_Size_ID=T.Var4;

    [sed_Size_ID,ia]=unique(sed_Size_ID);
    sed_size=sed_size(ia);

     [sed_ID_latlong,ia]=unique(sed_ID_latlong);
    sed_lat=sed_lat(ia);
    sed_lon=sed_lon(ia);

    idx=ismember(sed_ID_latlong,sed_Size_ID);
    sed_lat=(sed_lat(idx));
    sed_lon=sed_lon(idx);
    sed_lat(isnan(sed_size))=[];
    sed_lon(isnan(sed_size))=[];
    sed_size(isnan(sed_size))=[];

    raw_SedSize.lat=sed_lat;
    raw_SedSize.long=sed_lon;
    raw_SedSize.SedSize=sed_size;
    
save("Data\Processed_Data\Global_Data\SedSize.mat","raw_SedSize")