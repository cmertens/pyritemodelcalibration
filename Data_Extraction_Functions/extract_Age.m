
%% Data from Paradis et al 2023
    %C14 ages
    Data = readtable('Data\Raw_Data\TOC\mosaic_downcoretoc_sar_c14_conni_2023-02-02');
    
    coreId=unique(Data.core_id);
for i=1:numel(coreId)

    loopId=coreId(i);
    Age=Data.Age_14C_ybp(Data.core_id==loopId);
    Ageall(i)=mean(Age,'omitnan');
end    
    Raw_Age_Paradis.lat=Data.latitude;
    Raw_Age_Paradis.long=Data.longitude;
    Raw_Age_Paradis.Age=Ageall;

    save("Data\Processed_Data\Global_Data\Age_Paradis.mat","Raw_Age_Paradis")

    