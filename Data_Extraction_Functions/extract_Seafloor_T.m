%% Ocean temperature from NOAA sattelite data

Tlat=ncread('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\Temperature\data_2015.nc4','lat');
Tlon=ncread('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\Temperature\data_2015.nc4','lon');
TBW=ncread('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\Temperature\data_2015.nc4','water_temp_bottom');
[TLAT,TLON]=meshgrid(Tlat,Tlon);
Raw_T.lat=TLAT;
Raw_T.long=TLON;
Raw_T.T=TBW;

    save("Data\Processed_Data\Global_Data\Seafloor_Temperature.mat","Raw_T")