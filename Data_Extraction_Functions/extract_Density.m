close all; clear variabls;clc
%extract density based on sediment type, classified by Diesing 2020
Data=dbfread('Data\Raw_Data\Deep-sea_sediments_5_classes\SimpClass_DuplicatesRemoved_deepsea500.dbf');
Lat=Data(:,1);
Lat=cell2mat(Lat);
Long=Data(:,2);
Long=cell2mat(Long);

Type=Data(:,4);
Density=nan(1,length(Type));
%assign densities
Calcidx=strcmp(Type,'Calc.Sed');
Density(Calcidx)=2.71;

Clayidx=strcmp(Type,'Clay');
Density(Clayidx)=2.6;

Diaoozeidx=strcmp(Type,'Dia.Ooze');
Density(Diaoozeidx)=2.6;

Radioidx=strcmp(Type,'Rad.Ooze');
Density(Radioidx)=2.6;

Lithidx=strcmp(Type,'Lith.Sed');
Density(Lithidx)=2.65;


raw_Density.lat=Lat;
raw_Density.long=Long;
raw_Density.Density=Density;

save("Data\Processed_Data\Global_Data\Density.mat","raw_Density")


