%% Data from Jorgensen et al 2022
T=readtable('Data\Raw_Data\DOU\JorgensenData.xlsx');
T(1:3,:)=[];
T(isnan(T.Lat_N_S_),:)=[];
T(isnan(T.Long_E_W_),:)=[];

raw_TOU.lat=T.Lat_N_S_;
raw_TOU.long=T.Long_E_W_;
raw_TOU.TOU=T.TOU;

save("Data\Processed_Data\Global_Data\TOU.mat","raw_TOU")
