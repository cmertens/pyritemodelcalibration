function [Data,Depth]=removeNANs(InputData,NaNDepth,depthoffset)
idx=~isnan(InputData); %search for input data
NewData=InputData(idx);
NewDepth=NaNDepth(idx);
NewDepth=NewDepth-depthoffset;
if any(NewDepth>0)
Data=NewData(find(NewDepth==0|NewDepth>0));
Depth=NewDepth(find(NewDepth==0|NewDepth>0));
if(isempty(Data))
   Data=NaN;
   Depth=NaN;
end
else
Data=NaN;
Depth=NaN;
end

end