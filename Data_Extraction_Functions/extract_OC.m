
% extract_OC: Extracts total organic carbon (OC) data from different
% literature sources and stores value as a structure in the Data folder for
% faster accesibility
%% Data from Paradis et al 2023

    Data = readtable('Data\Raw_Data\TOC\mosaic_surfacetoc_conni_2023-10-03.csv');
    
    Raw_OC_Paradis.lat=Data.latitude;
    Raw_OC_Paradis.long=Data.longitude;
    Raw_OC_Paradis.OC=Data.total_organic_carbon__;

    save("Data\Processed_Data\Global_Data\OC_Paradis.mat","Raw_OC_Paradis")

    %% Data from Hayes et al 2021

    HayesTable = readtable('Data\Raw_Data\TOC\Iron_andOC_Hayes2021.txt');
    TOCHayes = HayesTable.TOC;
    LatHayes = HayesTable.latitude;
    LongHayes = HayesTable.longitude;
    LongHayes(TOCHayes == -999) = [];
    LatHayes(TOCHayes == -999) = [];
    TOCHayes(TOCHayes == -999) = [];
 
    Raw_OC_Hayes.lat=LatHayes;
    Raw_OC_Hayes.long=LongHayes;
    Raw_OC_Hayes.OC=TOCHayes;
    
    save("Data\Processed_Data\Global_Data\OC_Hayes.mat","Raw_OC_Hayes")


   
    %% Data from Seiter et al 2005

     % Read data
    OCpath = 'Data\Raw_Data\TOC\TOC_Seiteretal2004.asc';
    [Data, RefMat] = arcgridread(OCpath);

    % Calculate size and grids
    [nrows, ncols, ~] = size(Data);
    [row, col] = ndgrid(1:nrows, 1:ncols);
    [olat, olon] = pix2latlon(RefMat, row, col);
    Data(Data>30)=NaN;
    Raw_OC_Seiter.lat=olat;
    Raw_OC_Seiter.long=olon;
    Raw_OC_Seiter.OC=Data;
    
    save("Data\Processed_Data\Global_Data\OC_Seiter.mat","Raw_OC_Seiter")
    %% Data from Atwood et al 2020


    % extract_mean_carbon_stock: Extracts mean carbon stock data

    % Define the path to the GeoTIFF file
    OCpath = 'Data\Raw_Data\TOC\Mean carbon_stock.tif';

    % Read the GeoTIFF file
    t = Tiff(OCpath);
    OC = read(t);
    OC=OC(1:5:end,1:5:end);
    % Create longitude and latitude grids
    xlong = linspace(-180, 180, size(OC, 2))';
    xlat = linspace(80, -80, size(OC, 1))';
    [OClat,OClong]=meshgrid(xlat,xlong);


    % Calculate back to wt%OC according to Atwood et al. 2020
    OC(OC < 0) = NaN;
    int = OC ./ 8610;
    OCwt = nthroot(int, 0.601); % as %

    Raw_OC_Atwood.lat=OClat;
    Raw_OC_Atwood.long=OClong;
    Raw_OC_Atwood.OC=OCwt';
    
    save("Data\Processed_Data\Global_Data\OC_Atwood.mat","Raw_OC_Atwood")
  
    %% Data froom Lee et al 2019

    % Define the path to the text file containing OC data
    OCpath = 'Data\Raw_Data\TOC\GlobalOCLee2019.txt';

    % Read data from the text file
    Data = readtable(OCpath);
    LongLee = Data.Var1;
    LatLee = Data.Var2;
    OCLee = Data.Var3;

    Raw_OC_Lee.lat=LatLee;
    Raw_OC_Lee.long=LongLee;
    Raw_OC_Lee.OC=OCLee';
    
    save("Data\Processed_Data\Global_Data\OC_Lee.mat","Raw_OC_Lee")

