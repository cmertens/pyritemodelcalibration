
    % extract_distance_to_coast: Extracts the distance to the nearest coast based on latitude and longitude values

    % Open the netCDF file containing distance to coast data
    file_path = 'Data\Raw_Data\DistanceNearestCoast\dist2coast_4deg.nc';
    file_id = netcdf.open(file_path);

    % Read latitude, longitude, and distance to coast data from the netCDF file
    sed_lat = netcdf.getVar(file_id, 1);
    sed_lon = netcdf.getVar(file_id, 2);
    sed_d = double(netcdf.getVar(file_id, 0));

    % Create a meshgrid of latitude and longitude values
    [lat_mesh, lon_mesh] = meshgrid(sed_lat, sed_lon);

     raw_Dist.lat=lat_mesh;
    raw_Dist.long=lon_mesh;
    raw_Dist.Dist=sed_d;
    sed_d(sed_d<0)=NaN;
    
save("Data\Processed_Data\Global_Data\Dist2Coast.mat","raw_Dist")

    % Close the netCDF file
    netcdf.close(file_id);

