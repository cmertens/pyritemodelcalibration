
%add values for antarcic oceans (Huang2023)
    FeHRHuang=[  0.0711    0.0979    0.1242    0.3665    0.1818    0.2478    0.3217    0.1371    0.1913    0.3390...
0.3872    0.3430    0.2786    0.1326    0.3273    0.4795    0.7721    0.7049    0.8057    0.8504];
    FeHrlongHuang=[70.8700   70.4900   78.0000   74.1100   71.0500   73.0200   77.1800   72.9200   76.2000   68.0100...
72.9500   70.5200   75.3800   76.1100   75.4900   75.4900   75.6800   77.8200   73.2100   67.8100];
    FeHrlatHuang=-[67.5200   66.9900   66.9400   68.9900   68.4900   66.9700   67.4400   68.0000   68.3800   67.5100...
68.4200   68.0100   67.9600   69.2800   68.9100   67.2500   65.9900   65.4800   64.9000   66.0000];

%add values (Poulton2002)
latRaiswell=[-22.220000;-18.566667];
longRaiswell=[ -175.748333;-177.861667];
FeHRRaiswell=[0.42;1.94];
%Ianni2010
lat_Ianni = [-75.1017, -75.9733, -73.9950, -72.4117, -72.3045, -71.3123, -75.9950, -77.1045, -74.1947];
long_Ianni = [164.475, -177.497, 175.013, 173.087, 170.075, 169.749, -177.273, 166.565, 166.047];
FeIanni=[2.6607, 5.8725, 4.2969, 4.7209, 1.3209, 2.2429, 2.3028, 2.4761, 2.2419];
FeIanni=0.1*FeIanni;
%Dicen2019 Indonesia
latDicen=[12.057665 12.310028 10.306352 15.349959 14.697398 11.029708 ];
longDicen=[119.845446 119.823473 124.547594 119.872912 120.186022 123.580798];
FeHRDicen=[0.56 0.4 0.56 0.43 0.5 1.03];

v=[FeHRHuang';FeHRRaiswell;FeIanni';FeHRDicen'];
x=[(FeHrlatHuang)';(latRaiswell);(lat_Ianni');(latDicen)'];
y=[(FeHrlongHuang)';(longRaiswell);(long_Ianni');(longDicen)'];

raw_FeHR.lat=x;
raw_FeHR.long=y;
raw_FeHR.FeHR=v;

save("Data\Processed_Data\Global_Data\FeHR.mat","raw_FeHR")


