    % extract_dust_input: Extracts dust input based on latitude and longitude values
    % check with Giovanni NASA sensor for higher resolution
    % Read dust data from text file
    T = readtable('Data\Raw_Data\Dust\OpticalDepth.txt');
    dust_lat = table2array(T(2:end, 1));
    dust_lon = table2array(T(1, 2:end));
    dust_data = table2array(T);
    dust_data = dust_data(2:end, 2:end);
    dust_data(dust_data == 99999) = NaN;

    % Create meshgrid of longitude and latitude values
    [Lon, Lat] = meshgrid(dust_lon', dust_lat);
     
     raw_Dust.lat=Lat;
    raw_Dust.long=Lon;
    raw_Dust.Dust=dust_data;
    
save("Data\Processed_Data\Global_Data\Dust.mat","raw_Dust")

