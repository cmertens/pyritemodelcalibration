%% Data from Jorgensen et al 2022
T=readtable('Data\Raw_Data\DOU\JorgensenData.xlsx');
T(1:3,:)=[];
T(isnan(T.Lat_N_S_),:)=[];
T(isnan(T.Long_E_W_),:)=[];

raw_OPD.lat=T.Lat_N_S_;
raw_OPD.long=T.Long_E_W_;
raw_OPD.OPD=T.OPD;

save("Data\Processed_Data\Global_Data\OPD.mat","raw_OPD")
