
% extract_FeTot: Extracts total iron concentrationdata based on latitude and longitude values
%% Load Data from Hayes 2021
HayesTable=readtable('Data\Raw_Data\Iron\Iron_andOC_Hayes2021.txt');
FeHayes=HayesTable.Fe;
LatHayes=HayesTable.latitude;
LongHayes=HayesTable.longitude;
LongHayes(FeHayes==-999)=[];
LatHayes(FeHayes==-999)=[];
FeHayes(FeHayes==-999)=[];
FeHayes(FeHayes<0)=[];
%interpolating total iron from Hayes 2021 to measured profile and comparing
%values


raw_FeTot.lat=LatHayes;
raw_FeTot.long=LongHayes;
raw_FeTot.FeTot=FeHayes;

save("Data\Processed_Data\Global_Data\FeTot.mat","raw_FeTot")


