sedid=netcdf.open('Data\Raw_Data\Sediment Thickness\GlobSed_v2.nc');
sedZ=double(netcdf.getVar(sedid,2));
sedLong=netcdf.getVar(sedid,0);
sedLat=netcdf.getVar(sedid,1);

[Lat,Long] = meshgrid(sedLat,sedLong);

raw_Thickness.lat=Lat;
raw_Thickness.long=Long;
raw_Thickness.Thickness=sedZ;

save("Data\Processed_Data\Global_Data\Sediment_Thickness.mat","raw_Thickness")