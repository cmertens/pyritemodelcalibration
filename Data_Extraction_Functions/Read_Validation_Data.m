clear variables;close all;clc
%% Read and process data from individual Excel sheets for each site.

files = dir('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Validation\Digitized_Profiles\*.xlsx');
numFiles = numel(files);

% Preallocate arrays for speed
Pyrite = NaN(500, numFiles);
FeHR = NaN(500, numFiles);
FeTot = NaN(500, numFiles);
H2S = NaN(500, numFiles);
Depth = NaN(500, numFiles);
SO4 = NaN(500, numFiles);
CH4 = NaN(500, numFiles);
SRR = NaN(500, numFiles);
dSO4 = NaN(500, numFiles);
dH2S = NaN(500, numFiles);
dPy = NaN(500, numFiles);
OC = NaN(500, numFiles);
Porosity = NaN(500, numFiles);
SedRate = NaN(500, numFiles);
Age = NaN(500, numFiles);
Ref = cell(1, numFiles);
Names = cell(1, numFiles);
RefNames = cell(1, numFiles);
lat = NaN(1, numFiles);
long = NaN(1, numFiles);
WaterDepth = NaN(1, numFiles);


% Read data from each file
for i = 1:numFiles
    filename = fullfile('Data\Validation\Digitized_Profiles\', files(i).name);
    opts = detectImportOptions(filename);
    varNamesToDouble = opts.VariableNames(2:end);

    % Set variable types to double for the specified range
    opts = setvartype(opts, varNamesToDouble, 'double');
    data = readtable(filename,opts);

    % Extract data from the table
    Pyrite(1:numel(data.S_py_wt_), i) = data.S_py_wt_;
    FeHR(1:numel(data.Fe_HR_wt_), i) = data.Fe_HR_wt_;
    FeTot(1:numel(data.Fe_tot_wt_), i) = data.Fe_tot_wt_;
    H2S(1:numel(data.H2S_mM), i) = data.H2S_mM;
    Depth(1:numel(data.depth_cm), i) = data.depth_cm;
    SO4(1:numel(data.SO4_mM), i) = data.SO4_mM;
    CH4(1:numel(data.CH4_mM), i) = data.CH4_mM;
    SRR(1:numel(data.lnSRR_molL_1d_1), i) = data.lnSRR_molL_1d_1;
    dSO4(1:numel(data.d34S_SO4), i) = data.d34S_SO4;
    dH2S(1:numel(data.d34S_H2S), i) = data.d34S_H2S;
    Age(1:numel(data.age_yr),i)=data.age_yr;
    dPy(1:numel(data.d34S_py), i) = data.d34S_py;
    OC(1:numel(data.TOC_wt_), i) = data.TOC_wt_;
    Porosity(1:numel(data.porosity_vol_), i) = data.porosity_vol_;
    SedRate(1:numel(data.sed_rate_cmky), i) = data.sed_rate_cmky;
    Ref{i} = data.reference{1};
    Names{i} = files(i).name;
    RefNames{i} = [data.reference{1}, '  ', files(i).name];
    lat(i) = data.latitude_dd(1);
    long(i) = data.longitude_dd(1);
    WaterDepth(i) = data.water_depth_m(1);
    
  
end

% Sort data
[Ref, sortIdx] = sort(Ref);
RefNames = RefNames(sortIdx);
Names = Names(sortIdx);
WaterDepth = WaterDepth(sortIdx);
lat = lat(sortIdx);
long = long(sortIdx);
Pyrite = Pyrite(:, sortIdx);
FeHR = FeHR(:, sortIdx);
FeTot = FeTot(:, sortIdx);
SO4 = SO4(:, sortIdx);
H2S = H2S(:, sortIdx);
CH4 = CH4(:, sortIdx);
SRR = SRR(:, sortIdx);
Age = Age(:, sortIdx);
OC = OC(:, sortIdx);
Depth = Depth(:, sortIdx);
Porosity = Porosity(:, sortIdx);
SedRate = SedRate(:, sortIdx);
SedRate = SedRate ./ 1000; % Convert sed rate to cm/y
dSO4 = dSO4(:, sortIdx);
dH2S = dH2S(:, sortIdx);
dPy = dPy(:, sortIdx);


%% Read additional information from the large Excel sheet (Profiles_Additional_Info)
% Note that these are a combination of boundary conditions at the
% sediment-water interface(no depth profiles) as well as additional
% variables such as sedimentation rates
Additional_Data=readtable('Data\Validation\Profiles_Additional_Info.xlsx');
SedRate_Additional=Additional_Data.Sed_rate_cm_ky_/1000; %in cm/year
Porosity_Additional=Additional_Data.Porosity___/100; %in fraction
OC_Additional=Additional_Data.OC_0_Wt_;
Age_Additional=Additional_Data.C14Age_ka_*1000; %in years
z0=Additional_Data.z0_depthAtWhichSO4ConsumtionStarts_;
CH4_Additional=Additional_Data.CH4;
SteadyState=Additional_Data.SteadyState;
Comment=Additional_Data.Comment;
Temperature=Additional_Data.bottomSeawaterT__C_;

%% Combine Both for Boundary conditions etc.

OC_Combined = NaN(1,numFiles);
SO4_Combined = repmat(28,1,numFiles);
FeHR_Combined = NaN(1,numFiles);
FeTot_Combined = NaN(1,numFiles);
SedRate_Combined = NaN(1,numFiles);
Porosity_Combined=NaN(1,numFiles);
Age_Combined = NaN(1,numFiles);
z0_Combined= z0;

 %add Data from additional excel file
for i=1:numFiles
%-------------------------------------------------------------------------
% Sulfate
% assume sulfate concentration is 28 mM globally, except for the Black Sea,
% where we assume 16 mM
SO4_Core=removeNANs(SO4(:,i),Depth(:,i),0);
SO4_Combined(i)=max([max(SO4_Core) 28]);
Black_Sea_idx=strcmp(Comment,'Black Sea');
SO4_Combined(Black_Sea_idx)=16;

%-------------------------------------------------------------------------
% Organic Carbon
% take the max value of OC Array within the first 10% of the array
[OC_Core,Depth_OC]=removeNANs(OC(:,i),Depth(:,i),0);
N = length(OC_Core);
        idx = ceil(N * 0.1);
        if numel(OC_Core)>1
            idx = max([ceil(N * 0.1) 1]);
        end
OC_Core= max(OC_Core(1:idx));
if isempty(OC_Core)
OC_Combined(i)=OC_Additional(i);
else
OC_Combined(i)=OC_Core;
end
%-------------------------------------------------------------------------
% Reactive Iron
[FeHR_Core,Depth_FeHR]=removeNANs(FeHR(:,i),Depth(:,i),0);
N = length(FeHR_Core);
        idx = ceil(N * 0.1);
        if numel(FeHR_Core)>1
            idx = max([ceil(N * 0.1) 1]);
        end
FeHR_Core= max(FeHR_Core(1:idx));
if ~isempty(FeHR_Core)
FeHR_Combined(i)=FeHR_Core;
end

% Total Iron
[FeTot_Core,Depth_FeTot]=removeNANs(FeTot(:,i),Depth(:,i),0);
N = length(FeTot_Core);
        idx = ceil(N * 0.1);
        if numel(FeTot_Core)>1
            idx = max([ceil(N * 0.1) 1]);
        end
FeTot_Core= max(FeTot_Core(1:idx));
if ~isempty(FeTot_Core)
FeTot_Combined(i)=FeTot_Core;
end
%-------------------------------------------------------------------------
%Sedimentation Rate

SedRate_Combined(i) = mean(SedRate(:,i),'omitnan');
if isnan(SedRate_Combined(i))
    SedRate_Combined(i)=SedRate_Additional(i);
end

%-------------------------------------------------------------------------
%Porosity
Porosity_Combined(i)=mean(Porosity(:,i),'omitnan');
if isnan(Porosity_Combined(i))
Porosity_Combined(i)=Porosity_Additional(i);
end

%-------------------------------------------------------------------------
%Age
Age_Combined(i)=min(Age(:,i));
if isnan(Age_Combined(i))
Age_Combined(i)=Age_Additional(i);
end
end

%% Add global estimates

SedRate_Combined_withGlobal=SedRate_Combined;
idx=isnan(SedRate_Combined_withGlobal);
SedRate_Combined_withGlobal(idx)=Interpolate_SedRate(lat(idx),long(idx),'Restreppo');

Porosity_Combined_withGlobal=Porosity_Combined;
idx=isnan(Porosity_Combined_withGlobal);
Porosity_Combined_withGlobal(idx)=Interpolate_Porosity(lat(idx),long(idx),'Martin'); 

Density_Combined_withGlobal=Interpolate_Density(lat,long);

T_Combined_withGlobal=Temperature;
idx=isnan(T_Combined_withGlobal);
T_Combined_withGlobal(idx)=Interpolate_Seafloor_T(lat(idx),long(idx));

OC_Combined_withGlobal=OC_Combined;
idx=isnan(OC_Combined_withGlobal);
OC_Combined_withGlobal(idx)=Interpolate_OC(lat(idx),long(idx),'HayesandParadis');

FeHR_Combined_withGlobal=FeHR_Combined;
idx=isnan(FeHR_Combined_withGlobal);
FeHR_Combined_withGlobal(idx)=Interpolate_FeHR(lat(idx),long(idx));

WaterDepth_additional=Interpolate_WaterDepth(lat,long);
WaterDepth_Combined_withGlobal=WaterDepth;
WaterDepth_Combined_withGlobal(isnan(WaterDepth_Combined_withGlobal))=WaterDepth_additional(isnan(WaterDepth_Combined_withGlobal));

SMT_Combined_withGlobal=Interpolate_MTZ(lat, long,SedRate_Combined_withGlobal);

%% Return the processed validation data.
% Save individual excel sheet data to a .mat file
save('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Validation_Data.mat',...
    'Pyrite','FeHR','FeTot','H2S','Depth','SO4','CH4','SRR','dSO4','dH2S', ...
    'dPy','OC','lat','long',...
    'WaterDepth','Porosity','SedRate','Ref','Names','RefNames','Age');

% Save data from the large excel sheet to a .mat file
save('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Validation_Data_Additional_Info.mat',...
    'SedRate_Additional',"Porosity_Additional","OC_Additional", ...
    "SteadyState","CH4_Additional","z0","Age_Additional");

% Save combined data to a .mat file
save('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Validation_Data_Combined.mat','SO4_Combined',...
    'SedRate_Combined',"Age_Combined","Porosity_Combined","FeHR_Combined", ...
    "FeTot_Combined","OC_Combined","z0_Combined",'WaterDepth','Temperature', ...
    'lat','long',"Black_Sea_idx");
% Save combined with global Data to .mat file¨¨

save('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Validation_Data_Combined_withGlobal.mat','SO4_Combined',...
    'SedRate_Combined_withGlobal',"Porosity_Combined_withGlobal","FeHR_Combined_withGlobal", ...
    "FeTot_Combined","OC_Combined_withGlobal","z0_Combined",'WaterDepth','T_Combined_withGlobal', ...
    'lat','long','Density_Combined_withGlobal','T_Combined_withGlobal', ...
    'WaterDepth_Combined_withGlobal','SMT_Combined_withGlobal',"T_Combined_withGlobal");

