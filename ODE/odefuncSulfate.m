function dSdz=odefuncSulfate(z,S,Parameters,Boundaries)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
dSdz=[S(2)                %convert 2.order ODE to system of 1.order ODE
    S(2)/Parameters.Da+Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*S(1)/(Parameters.KappaS+S(1))];
%force derivative to be negative
dSdz(1)=-abs(dSdz(1)); 
end
