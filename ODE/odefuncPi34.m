function dPidz=odefuncPi34(zPi,Parameters,Boundaries,zetaEta,Eta,zetaIso34,EtaIso34)
H=interp1(zetaEta,Eta,zPi);
H34=interp1(zetaIso34,EtaIso34,zPi);
H(H==0)=eps;
H34(H34==0)=eps;
Psi=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*zPi))).^Parameters.b;
dPidz=H34./(Parameters.KappaE+H)*Parameters.chi.*Parameters.Da.*Psi*(Parameters.b./(Parameters.b+Parameters.Da*Parameters.chi*zPi));
end