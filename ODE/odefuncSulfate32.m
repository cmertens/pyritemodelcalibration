
function dSdz=odefuncSulfate32(z,S,Parameters,Boundaries,zetaSigma,Sigma)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
Si=interp1(zetaSigma,Sigma(:,1),z);
Parameters.KappaS=0.5;
dSdz=[S(2)  %d32S
     S(2)/Parameters.Da+Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*Si/(Parameters.KappaS+Si)*(S(1)./(Parameters.alpha*(Si-S(1))+S(1)))];
 end

