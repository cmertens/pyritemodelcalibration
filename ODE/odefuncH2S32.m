
function dHdz = odefuncH2S32(z,H,Parameters,Boundaries,BvPSolutionSulfateIso,zetaEta,Eta)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
Psi=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*z))).^Parameters.b;
SzetaIso=BvPSolutionSulfateIso.x;
Sol34=BvPSolutionSulfateIso.y(1,:);
Sigma34=interp1(SzetaIso,Sol34,z);
dS=interp1(SzetaIso,BvPSolutionSulfateIso.y(4,:),z);
Sol32=BvPSolutionSulfateIso.y(3,:);
Sigma32=interp1(SzetaIso,Sol32,z);
Htot=max([interp1(zetaEta,Eta,z) eps]);
if Htot<Parameters.KappaE
dHdz =[ H(2) %d32H
    1/(Parameters.Delta)*(H(2)/Parameters.Da-Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(Sigma34+Sigma32)/(Parameters.KappaS+Sigma34+Sigma32)*Sigma32/(max([eps Parameters.alpha*Sigma34+Sigma32]))+H(1)/(Parameters.KappaE+Htot)*Psi*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z))];
 else
dHdz =[ H(2) %d32H
    1/(Parameters.Delta)*(H(2)/Parameters.Da-Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(Sigma34+Sigma32)/(Parameters.KappaS+Sigma34+Sigma32)*Sigma32/(max([eps Parameters.alpha*Sigma34+Sigma32]))+H(1)/(Htot)*Psi*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z))];
end
% Source=Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*(Sigma34+Sigma32)/(Parameters.KappaS+Sigma34+Sigma32)*Sigma32/(max([eps Parameters.alpha*Sigma34+Sigma32]));
% Sink=H(1)/(Parameters.KappaE+Htot)*Psi*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z);
% 
% if dHdz(1)>-dS
%     dHdz(1)=-dS;
% elseif Source<Sink
%     dHdz(1)=-abs(dHdz(1));
% end
end

   