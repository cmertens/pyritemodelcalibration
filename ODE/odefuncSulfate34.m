
function dSdz=odefuncSulfate34(z,S,Parameters,Boundaries,zetaSigma,Sigma)
factor34=sqrt(98/96); %factor taking into account different diffusivities for isotopes
Gamma34=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da*factor34.*z))).^Parameters.a;
Si=interp1(zetaSigma,Sigma(:,1),z);
dSdz=[ S(2)   %d34S
    S(2)/(Parameters.Da*factor34)+Gamma34*Parameters.a/(Parameters.a+Parameters.Da*factor34*z)*Si/(Parameters.KappaS+Si)*(Parameters.alpha*S(1)/(Si-S(1)+Parameters.alpha*S(1)))];
 end

 