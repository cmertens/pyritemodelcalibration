
function dHdz = odefuncH2S(z,H,Parameters,Boundaries,zetaSigma,SigmaSol)
Gamma=Boundaries.Gamma0*(Parameters.a./(Parameters.a+(Parameters.Da.*z))).^Parameters.a;
F=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*z))).^Parameters.b;
Sigma=max([1e-10 interp1(zetaSigma,SigmaSol(:,1),z)]);
dS=interp1(zetaSigma,SigmaSol(:,2),z);


dHdz=[H(2)
 1/(Parameters.Delta)*(H(2)/Parameters.Da-Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*Sigma/(Parameters.KappaS+Sigma)+H(1)/(Parameters.KappaE+H(1))*F*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z))];
 
Source=Gamma*Parameters.a/(Parameters.a+Parameters.Da*z)*Sigma/(Parameters.KappaS+Sigma);
Sink=H(1)/(Parameters.KappaS+H(1))*F*Parameters.chi*Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*z);

if dHdz(1)>-dS
    dHdz(1)=-dS;
elseif Source<Sink
    dHdz(1)=-abs(dHdz(1));
end


end

