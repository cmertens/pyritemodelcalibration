function dPidz=odefuncPi32(zPi,Parameters,Boundaries,zetaEta,Eta,zetaIso32,EtaIso32)

H=interp1(zetaEta,Eta,zPi);
H32=interp1(zetaIso32,EtaIso32,zPi);
H(H==0)=eps;
H32(H32==0)=eps;
Psi=Boundaries.Psi0*(Parameters.b./(Parameters.b+(Parameters.Da*Parameters.chi*zPi))).^Parameters.b;
dPidz=H32./(Parameters.KappaE+H)*Parameters.chi.*Psi*Parameters.Da.*(Parameters.b/(Parameters.b+Parameters.Da*Parameters.chi*zPi));
end