function Parameters=ImportParameters(CoreNr)
% Load data compiled from papers and from global estimates
load('Data\Processed_Data\Extracted_Variables\kgSW.mat','kgSW')
load('Data\Processed_Data\Extracted_Variables\kF0.mat','kF0')
load("Data\Processed_Data\Validation_Data_Combined_withGlobal.mat","SedRate_Combined_withGlobal", ...
    'Porosity_Combined_withGlobal','WaterDepth_Combined_withGlobal','Density_Combined_withGlobal','T_Combined_withGlobal','lat','long')

%Assign Values
%% Sedimentation rate [w]
field3='w';value3=SedRate_Combined_withGlobal(CoreNr);
if isnan(value3)
    value3=mean(SedRate_Combined_withGlobal,'omitnan');
end
%value3=Interpolate_SedRate(lat(CoreNr),long(CoreNr),'Restreppo');


%% OC reactivity [kg0]
field1='kgSW';value1=kgSW(CoreNr); %kg0 based on OC degradation profile
%value1=NaN;
if isnan(value1)
    value1=calkSW(value3);% kgo based on relationship with sedimentation rate
end

%% Fe reactivity [kF0]
field2='kF0';value2=value1; %estimating kF0 = kg0;

%% Porosity
field4='phi';value4=Porosity_Combined_withGlobal(CoreNr); %Porosity reported in papers
if isnan(value4)
    value4=mean(Porosity_Combined_withGlobal,'omitnan'); %generic value
end
%value4=Interpolate_Porosity(lat(CoreNr),long(CoreNr),'Martin');
%% Density
value20=2.7;%Density_Combined_withGlobal(CoreNr);

field20='rho';
%% Sulfate diffusivity
Tq=T_Combined_withGlobal(CoreNr);
D0=7.12*Tq+156.5;
field5='D0';value5=D0;
field6='DS';value6=value5./(1-2*log(value4));

%H2S diffusivity
DH0=600+12.1*Tq;  %H2S Diffusivity
field7='DH0';value7=DH0;
field16='DH';value16= DH0/(1-2*log(value4));
field8='Depth';value8=WaterDepth_Combined_withGlobal(CoreNr);
field9='T';value9=Tq;
fG=calcfG(value4,value20);
field10='fG';value10=fG;
fF=calcfF(value4,value20);
field12='fF';value12=fF;
field13='fP';value13=calcfP(value4,value20);
field14='a';value14=0.23;
field15='Ks';value15=1.62; %Monod rate coeffcient
field11='b';value11=0.17;

Parameters=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4,field5,value5,field6,value6,field7,value7,field8,value8,field9,...
    value9,field10,value10,field11,value11,field12,value12,field13,...
    value13,field14,value14,field15,value15,field16,value16,field20,value20);



end

