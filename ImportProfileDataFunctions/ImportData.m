function Data=ImportData(CoreNr)
%Imports Data for each Profile
load('Data\Processed_Data\Validation_Data.mat','OC','SO4','H2S','FeHR','Pyrite','dSO4','dH2S','dPy','Depth')
%load defined z0 for the model:
Parameters=ImportParameters(CoreNr);
% %% z0=Oxygen penetration Depth
z0=calcz0(Parameters.w);
j=CoreNr;

[S,zS]=removeNANs(SO4(:,j),Depth(:,j),z0);%sulfate reduction rate
[G,zG]=removeNANs(OC(:,j),Depth(:,j),z0);%Organic carbon
[H,zH]=removeNANs(H2S(:,j),Depth(:,j),z0);%H2S
[F,zF]=removeNANs(FeHR(:,j),Depth(:,j),z0);%Reactive Iron
[P,zP]=removeNANs(Pyrite(:,j),Depth(:,j),z0); %Pyrite
[d34S,zd34S]=removeNANs(dSO4(:,j),Depth(:,j),z0); %d34S SO4
[d34H,zd34H]=removeNANs(dH2S(:,j),Depth(:,j),z0);%d34S H2S
[d34P,zd34P]=removeNANs(dPy(:,j),Depth(:,j),z0);%d34S Pyrite

field1='G';value1={zG, G};
field2='S';value2={zS, S};
field3='H';value3={zH, H};
field4='F';value4={zF, F};
field5='P';value5={zP, P};
field6='d34S';value6={zd34S, d34S};
field7='d34H';value7={zd34H, d34H};
field8='d34P';value8={zd34P, d34P};
Data=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4,field5,value5,field6,value6,field7,value7,field8,value8);
end
