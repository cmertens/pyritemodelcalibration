function maxDepth=findMaxDepth(Data)
fnD = fieldnames(Data);
for k=1:numel(fnD)
name=fnD{k};
maxDepth(k)=max(Data(1).(fnD{k})); 
end
maxDepth=max(maxDepth);
if isnan(maxDepth)
    maxDepth=1;
end
if maxDepth==0
    maxDepth=1;
end

end