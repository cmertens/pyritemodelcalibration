function Boundaries=ImportBoundaries(Data,CoreNr)
%This function returns boundary conditions for a certain sedimentary core
%% Load Data
load("Data\Processed_Data\Validation_Data_Combined_withGlobal.mat", ...
    "OC_Combined_withGlobal","SO4_Combined","FeHR_Combined_withGlobal")
load("Data\Processed_Data\Validation_Data.mat",'lat','long')

%% Organic Carbon
% take the max value of the first 10% of the measured array
field1='G0';value1=OC_Combined_withGlobal(CoreNr);
%value1=Interpolate_OC(lat(CoreNr),long(CoreNr),'HayesandParadis');
%% Sulfate
field2='S0';value2=SO4_Combined(CoreNr);

%% Reactive Iron
field3='F0';value3=FeHR_Combined_withGlobal(CoreNr);
%value1=Interpolate_FeHR(lat(CoreNr),long(CoreNr));
%% Pyrite
field4='P0';
value4=0;

Boundaries=struct(field1,value1,field2,value2,field3,value3,field4,...
    value4);
end
