%% Validating the non-dimensional equations conceptually and comparing to literature
%clear variables;close all;clc
%% Parallelise
%parpool('local')
%% Preallocate
 %allCost=struct;
%  allResiduals=struct;
% allSolutions = repmat(struct(), 40,40);
%% Loop
%looping through all profiles
for i=1
   % try
    CoreNr=i;
    %% Import Parameters
    Data=ImportData(CoreNr);
    Parameters=ImportParameters(CoreNr);
    Parameters.kg0=Parameters.kgSW;
    Boundaries=ImportBoundaries(Data,CoreNr);
    NonDimParameters=makeNonDimParameters(Parameters,Boundaries);
    [NonDimData]=makeNonDimData(Data,Parameters,Boundaries);
    [NonDimBoundaries]=makeNonDimBoundaries(Parameters,Boundaries);
    S0=Boundaries.S0;
    %%-------------------------------------------------------------------------------------------------------------------------------------
    
    %NonDimBoundaries.Psi0=1;
Da=logspace(-0.7,3.3,40);
 NonDimBoundaries.Gamma0=0.50;%5%Global avergae   --logspace(-0.73,2.7,40);
NonDimBoundaries.Psi0=1;%logspace(-3,2.15,40);
chi0=logspace(-2,2,40);
%Eta=logspace(-4,0,40);
for j=1:40
    j
    for k=1:40
        try
k
maxDepth=5;%max(allSolutions(j,k).SubStruct(1).Pi);
zetamesh=linspace(0,maxDepth,300);
NonDimParameters.Da=Da(j);
%NonDimBoundaries.Eta0=Eta(k);
%NonDimBoundaries.Gamma0=Gamma0(k);
%NonDimBoundaries.Psi0=Psi0(k);
NonDimParameters.chi=chi0(k);
%     try
%     % %     %%
  ModelSolution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,zetamesh);
% % 
 allSolutions(j,k).SubStruct = ModelSolution;
[Pyd(j,k),PydDepth(j,k),PyWtPercent(j,k)]=calcPy(allSolutions(j,k).SubStruct);
%     Handle Errors
         catch ME
            fprintf('Profile %1.0f failed',i)
         end
    end 
end

end   

