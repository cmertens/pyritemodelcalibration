%Near-shore site
Data=readtable("Data\Raw_Data\Pasquier_IODP_1352.txt");
lat=-41.78594599907921;
long=-171.4990107290167;
phi=Interpolate_Porosity(lat,long,'Martin');
rho=2.7;
fG=calcfG(phi,rho);
Gamma=Data.TOC*fG/28;
