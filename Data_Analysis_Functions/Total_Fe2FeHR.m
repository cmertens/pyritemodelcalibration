function fitresult = Total_Fe2FeHR(plotting)
% TOTAL_FE2FEHR explores the relationship between total and reactive iron
% and returns the fitresult.
% Arguments:
%   - plotting: Boolean flag indicating whether to plot the results (default: false)

% Load measured total and reactive iron
load("Data\Processed_Data\Validation_Data.mat", 'FeHR', 'FeTot');

% Initialize arrays for first non-NaN values of FeTot and FeHR
Fe1st = nan(1, size(FeHR, 2));
FeHR1st = nan(1, size(FeHR, 2));

% Find the first non-NaN values of FeTot and FeHR
for i = 1:size(FeHR, 2)
    idx = find(~isnan(FeHR(:, i)), 1);
    if ~isempty(idx)
        Fe1st(i) = FeTot(idx, i);
        FeHR1st(i) = FeHR(idx, i);
    end
end

% Fit a linear model
warning off
[xData, yData] = prepareCurveData(Fe1st, FeHR1st);
ft = fittype('poly1');
[fitresult] = gmregress(xData, yData);

% Plot results if required
if plotting
    f = figure;
    plot(Fe1st(1:150), FeHR1st(1:150), 'o', 'MarkerFaceColor', 'k'); hold on;
    plot(Fe1st(150:180), FeHR1st(150:180), '<', 'MarkerFaceColor', 'b');
    plot(Fe1st(180:205), FeHR1st(180:205), '^', 'MarkerFaceColor', 'm');
    plot(Fe1st(205:end), FeHR1st(205:end), '*', 'MarkerFaceColor', 'r');
    
    % Plot fit
%     ci = predint(fitresult, linspace(0, 8, 100)); 
%     xconf = [linspace(0, 8, 100)'; linspace(8, 0, 100)'];
%     yconf = [ci(:, 1); flipud(ci(:, 2))];
%     fill(xconf, yconf, 'red', 'FaceAlpha', 0.2, 'EdgeColor', 'none');
    plot(linspace(min(xData),max(xData),10),linspace(min(xData),max(xData),10)*fitresult(2)+fitresult(1));
    
    % Add legend, labels, and adjust figure properties
    legend('Mazumdar 2012', 'Peketi 2015', 'Wijsman 2001', 'Zindorf 2019', ...
        sprintf('Fe_{HR} = %0.2f * Fe_{Tot}', fitresult(2)), 'Location', 'northwest');
    ylabel('Iron HR [wt%]');
    xlabel('Iron Total [wt%]');
    ylim([0, 3]);
    set(f, 'Units', 'Inches');
    pos = get(f, 'Position');
    set(f, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)]);
    %print(f, 'Figures/Data_Analysis/FeHR_FeTot.pdf', '-dpdf');
end

end
