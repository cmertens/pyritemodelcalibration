function mdlfit=extractkgSW

% ---------------- Import Data-----------------------------------------------------------------

DataSarah=readtable('Data\Raw_Data\TOC\mosaic_downcoretoc_sar_c14_conni_2023-02-02');
load("Data\Processed_Data\Validation_Data.mat","OC","Depth")
load("Data\Processed_Data\Validation_Data_Combined.mat","SedRate_Combined","OC_Combined",'lat','long')
%% Calculate kgSW for profiles

wall=SedRate_Combined;
kgSW=nan(size(lat));
for i=1:size(OC,2)
    [G,zG]=removeNANs(OC(:,i),Depth(:,i),0);%Organic carbon
    steadystateidx=mean(diff(movmean(G,numel(G/2))));
    w=SedRate_Combined(i);
    if numel(G)>2&&~isnan(w)&&steadystateidx<0&&~isnan(OC_Combined(i))
        G0=OC_Combined(i);
        Parameters=ImportParameters(i);
        %let kg0 be a free parameter
        modelfun = @(b,t) G0*(Parameters.a*w./(Parameters.a*w+b(1).*t)).^Parameters.a;
        beta0 = 1e-10; %initial guess
        mdl = fitnlm(zG,G,modelfun,beta0);
        kgSW(i)=mdl.Coefficients.Estimate(1);
        kgSWerr(i)=mdl.Coefficients.SE;
        if kgSW(i)<0
            kgSW(i)=NaN;
            kgSWerr(i)=NaN;
        end
    end
end

save('Data\Processed_Data\Extracted_Variables\kgSW.mat','kgSW','kgSWerr')
%% Additional Data from Sarah
coreId=unique(DataSarah.core_id);
for i=1:numel(coreId)
    j=size(Depth,2)+i;
    loopId=coreId(i);
    z=DataSarah.sample_depth_average_cm(DataSarah.core_id==loopId);
    OC=DataSarah.total_organic_carbon__(DataSarah.core_id==loopId);

    w=DataSarah.SAR_cm_yr(DataSarah.core_id==loopId);
    w=w(1);
    wall(j)=w;

    latSarah=DataSarah.latitude(DataSarah.core_id==loopId);
    longSarah=DataSarah.longitude(DataSarah.core_id==loopId);
    lat(j)=latSarah(1);
    long(j)=longSarah(1);
   
    steadystateidx=mean(diff(movmean(OC,numel(OC/2))));
    if numel(OC)>2&&~isnan(w)&&w>0&&steadystateidx<0
        %calculate max OC
        N = length(OC);
        idx = ceil(N * 0.1);
        if numel(OC)<1
            idx = max([ceil(N * 0.1) 1]);
        end
        G0 = OC(1:idx);
        G0=max(G0);
        modelfun = @(b,t) G0*(Parameters.a*w./(Parameters.a*w+b(1).*t)).^Parameters.a;
        beta0 = 1e-10;
        mdl = fitnlm(z,OC,modelfun,beta0);
        kgSW(j)=mdl.Coefficients.Estimate(1);
        kgSWerr(j)=mdl.Coefficients.SE;
    else
        kgSW(j)=NaN;
        kgSWerr(j)=NaN;
    end
end

save('Data\Processed_Data\Extracted_Variables\kgSW_combined.mat','kgSW','kgSWerr','lat','long')
%% Regression
[k,w]=prepareCurveData(log10(kgSW),log10(wall));
[kErr,~]=prepareCurveData(log10(kgSWerr),log10(wall));
k(kErr>0)=[];
w(kErr>0)=[];
kErr(kErr>0)=[];
mdlfit=gmregress(w,k); %geometric mean regression
lm=fitlm(w,k);

%% Function for fit
    function y = linear_model(b, x)
        y = b(1) + b(2)*x(:,1);
    end
end