% This function exlores predictive relationships between reactive iron and
% other environmental properties.
close all; clear variables; clc
%% Import total Iron
load("Data\Processed_Data\Global_Data\FeHR.mat","raw_FeHR")
load("Data\Processed_Data\Global_Data\FeTot.mat","raw_FeTot")

% import additional explanatory variables
distance_to_coast = Interpolate_Dist2Coast(raw_FeTot.lat, raw_FeTot.long);
sed_size = Interpolate_SedSize(raw_FeTot.lat, raw_FeTot.long);
dust_input=Interpolate_Dust(raw_FeTot.lat, raw_FeTot.long);
SedRate=Interpolate_SedRate(raw_FeTot.lat, raw_FeTot.long,'Restreppo');
OC=Interpolate_OC(raw_FeTot.lat, raw_FeTot.long,'HayesandParadis');
WaterDepth=Interpolate_WaterDepth(raw_FeTot.lat, raw_FeTot.long);

%fit linear model
TBL=table(raw_FeTot.FeTot,SedRate,OC, ...
    WaterDepth,distance_to_coast,sed_size,dust_input);
fitlm(TBL)
