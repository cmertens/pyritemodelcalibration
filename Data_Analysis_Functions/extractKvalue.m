function [ProfileK,ProfileT]=extractKvalue(Fe,Depth,SedRate,movVal)
%Prepare Data
tFe=Depth/(SedRate); %converted to time;
Fe=movmedian(Fe,movVal);
plot(Fe,-tFe,'o-')
%% Extract k
ProfileK=[];
ProfileT=[];
for i=1:numel(Fe)-1
ProfileK(end+1)=1/(tFe(i+1)-tFe(i))*log(abs(Fe(i)/Fe(i+1)));
ProfileT(end+1)=mean([tFe(i+1) tFe(i)]);
end

end