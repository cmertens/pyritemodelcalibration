%% Explore controls of environmental parameters on kgSW
clear variables;close all; clc
% ---------------- Import Data-----------------------------------------------------------------
load("Data\Processed_Data\Extracted_Variables\kgSW_combined.mat","kgSW",'kgSWerr',"lat","long")
load("Data\Processed_Data\Global_Data\SedRate_Paradis.mat","raw_SedRate_Paradis")
load("Data\Processed_Data\Global_Data\Age_Paradis.mat","Raw_Age_Paradis")
load("Data\Processed_Data\Validation_Data_Combined.mat","OC_Combined","Age_Combined","SedRate_Combined")

raw_SedRate_Paradis.SedRate

%% Combine data
SedRate=[SedRate_Combined';raw_SedRate_Paradis.SedRate'];
Age=[Age_Combined'; Raw_Age_Paradis.Age'];
OC=Interpolate_OC(lat,long,'Paradis');
OC(1:size(OC_Combined,2))=OC_Combined';
Porosity=Interpolate_Porosity(lat,long,'Martin');
Depth=Interpolate_WaterDepth(lat,long);
Temp=Interpolate_Seafloor_T(lat,long);
TOU=Interpolate_TOU(lat,long);
SedSize=Interpolate_SedSize(lat,long);
%% Prepare Data
kgSW(kgSW>1)=NaN; %excluse extreme data
kgSW(kgSW==0)=NaN;
kgSW(kgSW<0)=NaN;
Temp(Temp<0)=NaN;
Depth(isinf(Depth))=NaN;
T=table(log10(SedRate),log10(Depth'),log10(Temp'),...
    log10(OC'),log10(Age),log10(TOU'),log10(Porosity'),log10(SedSize)',log10(kgSW'),log10(kgSWerr'),...
    'VariableNames',{'w';'Depth';'T';'OC';'C14';'TOU';'Phi';'SedSize';'kgSW';'kgSWErr'});
Tarray=table2array(T);
Tarray(isinf(Tarray))=NaN;
T=T(~isnan(kgSW),:);
T=T(~isinf(T.kgSW),:);
T.Depth(isinf(T.Depth))=NaN;
fitlm(T(:,1:end-1))


%% ---------------------------------------------------------------
figure
tiledlayout('flow','TileSpacing','tight','Padding','tight')
%sedrate
ax=nexttile();
p1=scatter(T.w,T.kgSW,[],10.^T.kgSWErr./10.^T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10}SE k_{G_{SW}}';
mdl=fitlm(T.w,T.kgSW);
[a]=gmregress(T.w,T.kgSW);
hold on
p2=plot(T.w,T.w*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} Sedimentation Rate [cm y^{-1}]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Blues')))
%Depth
%--------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.Depth,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.Depth,T.kgSW);
[a]=gmregress(T.Depth,T.kgSW);
hold on
p2=plot(T.Depth,T.Depth*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} Water Depth [m]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Purples')))
%temperature
%--------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.T,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.T,T.kgSW);
[a]=gmregress(T.T,T.kgSW);
hold on
p2=plot(T.T,T.T*a(2)+a(1),'k:','LineWidth',1);xlabel('log_{10} Temperature [°C]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Oranges')));
% OC
%--------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.OC,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.OC,T.kgSW);
[a]=gmregress(T.OC,T.kgSW);
hold on
p2=plot(T.OC,T.OC*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} Organic Carbon G_0 [wt%]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Greens')));
% C14
%---------------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.C14,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.C14,T.kgSW);
[a]=gmregress(T.C14,T.kgSW);
hold on
p2=plot(T.C14,T.C14*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} C14 Age [ybp]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Greys')));
% DOU
%---------------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.TOU,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.TOU,T.kgSW);
[a]=gmregress(T.TOU,T.kgSW);
hold on
p2=plot(T.TOU,T.TOU*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} Total Oxygen Uptake [mM m^{-2} d^{-1}]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Reds')));
% Porosity
%---------------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.Phi,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.Phi,T.kgSW);
[a]=gmregress(T.Phi,T.kgSW);
hold on
p2=plot(T.Phi,T.Phi*a(2)+a(1),'k:','LineWidth',1);
xlabel('Porosity')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('Reds')));
% Sediment size
%-------------------------------------------------------------------------------------
ax=nexttile();
p1=scatter(T.SedSize,T.kgSW,[],T.kgSWErr./T.kgSW,'filled');
c=colorbar;
c.Label.String='Relative log_{10} SE k_{G_{SW}}';
mdl=fitlm(T.SedSize,T.kgSW);
[a]=gmregress(T.SedSize,T.kgSW);
hold on
p2=plot(T.SedSize,T.SedSize*a(2)+a(1),'k:','LineWidth',1);
xlabel('log_{10} Sediment Size [\phi]')
ylabel('log_{10} k_{G_{SW}} [y^{-1}]')
legend(p2,sprintf('R^2=%0.2f',mdl.Rsquared.Ordinary),'Location','best')
colormap(ax,flipud(slanCM('BuGN')));

%% Gloabl kgSW
xlat=linspace(90,-90,1000)'; %these are not the right latitude
xlong=linspace(180,-180,1000)';
[klong,klat]=meshgrid(xlat,xlong);
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Model\ExtractedVariables\AdditionalProfileData.mat','wExtra')
sedpath='C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\RawSatteliteData\GlobalSedRates.nc';
sedncid=(netcdf.open(sedpath));
sedLat=netcdf.getVar(sedncid,0);
sedLong=netcdf.getVar(sedncid,1);
v=double(netcdf.getVar(sedncid,2)); %these are already log values
[sedX,sedY]=meshgrid(sedLat,sedLong);
for i=1:1000
    Wq(:,i)=interp2(sedX,sedY,v,klong(:,i),klat(:,i));
end
Wq=log(10.^Wq);
kWq=exp(-4.1148 + 0.9109*(Wq));
f=figure;
worldmap world
geoshow(klong,klat,Wq,'DisplayType','Texturemap')
geoshow('landareas.shp','FaceColor','black')
cb = colorbar('southoutside');
cb.Label.String = 'k_{G0} [y^{-1}]';
colormap(flipud(slanCM('rain')))
mlabel off; plabel off; gridm off
set(gca,'ColorScale','log')
exportgraphics(f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Global_Input\GlobalkgSW.jpg','Resolution',3000)
%% Function for fit

function y = linear_model(b, x)
y = b(1) + b(2)*x(:,1);
end