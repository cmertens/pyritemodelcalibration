for i=1:216
    Data=ImportData(i);
    if min(Data(2).S)<1
    idx=find(Data(2).S<1,1);
    Depth=Data(1).S;
    SulfateDepletionDepth(i)=Depth(idx);
    end
end
load("Data\Processed_Data\Validation_Data_Combined_withGlobal.mat","WaterDepth_Combined_withGlobal");
load("Data\Processed_Data\Validation_Data_Combined.mat","Black_Sea_idx")
%%
%find all sufate depletion depth in shelf areas
Shelfidx=WaterDepth_Combined_withGlobal<200;
Shelfidx(Black_Sea_idx)=0;
SD=SulfatedepletionDepth(Shelfidx);
SD(SD==0)=[];
ShelfDepth=max(SD)/100;
%characteristic porossity loss at that depth
m1=66.3.*exp(-1/1333);
CD=m1.*exp(-linspace(0,ShelfDepth,100)/1333);
%1st meter

CDpercentShelf=100-mean(CD)/m1*100

%find all sufate depletion depth in slope areas
Slopeidx=WaterDepth_Combined_withGlobal>200&WaterDepth_Combined_withGlobal<2000;
Slopeidx(Black_Sea_idx)=0;
SD=SulfatedepletionDepth(Slopeidx);
SD(SD==0)=[];
SlopeDepth=max(SD)/100;
%characteristic porossity loss at that depth
CD=m1.*exp(-linspace(0,SlopeDepth,100)/1333);
CDpercentSlope=100-mean(CD)/m1*100

%find all sufate depletion depth in slope areas
Abyssidx=WaterDepth_Combined_withGlobal>2000;
Abyssidx(Black_Sea_idx)=0;
SD=SulfatedepletionDepth(Abyssidx);
SD(SD==0)=[];
AbyssDepth=max(SD)/100;
%characteristic porossity loss at that depth
%CD=82.7.*exp(-AbyssDepth/430);
CD=m1.*exp(-linspace(0,AbyssDepth,100)./1333);
CDpercentAbyss=100-mean(CD)/m1*100

