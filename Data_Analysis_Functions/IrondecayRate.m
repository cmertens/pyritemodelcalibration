close all;
clear variables;
%% Validation
%constructing a fake k(t) and H(t) profile, calculate F(t) and extract k(t)
%again from it to see if this works.
close all; clear variables

kt=linspace(0,10,100);

k=0.2*exp(-kt);
f=figure;
subplot(2,2,1)
plot(kt,k,'k','LineWidth',2);
hold on
ylabel('k_F(z)')
xlabel('z')
%legend('k_F=0.2e^{-t}')

H=sqrt(40*sin(0.3*kt));
H2=sqrt(2*sin(0.3*kt));
H3=sqrt(200*sin(0.3*kt));
subplot(2,2,2)
plot(kt,H,'LineWidth',2,'Color',[ 0.1804    0.5294    0.5059])
hold on
plot(kt,H2,'LineWidth',2,'Color',[0.5020    0.1294    0.1294])
plot(kt,H3,'LineWidth',2,'Color',[0.2902    0.5098    0.7608])
ylabel('$\sqrt{H(z)}$','Interpreter','latex')
xlabel('z')

%legend('$\sqrt{H1(t)}=\sqrt{40sin(0.3t)}$','$\sqrt{H2(t)}=\sqrt{2sin(0.3t)}$','$\sqrt{H3(t)}=\sqrt{200sin(0.3t)}$','Interpreter','latex')
% k=k.*H;
% k2=k.*H2;
% k3=k.*H3;
ic = 1;
options1 = odeset('NonNegative',1);
[t,F] = ode45(@(t,F) myode(F,t,kt,H,k),kt, ic,options1);
[t2,F2] = ode45(@(t,F) myode(F,t,kt,H2,k),kt, ic,options1);
[t3,F3] = ode45(@(t,F) myode(F,t,kt,H3,k),kt, ic,options1);
subplot(2,2,3)
plot(t,F,'LineWidth',2,'Color',[ 0.1804    0.5294    0.5059])
hold on 
plot(t2,F2,'LineWidth',2,'Color',[0.5020    0.1294    0.1294])
plot(t3,F3,'LineWidth',2,'Color',[0.2902    0.5098    0.7608])
ylabel('F(z)')
xlabel('z')
%legend('F1','F2')
% extrakt k
knew=[];
tplot=[];
Hnew=[];
for i=1:numel(F)-1
knew(end+1)=1/(t(i+1)-t(i))*log(abs(F(i)/F(i+1)));
tplot(end+1)=t(i)+(t(i+1)-t(i))/2;
Hnew(end+1)=sqrt(20+20*sin(tplot(end)));
end
knew2=[];
tplot2=[];
Hnew2=[];
for i=1:numel(F2)-1
knew2(end+1)=1/(t2(i+1)-t2(i))*log(abs(F2(i)/F2(i+1)));
tplot2(end+1)=t2(i)+(t2(i+1)-t2(i))/2;
Hnew2(end+1)=sqrt(0.01+2*sin(0.3*tplot2(end)));
end
knew3=[];
tplot3=[];
Hnew3=[];
for i=1:numel(F3)-1
knew3(end+1)=1/(t3(i+1)-t3(i))*log(abs(F3(i)/F3(i+1)));
tplot3(end+1)=t3(i)+(t3(i+1)-t3(i))/2;
Hnew3(end+1)=sqrt(0.01+2*sin(0.3*tplot3(end)));
end
subplot(2,2,4)
plot(kt,k,'k','LineWidth',2);
hold on
plot(tplot,knew,'--','LineWidth',2,'Color',[ 0.1804    0.5294    0.5059])
plot(tplot2,knew2,'--','LineWidth',2,'Color',[0.5020    0.1294    0.1294])
plot(tplot3,knew3,'--','LineWidth',2,'Color',[0.2902    0.5098    0.7608])
xlabel('z')
ylabel('k_F(z)')
legend('predetermined k_F','extracted k_F','extracted k_F','extracted k_F')

 set(f,'Units','Inches');
      pos = get(f,'Position');
      set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
      print('-vector',f,'C:\Users\cmertens\Documents\PhanerozoicPyrite\Figures\Deriving_Constants\ExtractkF.pdf','-dpdf','-r0')

%% Find kF0 out of Poultons Data
%assume a certain H2S concentration in millimolar
H2Smm=sqrt(0.1*10^-3);
tP=logspace(-5,0,50);%timescale of poultons experiments in year
%close all; clear varibales; clc
kHFO=4.41*525600 ; %525600 to convert from min to year; *168 to convert to per gram and not per mol 
kLep=0.034*525600;
kGoe=0.00024*525600;
kMag=0.000032*525600;
kHem=0.0000089*525600;
%HFO
syms F(t) t
odeHFO=diff(F,t)==-kHFO*F*H2Smm;
cond=F(0)==0.32;
solHFO(t)=dsolve(odeHFO,cond);
%Lep
odeLep=diff(F,t)==-kLep*F*H2Smm;
cond=F(0)==0.32;
solLep(t)=dsolve(odeLep,cond);
%Geothite
odeGoe=diff(F,t)==-kGoe*F*H2Smm;
cond=F(0)==0.18;
solGoe(t)=dsolve(odeGoe,cond);
%Magnetite
odeMag=diff(F,t)==-kMag*F*H2Smm;
cond=F(0)==0.05;
solMag(t)=dsolve(odeMag,cond);

%Hematite
odeHem=diff(F,t)==-kHem*F*H2Smm;
cond=F(0)==0.18;
solHem(t)=dsolve(odeHem,cond);

%overall solution

sol=solHFO+solLep+solGoe+solMag+solHem;
F=sol(tP);
f=figure;
loglog(tP,F,'o')
xlabel('time (years)')
ylabel('Reactive Iron concentration wt%')


%derive ksqrt(H)
knew=[];
tplot=[];
for i=1:numel(F)-1
knew(end+1)=1/(tP(i+1)-tP(i))*log(abs(F(i)/F(i+1)));
tplot(end+1)=tP(i)+(tP(i+1)-tP(i))/2;
end

figure
loglog(tplot,knew,'o')
xlabel('t')
ylabel('$k_F(t)$','Interpreter','latex')

% for a small H2S concentration
H2Smm=sqrt(10*10^-3);
%HFO
syms F(t) t
odeHFO=diff(F,t)==-kHFO*F*H2Smm;
cond=F(0)==0.32;
solHFO(t)=dsolve(odeHFO,cond);
%Lep
odeLep=diff(F,t)==-kLep*F*H2Smm;
cond=F(0)==0.32;
solLep(t)=dsolve(odeLep,cond);
%Geothite
odeGoe=diff(F,t)==-kGoe*F*H2Smm;
cond=F(0)==0.37;
solGoe(t)=dsolve(odeGoe,cond);
%Magnetite
odeMag=diff(F,t)==-kMag*F*H2Smm;
cond=F(0)==0.05;
solMag(t)=dsolve(odeMag,cond);

%Hematite
odeHem=diff(F,t)==-kHem*F*H2Smm;
cond=F(0)==0.37;
solHem(t)=dsolve(odeHem,cond);

%overall solution

sol=solHFO+solLep+solGoe+solMag+solHem;
F=sol(tP);
f=figure;
loglog(tP,F,'o')
xlabel('time (years)')
ylabel('Reactive Iron concentration wt%')

%derive ksqrt(H)
knewLarge=[];
tplotLarge=[];
for i=1:numel(F)-1
knewLarge(end+1)=1/(tP(i+1)-tP(i))*log(abs(F(i)/F(i+1)));
tplotLarge(end+1)=tP(i)+(tP(i+1)-tP(i))/2;
end

%% Add experimental data from canfield 1989
Fe=[0.07 0.025 0.018 0.016 0.017 0.001];
tFe=[1 3 5 7.5 10.5 11]/0.1;
Fe=movmedian(Fe,3);
kCanfield=[];
tCanfield=[];
for i=1:numel(Fe)-1
kCanfield(end+1)=1/(tFe(i+1)-tFe(i))*log(abs(Fe(i)/Fe(i+1)));
tCanfield(end+1)=mean([tFe(i+1) tFe(i)]);
end


%% compare to sed profiles
close all;
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Processed_Data\Validation_Data_Combined.mat');   

%Good profiles are by raven et al 2016 and Zindorf2019
%Raven(2016);
Data=ImportData(158);
[kRaven,tRaven]=extractKvalue(Data(2).F,Data(1).F,SedRate_Combined(158),numel(Data(2).F)/3); %idx of profile,Iron,Depth,Names,Plot yes or no?(1/0),no. for moving avergae
kRaven=kRaven(1:12);
tRaven=tRaven(1:12);

Data=ImportData(159);
[kRaven2,tRaven2]=extractKvalue(Data(2).F,Data(1).F,SedRate_Combined(159),numel(Data(2).F)/3); %idx of profile,Iron,Depth,Names,Plot yes or no?(1/0),no. for moving avergae

% Zindorf(2019)
Data=ImportData(212);
[kZindorf,tZindorf]=extractKvalue(Data(2).F,Data(1).F,SedRate_Combined(212),numel(Data(2).F)/3); %Zindorf
kZindorf=kZindorf(1:95);
tZindorf=tZindorf(1:95);

%Mazumdar(2012)
Data=ImportData(142);
[kMazumdar,tMazumdar]=extractKvalue(Data(2).F,Data(1).F,SedRate_Combined(142),numel(Data(2).F)/3); %Mazumda2012

knewInterpSmall=interp1(tplotLarge,knewLarge,tplot);
Poultonmax=log10(max([knewInterpSmall; knew]));
Poultonmax=smooth(Poultonmax,15);

knewInterpLarge=interp1(tplot,knew,tplotLarge);
Poultonmin=log10(max([knewInterpLarge; knew]));
Poultonmin=smooth(Poultonmin,15);

figure
p0=plot(log10(tRaven),log10(kRaven),'x')
hold on
p1=plot(log10(tplotLarge),(Poultonmax),'o','MarkerSize',4,'Color',[0.8510    0.3255    0.0980]);
p2=plot(log10(tplot),(Poultonmin),'o','MarkerSize',4,'Color',[0.9098    0.7451    0.6745]);
p3=plot(log10(tRaven2),log10(kRaven2),'kx');
p4=plot(log10(tZindorf),log10(kZindorf),'+');
p5=plot(log10(tMazumdar),log10(kMazumdar),'*');
%loglog(tWerne,(kWerne),'<')
%loglog(tPeketi,(kPeketi),'>')
p6=plot(log10(tCanfield),log10(kCanfield),'d');
%shaded area for poulton data
%%

g=[repmat(1,size(tplot)) repmat(0,size(tplot))];
x=[log10((tplot)) fliplr(log10(tplotLarge))];
y=[(Poultonmax);flipud((Poultonmin))];
g(isnan(x))=[];
y(isnan(x))=[];
x(isnan(x))=[];
y(isinf(x))=[];
g(isinf(x))=[];
x(isinf(x))=[];
p=patch(x,y,g,'EdgeColor','none','FaceAlpha',0.4);
c=colormap((slanCM('reds')));
c=c(100:end,:);
colormap(c);
%% Fit
t=[tplot tplotLarge tCanfield tRaven tRaven2 tMazumdar tZindorf ];
k=[knew knewLarge kCanfield kRaven kRaven2  kMazumdar kZindorf ];
k(isnan(t))=[];
t(isnan(t))=[];
 t(k<0)=[];
 k(k<0)=[];

%k=abs(k);
[xData, yData] = prepareCurveData(log10(t),log10(k));
[b,bint]=gmregress(xData,yData); %geometric mean regression
%force slope to -1
a=mean(yData+xData);
p7=plot(log10(t),log10(10.^(b(1))*t.^b(2)),'--');
y=10.^(a)*t.^-1;
p8=plot(log10(t),log10(y));
%only core data
t=[tCanfield tRaven tRaven2 tMazumdar tZindorf ];
k=[kCanfield kRaven kRaven2 kMazumdar kZindorf ];
k(isnan(t))=[];
t(isnan(t))=[];
 t(k<0)=[];
 k(k<0)=[];
%k=abs(k);
[xData, yData] = prepareCurveData(log10(t),log10(k));
[bX,bintX]=gmregress(xData,yData);
aEx=mean(yData+xData);
p9=plot(log10(t),log10(10.^(bX(1))*t.^bX(2)),':');

ylabel('log_{10} k_F(t) [y^{-1}]')
xlabel('log_{10} t [y]')
legend([p0 p1 p2 p3 p4 p5 p6 p7 p8 p9],'Raven2016 GC','Poulton2004-10 mM H_2S','Poulton2004-0.1 mM H_2S','Raven2016 MC','Zindorf2019','Mazumdar2012','Canfield1989',sprintf('y=%0.2f t^{%0.2f}',10.^b(1),b(2)),sprintf('y=%0.2f {t}^{-1}',10.^a),sprintf('y=%0.2f {t}^{%0.2f}',10.^bX(1),bX(2)),'Location',"southwest") 

%% CI of forced slope by bootstrapping
t=[tplot tplotLarge tCanfield tRaven tRaven2 tMazumdar tZindorf ];
k=[knew knewLarge kCanfield kRaven kRaven2  kMazumdar kZindorf ];
k(isnan(t))=[];
t(isnan(t))=[];
 t(k<0)=[];
 k(k<0)=[];
% Your log-transformed variables
[xData, yData] = prepareCurveData(log10(t),log10(k));
log_x = xData; % Your log-transformed x variable
log_y = yData; % Your log-transformed y variable

% Number of bootstrap samples
num_samples = 1000;

% Function to perform geometric mean regression with forced slope -1
% Implement your own regression method here or use a third-party toolbox
% For instance, you might use 'fitglm' or other fitting functions depending on your approach

% Preallocate array to store intercept values
intercept_values = zeros(num_samples, 1);

% Bootstrap resampling
for i = 1:num_samples
    % Generate bootstrap sample indices
    indices = datasample(1:length(log_x), length(log_x));
    bootstrapped_x = log_x(indices);
    bootstrapped_y = log_y(indices);
    
    % Perform geometric mean regression on the bootstrap sample
    % Implement your regression function here to get the intercept
    % For example:
    intercept = mean(bootstrapped_x+bootstrapped_y);
    % Replace 'your_geometric_mean_regression_function' with your actual regression function
    
    % Store the intercept value
    intercept_values(i) = intercept;
end

% Calculate confidence interval
confidence_interval = prctile(intercept_values, [2.5, 97.5]);
disp(['95% Confidence Interval for Intercept: [' num2str(confidence_interval(1)) ', ' num2str(confidence_interval(2)) ']']);

function dydt = myode(F,t,kt,H,k)
knew = interp1(kt,k,t); % Interpolate the data set (ft,f) at time t
Ht = interp1(kt,H,t); % Interpolate the data set (gt,g) at time t
dydt = F*-knew*sqrt(Ht); % Evaluate ODE at time t

end


