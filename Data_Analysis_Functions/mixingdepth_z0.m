%From Solan et al 2019
Data=tdfread('C:\Users\cmertens\Documents\PhanerozoicPyrite\Data\Raw_Data\MixedLayerDepth\solan_etal_DbL_v4.tab');
%first look at global distribution
[lat, long] = Define_Global_Grid(5);
SedRate = Interpolate_SedRate(Data.DbL_lat, Data.DbL_long, 'Restreppo');
OPD = calcz0(SedRate) * 0.1; % because z0 is already taking 10*OPD into account

% Compare to specific sites
L = Data.DbL_L;
SedRate = Interpolate_SedRate(Data.DbL_lat, Data.DbL_long, 'Restreppo');
OPD = calcz0(SedRate) * 0.1; % because z0 is already taking 10*OPD into account
[xData, yData] = prepareCurveData(log10(OPD), log10(L));

% Fitting a linear model
lm = fitlm(xData, yData);
% lm = fitlm(xData, yData - 1 * xData, 'constant'); % Uncomment if you want a specific model
gmregress(xData, yData);

% Mean deviation from 1:1 line
Res_1to1 = yData - log10(1) * xData; % Residuals from 1:1 line
AbsDev_1to1 = abs(Res_1to1); % Absolute deviations
MD_1to1 = sum(AbsDev_1to1) / numel(AbsDev_1to1); % Mean deviation

% Mean deviation from 1:10 line
Res_1to10 = yData - (1+ xData); % Residuals from 1:10 line (log10(10) = 1)
AbsDev_1to10 = abs(Res_1to10); % Absolute deviations
MD_1to10 = sum(AbsDev_1to10) / numel(AbsDev_1to10); % Mean deviation

% Display results
disp(['Mean Deviation from 1:1 line: ', num2str(MD_1to1)]);
disp(['Mean Deviation from 1:10 line: ', num2str(MD_1to10)]);