%% OPD based on sedimentation rate
function fitmdl=OPD_Predictors(plotting)
load("Data\Processed_Data\Global_Data\OPD.mat","raw_OPD")

%interpolate sedrates to OPD of Jorgensen
SedRate=Interpolate_SedRate(raw_OPD.lat,raw_OPD.long,'Restreppo');
%geometric mean regression
fitmdl=gmregress(log10(SedRate),log10(raw_OPD.OPD*0.1));
if plotting
figure
scatter(log10(SedRate),log10(raw_OPD.OPD*0.1),[],'b','filled','MarkerEdgeColor','k');
hold on
p2=plot(log10(SedRate),log10(SedRate)*fitmdl(2)+fitmdl(1),'k:','LineWidth',1);
xlabel('log_{10} w [cm y^{-1}]')
ylabel('log_{10} ODP [cm]')
lm=fitlm(log10(SedRate),log10(raw_OPD.OPD*0.1));
Rsquared=lm.Rsquared.Adjusted; %outside of gmregress function
legend(p2,sprintf('R^2=%0.2f',Rsquared),'Location','best')
colormap(flipud(slanCM('GnBu')))

end
