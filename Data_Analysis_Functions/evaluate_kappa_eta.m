%% Validating the non-dimensional equations conceptually and comparing to literature
clear variables;close all;clc

CoreNr=1;
%% Import Parameters
Data=ImportData(CoreNr);
Parameters=ImportParameters(CoreNr);
Boundaries=ImportBoundaries(Data,CoreNr);
%% z0=Oxygen penetration Depth
z0=calcz0(Parameters.w); %in cm
%new Boundaries at z0
Boundaries.G0=Boundaries.G0*(Parameters.a.*Parameters.w./(Parameters.a*Parameters.w+Parameters.kgSW.*z0)).^Parameters.a;
%Boundaries.F0=Boundaries.F0*(Parameters.b.*Parameters.w./(Parameters.b*Parameters.w+Parameters.kF0.*z0)).^Parameters.b;
Parameters.kg0=Parameters.a./(Parameters.a./Parameters.kgSW+z0./Parameters.w);
NonDimParameters=makeNonDimParameters(Parameters,Boundaries);
[NonDimData]=makeNonDimData(Data,Parameters,Boundaries);
[NonDimBoundaries]=makeNonDimBoundaries(Parameters,Boundaries);
S0=Boundaries.S0;

%%-------------------------------------------------------------------------------------------------------------------------------------
maxDepth=5;
zetamesh=linspace(0,maxDepth,300);
%%
f=figure;
s1=subplot(1,4,1);
hold on
s2=subplot(1,4,3);
hold on
Kappaeta_Factor=[0.1 1 10];
for i=1:3
    NonDimParameters.Da=2;
    NonDimBoundaries.Gamma0=20;
    NonDimBoundaries.Psi0=50;
    NonDimParameters.KappaE=Kappaeta_Factor(i)*NonDimParameters.KappaS;
    ModelSolution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,zetamesh);
    plot(s1,ModelSolution(2).Eta,-ModelSolution(1).Eta,'LineWidth',2)
    legend('\kappa_\eta=0.1\kappa_\Sigma','\kappa_\eta=\kappa_\Sigma','\kappa_\eta=10\kappa_\Sigma')
xlabel('\eta')
ylabel('\zeta')
    plot(s2,ModelSolution(2).Pi,-ModelSolution(1).Pi,'LineWidth',2)
    legend('\kappa_\eta=0.1\kappa_\Sigma','\kappa_\eta=\kappa_\Sigma','\kappa_\eta=10\kappa_\Sigma')
xlabel('\Pi')
ylabel('\zeta')
end
%%
% Plot Validation Histograms KappaE=0.1KappaS
load('Figures\Calibrated_Profiles\All52KappaEta0.1\Parameters.mat')
allValuesCell = {allResiduals.('H')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];

s3=subplot(1,4,2);
hold on
histogram(Res,10)

allValuesCell = {allResiduals.('P')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];


s4=subplot(1,4,4);
hold on
histogram(Res,10)

% Plot Validation Histograms KappaE=KappaS
load('Figures\Calibrated_Profiles\All50\Parameters.mat')
allValuesCell = {allResiduals.('H')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];

histogram(s3,Res,10)

allValuesCell = {allResiduals.('P')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];
histogram(s4,Res,10)

% Plot Validation Histograms 10KappaS
load('Figures\Calibrated_Profiles\All53KappaEta10\Parameters.mat')
allValuesCell = {allResiduals.('H')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];
histogram(s3,Res,10)
allValuesCell = {allResiduals.('P')};  % Extract the arrays within the field
multipliedArrays = cellfun(@(x) x , allValuesCell, 'UniformOutput', false);
Res= cat(2, multipliedArrays{:});  % Concatenate the arrays
Res(Res==0)=NaN;
Res(isnan(Res))=[];
histogram(s4,Res,10)


