%% Gamma distribution
v=0.125;
k=0:10;
G0=3;
a=0.21;

Gk0=G0.*k.^(v-1).*exp(-a*k)/gamma(v);
%figure
hold on
plot(k,Gk0)
xlabel('k')

%%
syms G(t) g0 t nu a k
int(exp(-a*k-k*t),k)
int((k*nu-k)*exp(-a*k-k*t),k)
eqn=G==(-G*k^nu*igamma(nu + 1, k*(a + t)))/(gamma(nu)*(a + t)*(k*(a + t))^nu);
%eqn=G==integral(((g0*k^{nu-1}*exp(-a*k))/(gamma(nu)))*k*exp(-k*t),0,1e100)
cond=G(0)==G0;
d=dsolve(eqn,cond);
simplify(d)

%% example

%Data from Westrich &Berner 1984: 
t=[0.01 30 80 280 400 700];
GG0=[1 0.54 0.43 0.41 0.4 0.39];

figure
plot(t,GG0,'o','MarkerFaceColor','k')

modelfun = @(b,t)b(1) + b(2)*exp(-b(3)*t) + b(4)*exp(-b(5)*t) 
beta0 = [0.1 0.1 0.1 0.1 0.1];
mdl = fitnlm(t,GG0,modelfun,beta0);
hold on
tplot=linspace(0,700,1000);
plot(tplot,modelfun(mdl.Coefficients.Estimate,tplot))


%%modelfun from westrich:
modelfun = @(b,t) (b(1).^(b(2)))./((b(1)+t).^b(2))
beta0 = [0.1 0.1];
mdl = fitnlm(t,GG0,modelfun,beta0);
plot(tplot,modelfun(mdl.Coefficients.Estimate,tplot))

legend('$\frac{G(t)}{G0}=b(1)+b(2)e^{-b(3)*t}+b(4)*e^{-b(5)*t}$','$\frac{G(t)}{G0}=\frac{a^\nu}{(a+t)^\nu} \quad a=0.04 \quad \nu=0.10$','Interpreter','latex')

%%our model
modelfun = @(b,t) ((b(3)*b(2)/b(1))./(b(3)*b(2)/b(1)+t)).^b(3);
beta0 = [0.1 0.1 0.1];
mdl = fitnlm(t,GG0,modelfun,beta0);
plot(tplot,modelfun(mdl.Coefficients.Estimate,tplot))


%%Non-Dim

modelfun = @(b,t)(0.26^0.26)./(0.26 + b(1)*t).^0.26
beta0 = [10];
mdl = fitnlm(0.01*t,GG0,modelfun,beta0);
plot(tplot,modelfun(mdl.Coefficients.Estimate,tplot))
plot(t*0.01,GG0,'o','MarkerFaceColor','g')
legend('Data Westrich&Berner 1984','$\frac{G(t)}{G0}=b(1)+b(2)e^{-b(3)*t}+b(4)*e^{-b(5)*t}$','$\frac{G(t)}{G0}=\frac{a^\nu}{(a+t)^\nu} \quad a=0.04 \quad \nu=0.10$','Our model','Non-Dimensional''Interpreter','latex')