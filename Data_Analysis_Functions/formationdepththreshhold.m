f=figure;
s1=subplot(2,2,1);
hold on
s2=subplot(2,2,2);
hold on
s3=subplot(2,2,3);
hold on
s4=subplot(2,2,4);
hold on

%Define avergae compositions for shelf, slope and abyss
load('C:\Users\cmertens\Documents\PhanerozoicPyrite\Euler\Euler42\Variables.mat')
[lat,long]=Define_Global_Grid(5);
waterDepth=Interpolate_WaterDepth(lat,long);
waterDepth=waterDepth(:);
Pq=Interpolate_Porosity(lat,long,'Martin');
PqEuler=Pq(:);
%% Shelf
%low OC
rho(1)= 2.6500;
phi(1)=0.5947;
w(1)=0.1163;
kg0(1)=0.0023;
DS(1)=107.9250;
DH(1)=347.9587;
OC(1)=0.6;
FeHR(1)=0.8948;
T(1)=9.1365;

%high OC
rho(2)=2.6500;
phi(2)=0.6047;
w(2)=0.1186;
kg0(2)=0.0022;
DS(2)=104.5806;
DH(2)=345.4088;
OC(2)=3.6;
FeHR(2)=0.9072;
T(2)=7.6310;
%% Slope
%low OC
rho(3)=2.6500;
phi(3)=0.66;
w(3)=0.0459;
kg0(3)=6.0429e-04;
DS(3)=96.1056;
DH(3)=346.8205;
OC(3)=0.5;
FeHR(3)=1.0459;
T(3)=2.6612;
%high OC
rho(4)=2.6500;
phi(4)=0.6614;
w(4)=0.0571;
kg0(4)=7.7121e-04;
DS(4)= 99.2637;
DH(4)=352.4751;
OC(4)=3.2;
FeHR(4)=0.9201;
T(4)=3.3765;
%% Abyss
%low OC
rho(5)=2.6500;
phi(5)=0.7461;
w(5)=0.0055;
kg0(5)=6.8679e-05;
DS(5)=104.1586;
DH(5)=388.2919;
OC(5)=0.3;
FeHR(5)=0.8631;
T(5)=1.1513;
%highOC
rho(6)=2.6500;
phi(6)=0.7451;
w(6)=0.0128;
kg0(6)=1.6216e-04;
DS(6)=105.5768;
DH(6)=390.2685;
OC(6)=2.6;
FeHR(6)=0.7960;
T(6)=1.5187;

for i=5
Parameters=struct();
        Parameters.rho=rho(i); %sediment density
        Parameters.phi=phi(i);%Porosity
        Parameters.fG=calcfG(Parameters.phi,Parameters.rho);
        Parameters.fF=calcfF(Parameters.phi,Parameters.rho);
        Parameters.fP=calcfP(Parameters.phi,Parameters.rho);
        Parameters.w=w(i); %sedimentation rate
        Parameters.kg0=kg0(i); %OC reactivity
        Parameters.DS=DS(i); %Sulfate Diffusivity
        Parameters.DH=DH(i); %H2S Diffusivity
        Parameters.a=0.23; %constant for OC decay
        Parameters.b=0.17; %constant for Fe decay
        Parameters.Ks=1.62; %Monod coefficient
        Parameters.T=T(i);

        %set up boundaries
        Boundaries=struct();
        Boundaries.G0=OC(i); %Organic carbon
        Boundaries.S0=28; %Sulfate
        Boundaries.F0=FeHR(i); %Reactive Iron
        Boundaries.P0=0;         %Pyrite
        Boundaries.d34S0=21;     %delta34S sulfate

        %calculate non-dim Parameters
        NonDimParameters=makeNonDimParameters(Parameters,Boundaries);
        NonDimBoundaries=makeNonDimBoundaries(Parameters,Boundaries);
        NonDimBoundaries.Eta0=0;

        maxDepth=1000; %non-dim depth until profiles are calculated
        zetamesh=linspace(0,maxDepth,300); %solver grid
        tspace=logspace(-5,-1,100); %formation depth threshhold grid    
            %solve equations
            ModelSolution=ModelSolverNonDim(NonDimBoundaries,NonDimParameters,zetamesh);

           for j=1:100

           
            %Pyrite concentration
            Py=ModelSolution(2).Pi;
            Pyz=ModelSolution(1).Pi;

            %Pyrite isotopic composition
            Pyd34=ModelSolution(2).d34Pi;
            Pyd34z=ModelSolution(1).d34Pi;
            Pyd34=interp1(Pyd34z,Pyd34,Pyz); %use same grid

            Pyz(Py==0)=[];
            Pyd34(Py==0)=[];
            Py(Py==0)=[];
            Pyz(isnan(Py))=[];
            Pyd34(isnan(Py))=[];
            Py(isnan(Py))=[];
            FormationDepthThreshhold=tspace(j)*Py(end);  
          
           
            idx=find(flipud(diff(Py))>FormationDepthThreshhold,1);% find first derivative in upside down array that is larger than threshhold
            if isempty(idx)
                PydDepth(i)=0;
            else
                PydDepth(i)=Pyz(numel(Py)-idx);
            end
          
      
            fdIdx=find(Pyz>=PydDepth(i),1);
            %delete pyrite values for depth larger than pyrite formation
            %depth
            Py(max([2 fdIdx]):end)=[];
            Pyd34(max([2 fdIdx]):end)=[];
            Pyz(max([2 fdIdx]):end)=[];

            %Pyrite formation rate
            if length(Py)>1
                %Pyrite concentration at Pyrite formation depth
                PyDim=Py*Boundaries.S0/Parameters.fP;
                PyatDepth=PyDim(end);
                dPydt(i)=PyatDepth/100*Parameters.w*Parameters.rho*(1-Parameters.phi);
                %Pyrite isotopic composition at pyrite formation depth
                Pyd(i)=Pyd34(end);
                %Total grams of pyrite/cm2
                PyWtPercentNonDim=trapz(Pyz,Py);%sum(Py)/numel(Pyz);%sum(Py);;
                %Dimnesional
                PyWtPercent(i)=PyWtPercentNonDim*Parameters.fP*Boundaries.S0/Parameters.fF;
                Pyg(i)=PyWtPercent(i)/100.*Parameters.rho.*(1-Parameters.phi);
            else
                PyWtPercent(i)=NaN;
                dPydt(i)=NaN;
                Pyg(i)=NaN;
                Pyd(i)=NaN;
            end
               if isnan(Pyd(i))
                Pyd34(isnan(Pyd34))=[];
                Pyd(i)=Pyd34(end);
            end
        
        Pygj(j)=Pyg(i);
        Pydj(j)=Pyd(i);
        PydDepthj(j)=PydDepth(i);
        dPydtj(j)=dPydt(i);
           end
     
        plot(s1,tspace,Pygj,'Linewidth',2)
        ylabel(s1,'Ttotal g of pyrite')
        plot(s2,tspace,Pydj,'Linewidth',2)
        ylabel(s2,'pyrite isotopic composition')
        plot(s3,tspace,PydDepthj,'Linewidth',2)
        ylabel(s3,'pyrite formation depth')
        plot(s4,tspace,dPydtj,'Linewidth',2)
        ylabel(s4,'pyrite formation rate')
        end
