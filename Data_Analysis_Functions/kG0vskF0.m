%% OC vs Iron reactivity
kF0=nan(1,216);
kF0err=nan(1,216);
kg0=nan(1,216);
kgsw=nan(1,216);
for i=1:216
    CoreNr=i;
    Data=ImportData(CoreNr);
    Parameters=ImportParameters(CoreNr);
    Fe=Data(2).F;
    z=(Data(1).F);
    b=Parameters.b;
    if numel(Fe)>2
        %let kF0 be a free parameter
        modelfun = @(p,t) max(Fe)*(b*Parameters.w./(b*Parameters.w+p(1).*t)).^b;
        beta0 = 1e-10;
        mdl = fitnlm(z,Fe,modelfun,beta0);
        kF0(i)=mdl.Coefficients.Estimate(1);
        kF0err(i)=mdl.Coefficients.SE;
        %Realtive RSME
        num = sum((Fe - modelfun(mdl.Coefficients.Estimate,z)).^2);
        den = sum((modelfun(mdl.Coefficients.Estimate,z)).^2);
        squared_error = num/den;
        rrsmeFree(i)= sqrt(squared_error);
        kgsw(i)=calkSW(Parameters.w);
        a=Parameters.a;
        OXX(i)=calcz0(Parameters.w); %in cm
        kg0(i)=a./(a./kgsw(i)+OXX(i)./Parameters.w); %kg a z=0
    end
end
kF0(kF0==0)=NaN;
kF0(kF0<0)=NaN;
%kF0(log10(kF0err)>0)=NaN;
[xData,yData]=prepareCurveData(log10(kgsw),log10(kF0));
[~,kf0err]=prepareCurveData(log10(kF0),log10(kF0err));
figure
[fitobject,gof] = fit(xData,yData,'poly1');%,'weights',abs(kf0err));
ci = predint(fitobject,linspace(min(xData)-1,max(xData)+1,100)); 
xconf = [linspace(min(xData)-1,max(xData)+1,100)'; linspace(max(xData)+1,min(xData)-1,100)']; 
yconf = [ci(:,1); flipud(ci(:,2))];  
hold on
p = fill(xconf,yconf,'red','FaceAlpha',0.2);
p.FaceColor = [0.3490    0.5647    0.7098];      
p.EdgeColor = 'none';  
pp=scatter(xData,yData,[],kf0err./yData,'filled','MarkerEdgeColor','k');
% [swData,yData]=prepareCurveData(log10(kgsw),log10(kF0));
% ppp=plot(swData,yData,'>','MarkerFaceColor','k');
l=plot(xconf,xconf,':');
%ll=plot(xconf,xconf+1,':');
p2=plot(fitobject,'b');
% [fitobjectsw,gof] = fit(swData,yData,'poly1');
% psw=plot(fitobjectsw,'k');
xlim([min(xconf) max(xconf)])
xlabel('log_{10} k_{G0} [y^{-1}]')
ylabel('log_{10} k_{F0} [y^{-1}]')
legend([p l],'y=0.94x1','1:1','Location','northwest')
colormap(flipud(slanCM('deep')))
c=colorbar;
c.Label.String='log_{10}SE k_{F0}';
%save("Data\Processed_Data\Extracted_Variables\kF0.mat",'kF0','kF0err');
%% Geometric mean regression
gmregress_zero_intercept(xData,yData)
%% 
% Here we explore if using Iron profiles with data within the ODP yields 
% bettter fit for kF0 than without, to explore the assumption that
% F0==F(sw) where F(sw) is reactive iron at the water-sediment interface and
% F0 at the topof the sulfate reduction zone.
dFdz=[];
dFdzOPD=[];
for j=[158 159 212 142]
    %remember to set z0 in importdata to 0
   Data=ImportData(j);
    Parameters=ImportParameters(j);
    Fe=Data(2).F;
    z=(Data(1).F);
    if j==158
        Fe=Fe(1:12);
        z=z(1:12);
    end
    if j==212
        Fe=Fe(1:95);
        z=z(1:95);
    end
     if numel(Fe)>2
    b=Parameters.b;  
    OXX=calcz0(Parameters.w); %in cm
%     Fe(z>10*OXX)=[];
%     z(z>10*OXX)=[];
    t=z./Parameters.w; %converted to time in ma;    
    OXX=OXX./Parameters.w;%converted to time in ma;
    if any(t<OXX)
    %Fe=movmedian(Fe,5);
    for i=1:numel(Fe)-1
    if t(i)>OXX       
dFdz(end+1)=-(Fe(i+1)-Fe(i))/(t(i+1)-t(i));
    else
dFdzOPD(end+1)=-(Fe(i+1)-Fe(i))/(t(i+1)-t(i));
    end
    end
    end
     end

end
%       subValue=min([dFdzOPD dFdz]);
%    dFdz=dFdz-subValue;
%     dFdzOPD=dFdzOPD-subValue;
%dFdz=randsample(dFdz,length(dFdzOPD));

dFdzS=sign(dFdz).*log10(abs(dFdz));
dFdzOPDS=sign(dFdzOPD).*log10(abs(dFdzOPD))

figure
boxchart(repmat(1,size(dFdz)),dFdzS)
hold on
boxchart(repmat(2,size(dFdzOPD)),dFdzOPDS)
ylabel('log_{10} dFdt')
set(gca,'xticklabel',{[]})
set(gca,'xticklabel',{'z>OPD','z<OPD'})

