function [cost,residuals]=calccost(ModelSolution,Data,Parameters,whichprofile,S0)
fn = fieldnames(Data);
idx=find(ismember(fn,whichprofile));
fnNonDim=fieldnames(ModelSolution);
for i=1:numel(idx)
    k=idx(i);
    z=getfield(ModelSolution(1),fnNonDim{k});
    zfactor=sqrt(Parameters.kg0/Parameters.DS);
    z=z./zfactor;
    D=getfield(ModelSolution(2),fnNonDim{k});
    %% Dimensionalize Equations again
D=D.*S0;
%Gamma
if strcmp(whichprofile,'G')
D=D./Parameters.fG;
end
%Psi
if strcmp(whichprofile,'F')
D=D./Parameters.fF;
end
%Pi
if strcmp(whichprofile,'P')
D=D./Parameters.fP;
end
%d34S
if contains(whichprofile,'d34')
D=D./S0;
end

InterpSolution=interp1(z,D,getfield(Data(1),fn{k}));
DataSolution=getfield(Data(2),fn{k});
DataSolution(isnan(InterpSolution))=[];
InterpSolution(isnan(InterpSolution))=[];
InterpSolution(isnan(DataSolution))=[];
residuals=(InterpSolution-DataSolution)';
cost=sqrt(sum(residuals.^2)/numel(InterpSolution));
if isempty(residuals)
    residuals=NaN;
end
end