function Solution = ModelSolverNonDim(NonDimBoundaries, NonDimParameters, zetamesh)
% ModelSolverNonDim solves a system of differential equations and boundary
% value problems to compute concentrations and isotopic compositions of
% various species in a sediment system.
%
% Inputs:
%   - NonDimBoundaries: Struct containing non-dimensional boundary conditions.
%   - NonDimParameters: Struct containing non-dimensional parameters.
%   - zetamesh: Array of mesh points for the solution grid.
%
% Output:
%   - Solution: Struct containing solution variables and their corresponding mesh points.

% Specify shooting function setup
maxIter = 50;   % Iterations for shooting function
ErrTol = 1e-6;  % Error tolerance for shooting function

% ODE options
options = odeset('NonNegative', 1);

%% Organic Carbon (Gamma)
Gamma = NonDimBoundaries.Gamma0 * (NonDimParameters.a ./ (NonDimParameters.a + (NonDimParameters.Da .* zetamesh))).^NonDimParameters.a;

%% Sulfate (Sigma)
ICguesses = [0, -500]; % Initial guesses for shooting function
odefun = @(zeta, Sigma) odefuncSulfate(zeta, Sigma, NonDimParameters, NonDimBoundaries); % ODE function
cEndS = 0; % Derivative at z = inf;
icS = NonDimBoundaries.Sigma0; % Initial condition for concentration
newIC = shooting(ICguesses, odefun, zetamesh, ErrTol, maxIter, [], [], icS, cEndS, options);
[zetaSigma, Sigma] = ode45(odefun, zetamesh, [NonDimBoundaries.Sigma0, newIC], options);

%% Reactive Iron (Psi)
Psi = NonDimBoundaries.Psi0 * (NonDimParameters.b ./ (NonDimParameters.b + (NonDimParameters.Da * NonDimParameters.chi * zetamesh))).^NonDimParameters.b;

%% H2S (Eta)
ICguesses = [newIC, -newIC]; % Initial guesses for shooting function
odefun = @(zeta, Eta) odefuncH2S(zeta, Eta, NonDimParameters, NonDimBoundaries, zetaSigma, Sigma); % ODE function
ic1 = NonDimBoundaries.Eta0; % Initial condition for H2S concentration
icend = 0; % Derivative of H2S concentration at end = 0;
newICH2S = shooting(ICguesses, odefun, zetamesh, ErrTol, maxIter, [], [], ic1, icend, options);
[zetaEta, Eta] = ode45(odefun, zetamesh, [ic1, newICH2S], options);

% Solve with BVP
bvpfunEta = @(zeta, Eta) bvpfuncH2S(zeta, Eta, NonDimParameters, NonDimBoundaries, zetaSigma, Sigma);
bvpbound = @(ya, yb) h2sboundary(ya, yb, NonDimBoundaries.Eta0); % Boundary conditions
solinitFunc = @(x) h2sguess(x, Eta, zetaEta); % Initial guess
solinit = bvpinit(zetamesh, solinitFunc); % Initial guess
BvPSolutionH2S = bvp5c(bvpfunEta, bvpbound, solinit);
Eta = BvPSolutionH2S.y(1, :);
zetaEta = BvPSolutionH2S.x;

%% Pyrite (Pi)
[zPi, Pi] = ode45(@(zPi, Pi) odefuncPi(zPi, NonDimParameters, NonDimBoundaries, zetaEta, Eta), zetamesh, NonDimBoundaries.Pi0);

%% Sulfate Isotopes (delta34Sigma)
% Limit depth until where sulfate concentration is very small
idx = find(Sigma(:, 1) < 1e-9, 1);
if isempty(idx)
    idx = numel(zetamesh);
end
zetameshBvP = linspace(0, zetamesh(min([idx, numel(zetamesh)])), 1000);
delta34S0 = 21.0; % d34S in seawater
gamma = 0.044162; % Standard isotopic 34S/32S ratio in VCDT
Rseawater = gamma * (delta34S0 / 1000 + 1); % 34/32S ratio in seawater
odefunIso = @(zeta, SigmaIso) bvpfuncSulfateIso(zeta, SigmaIso, NonDimParameters, NonDimBoundaries); % ODE function
odeboundIso = @(ya, yb) sulfateboundaryIso(ya, yb, NonDimBoundaries.Sigma0, Rseawater); % Boundary conditions
solinitFunc = @(x) sulfateguessIso(x, Sigma, zetaSigma); % Initial guess
solinitIso = bvpinit(zetameshBvP, solinitFunc); % Initial guess
BvPSolutionIso = bvp5c(odefunIso, odeboundIso, solinitIso);
deltaIso = BvPSolutionIso.y(1, :) ./ BvPSolutionIso.y(3, :);
delta34Sigma = ((deltaIso ./ gamma) - 1) * 1000;

%% H2S Isotopes (delta34Eta)
RseawaterH2S = gamma * (-40 / 1000 + 1); % 34/32S ratio in seawater for H2S
ic0EtaIso34 = NonDimBoundaries.Eta0 * RseawaterH2S / (1 + RseawaterH2S);
ic0EtaIso32 = NonDimBoundaries.Eta0 / (1 + RseawaterH2S);
odefunEtaIso = @(zeta, EtaIso) bvpfuncH2SIso(zeta, EtaIso, NonDimParameters, NonDimBoundaries, BvPSolutionIso, zetaEta, Eta); % ODE function
odeboundIso = @(ya, yb) h2sboundaryIso(ya, yb, ic0EtaIso32, ic0EtaIso34); % Boundary conditions
solinitFunc = @(x) h2sguessIso(x, BvPSolutionH2S.y, zetaEta); % Initial guess
solinitIso = bvpinit(zetameshBvP, solinitFunc); % Initial guess
BvPSolutionH2SIso = bvp5c(odefunEtaIso, odeboundIso, solinitIso);
deltaIsoEta = BvPSolutionH2SIso.y(1, :) ./ BvPSolutionH2SIso.y(3, :);
delta34Eta = ((deltaIsoEta ./ gamma) - 1) * 1000;

%% Pyrite Isotopes (delta34Pi)
options = odeset('NonNegative', 1,'MaxStep',1e-3);
% 32Pyrite
[~, Pi32] = ode45(@(zPi32, Pi32) odefuncPi32(zPi32, NonDimParameters, NonDimBoundaries, zetaEta, Eta, BvPSolutionH2SIso.x, BvPSolutionH2SIso.y(3, :)), zetamesh, 0, options);
% 34Pyrite
[~, Pi34] = ode45(@(zPi34, Pi34) odefuncPi34(zPi34, NonDimParameters, NonDimBoundaries, zetaEta, Eta, BvPSolutionH2SIso.x, BvPSolutionH2SIso.y(1, :)), zetamesh, 0, options);
% Calculate delta
deltaIsoPi = Pi34(:, 1) ./ Pi32(:, 1);
delta34Pi = ((deltaIsoPi ./ gamma) - 1) * 1000;

%% Build Solution Structure
Solution = struct('Gamma', {zetamesh, Gamma}, ...
                  'Sigma', {zetaSigma, Sigma(:, 1)}, ...
                  'Eta', {zetaEta, Eta}, ...
                  'Psi', {zetamesh, Psi}, ...
                  'Pi', {zPi, Pi}, ...
                  'd34Sigma', {BvPSolutionIso.x, delta34Sigma}, ...
                  'd34Eta', {BvPSolutionH2SIso.x, delta34Eta}, ...
                  'd34Pi', {zetamesh, delta34Pi});

end
