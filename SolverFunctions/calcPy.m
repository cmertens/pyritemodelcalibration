function [Pyd,PydDepth,PyWtPercent]=calcPy(ModelSolution)
%This function extracts Pyrite formation depth, concentration
Py=ModelSolution(2).Pi;
Pyd34=ModelSolution(2).d34Pi;
Pyz=ModelSolution(1).Pi;
Pyd34z=ModelSolution(1).d34Pi;
Pyd34=interp1(Pyd34z,Pyd34,Pyz);
Pyz(Py==0)=[];
Pyd34(Py==0)=[];
Py(Py==0)=[];
Pyz(isnan(Py))=[];
Pyd34(isnan(Py))=[];
Py(isnan(Py))=[];
PyDerivative=0.0005*Py(end);
idx=find(flipud(diff(Py))>PyDerivative,1);% find first derivative in upside down array that is larger than Threshhold
if isempty(idx)
    PydDepth=0;
else
    PydDepth=Pyz(numel(Py)-idx);
end
fdIdx=find(Pyz>=PydDepth,1);
%delete pyrite values for depth larger than pyrite formation
%depth
Py(fdIdx:end)=[];
Pyz(fdIdx:end)=[];
Pyd34(fdIdx:end)=[];
%convert pyrite concentrations into weights
Pyw= round(Py/min(Py));
% Pyrite delta at pyrite formation depth
result=[];
for l = 1:numel(Py)
    repeated_element = repmat(Pyd34(l), 1, Pyw(l));
    result = [result, repeated_element];
end
%Pyd=median(result,'omitnan');

Pyd34(isnan(Pyd34))=[];
Pyd=Pyd34(end);

PyWtPercentNonDim=trapz(Pyz,Py);%sum(Py)/numel(Pyz);%Py(end);%
%Dimnesional
PyWtPercent=PyWtPercentNonDim;%*2.1*28/537.2012;%PyWtPercentNonDim;%
%Pyrite Burial Rate



